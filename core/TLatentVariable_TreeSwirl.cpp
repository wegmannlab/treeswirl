//
// Created by creyna on 06.04.21.
//
#include "TLatentVariable_TreeSwirl.h"
#include "THelper.h"
#include "coretools/devtools.h"

using namespace coretools::instances;

void TLatentVariable_TreeSwirl::initialize(const TTree &Tree, const TGenome *Sites, size_t NumStatesPerMig, double Mu,
                                           double Sigma2_0, const std::string &Outname) {
	_tree    = Tree;
	_sites   = Sites;
	_mu      = Mu;
	_outname = Outname;

	_varCovarsW.initialize(_tree, NumStatesPerMig);
	_varCovarsSinv.initialize(_sites->getSigmas(), _varCovarsW, Sigma2_0);

	_dimGamma.init(std::array<size_t, 2>({_sites->numLoci(), _varCovarsW.numStates_J()}));
	_dimF.init(std::array<size_t, 3>{_tree.numBranches_K(), _varCovarsSinv.numSigmas(), _varCovarsW.numStates_J()});

	_openEMTrace();
	_openFileDetHessian();
	_readCommandLineParamsHessian();

	if (parameters().exists("fixEmission")) { _update = false; }
}

void TLatentVariable_TreeSwirl::initializeFromFile(std::string_view Filename) {
	coretools::TInputFile file(Filename, coretools::FileType::Header);
	std::string tmp;
	for (; !file.empty(); file.popFront()) { tmp = file.frontRaw(); } // go to last line
	coretools::str::TSplitter spl(tmp, '\t');
	// mu
	_mu = coretools::str::fromString<double>(spl.front());
	spl.popFront();

	// sigma2
	double sigma2 = coretools::str::fromString<double>(spl.front());
	spl.popFront();

	// branch lengths
	std::vector<double> branchLengths;
	for (auto it : spl) { branchLengths.emplace_back(coretools::str::fromString<double>(it)); }

	// set values in tree
	_tree.update(branchLengths);
	if (!_varCovarsW.update(_tree)) {
		UERROR("Invalid branch lengths provided in file ", Filename, ": ", branchLengths);
	}
	_varCovarsSinv.update(_sites->getSigmas(), _varCovarsW, sigma2);

	logfile().startIndent("Set initial emission parameters from file ", Filename, ":");
	logfile().list("mu = ", _mu);
	logfile().list("sigma2 = ", sigma2);
	logfile().list("branch lengths = ", branchLengths);
	logfile().endIndent();
}

void TLatentVariable_TreeSwirl::_openEMTrace() {
	std::vector<std::string> header{"mu", "sigma2"};
	for (NumBranchType b = 0; b < _tree.numBranches_K(); b++) { header.push_back("Branch:" + _tree.branchNames(b)); }
	std::string filename = _outname + "_EMParameters.txt";
	_traceEM.open(filename, header);
	_writeEMTrace();
}

void TLatentVariable_TreeSwirl::_openFileDetHessian() {
	if (writeTempFiles()) { _fileDetHessian.open(_outname + "_detHessian.txt", {"det_Hessian"}); }
}

void TLatentVariable_TreeSwirl::_readCommandLineParamsHessian() {
	// strategies for calculating Hessian
	if (parameters().exists("fracApproxHessian")) {
		_approxHessian = true;
		_alphaHessian  = parameters().get<coretools::ZeroOneClosed>("fracApproxHessian");
		logfile().list("Will approximate Hessian by calculating only the terms that are ", _alphaHessian,
		               " as good as max value of diagonal.");
	}

	// open file timer for NewtonRaphson
	if (writeTempFiles()) { _timerNR.open(_outname + "_timerNR.txt", {"counterFirst", "counterSecond", "timeNR"}); }
}

std::vector<double> TLatentVariable_TreeSwirl::getParameters() const {
	auto params = _tree.branchLengths_c();
	params.push_back(_mu);
	params.push_back(_varCovarsSinv.sigma2());
	return params;
}

bool TLatentVariable_TreeSwirl::setParameters(coretools::TConstView<double> Params) {
	// update tree and variance covariance matrices with new lengths and prior sigma2
	if (!_update) { return true; }

	std::vector<double> branchLengths(Params.begin(), Params.begin() + _tree.numBranches_K());
	_tree.update(branchLengths);
	if (!_varCovarsW.update(_tree)) { return false; }

	_mu = Params[_tree.numBranches_K()];
	_varCovarsSinv.update(_sites->getSigmas(), _varCovarsW, Params.back());

	return true;
}

void TLatentVariable_TreeSwirl::calculateEmissionProbabilities(
    NumLociType Index, stattools::TDataVector<double, NumStatesType> &Emission) const {
	arma::Col<double> dmu = _sites->data(Index) - _mu;
	for (int j = 0; j < _varCovarsW.numStates_J(); j++) { Emission[j] = emissionProbability(Index, j, dmu); }
}

double TLatentVariable_TreeSwirl::emissionProbability(NumLociType l, NumStatesType j,
                                                      const arma::Col<double> &DMu) const {
	// l is a locus position, have to map it to corresponding Sigma index and S matrix
	const NumSigmaType sigmaIx = _sites->LociToSigmaIndex(l);
	// calculate exponent of MVN distribution = - 1/2 (x-mu)^T Sigma^{-1} (x-mu)
	const double exponent      = -0.5 * arma::dot(DMu, _varCovarsSinv.getSinv(j, sigmaIx) * DMu);
	// calculate first term of MVN distribution: 1 / (sqrt((2*pi)^k * detSigma)
	// -> calculate in log and take exp afterward
	const double k_log2pi      = _tree.numPopulations_M() * LTWO_PI;
	const double logDet        = _varCovarsSinv.getLogDetSinv(j, sigmaIx);
	// put it all together
	const double emission      = exp(0.5 * (logDet - k_log2pi) + exponent);
	return emission;
}

void TLatentVariable_TreeSwirl::prepareEMParameterEstimationInitial() { _gammas.resize(_dimGamma.size()); }

void TLatentVariable_TreeSwirl::prepareEMParameterEstimationOneIteration() {
	// initialize gammas
	std::fill(_gammas.begin(), _gammas.end(), 0.0);

	// resize std vectors used in many functions
	_ones.ones(_tree.numPopulations_M(), _tree.numPopulations_M());
	_dmu.resize(_tree.numPopulations_M());
}

void TLatentVariable_TreeSwirl::handleEMParameterEstimationOneIteration(
    NumLociType Index, const stattools::TDataVector<double, NumStatesType> &Weights) {

	for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
		const size_t index = _dimGamma.getIndex({Index, j});
		_gammas[index]     = Weights[j];
	}
}

void TLatentVariable_TreeSwirl::finalizeEMParameterEstimationOneIteration() {
	if (_update) { _updateParameters(); }
	_writeEMTrace();
}

void TLatentVariable_TreeSwirl::finalizeEMParameterEstimationFinal() {
	_ones.reset();
	_dmu.reset();
	_gammas.clear();
	_traceEM.close();
	if (_fileDetHessian.isOpen()) { _fileDetHessian.close(); }
	if (_timerNR.isOpen()) { _timerNR.close(); }
}

void TLatentVariable_TreeSwirl::reportEMParameters() {
	coretools::instances::logfile().list("mu, sigma2 = [", _mu, ", ", _varCovarsSinv.sigma2(),
	                                     "], branch lengths = ", _tree.branchLengths_c());
}

void TLatentVariable_TreeSwirl::printLLInNewtonRaphson(TSTransMat *TransitionMatrices,
                                                       genometools::TDistancesBinned<NumDistanceGroupType> *Distances) {
	_printLLInNewtonRaphson = true;
	_transitionMatrices     = TransitionMatrices;
	_distances              = Distances;
}

void TLatentVariable_TreeSwirl::_updateParameters() {
	std::vector<double> vecParameters = _tree.branchLengths_c();
	vecParameters.push_back(_varCovarsSinv.sigma2());

	if (parameters().exists("doNelderMead")) {
		_doNelderMead(vecParameters);
	} else {
		_doNewtonRaphson(vecParameters);
	}
}

std::vector<double> convertToNelderMeadSpace(const std::vector<double> &VecParameters) {
	// first parameters are branch lengths: must be >= 0 -> take log
	// last parameter is sigma2: must be > 0 -> also take log
	std::vector<double> res(VecParameters.size());
	for (size_t i = 0; i < VecParameters.size(); ++i) { res[i] = log(VecParameters[i]); }
	return res;
}

std::vector<double> convertFromNelderMeadSpace(coretools::TConstView<double> VecParameters) {
	// inverse of convertToNelderMeadSpace
	std::vector<double> res(VecParameters.size());
	for (size_t i = 0; i < VecParameters.size(); ++i) { res[i] = exp(VecParameters[i]); }
	return res;
}

double TLatentVariable_TreeSwirl::_calcMinusQ(coretools::TConstView<double> Values) {
	// Values = log of branch lengths and sigma2
	using namespace coretools;
	auto vals = convertFromNelderMeadSpace(Values);
	_updateParametersNR(vals);

	double sum = 0.0;
	for (size_t l = 0; l < getSizeOfSubset(); ++l) {
		arma::Col<double> dmu = _sites->data(l) - _mu;
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			// save gammas for all loci and states
			const size_t index  = _dimGamma.getIndex({l, j});
			const auto gamma    = _gammas[index];
			const auto emission = emissionProbability(l, j, dmu);
			sum += log(emission) * gamma;
		}
	}

	return -sum; // return -sum, because we want to maximize Q, but Nelder-Mead minimize
}

void TLatentVariable_TreeSwirl::_doNelderMead(const std::vector<double> &VecParameters) {
	// start Nelder-Mead with simplex of previous iteration
	auto ptr = &TLatentVariable_TreeSwirl::_calcMinusQ;
	stattools::TNelderMead nelderMead(*this, ptr);

	if (_iter == 0) {
		// set low tolerance and maxNumFunctionEvaluations, as this is the first iteration
		// and EM will change values in next iteration anyways
		nelderMead.setFractionalConvergenceTolerance(10e-10);
		nelderMead.setMaxNumFunctionEvaluations(1000);

		// set initial simplex for Nelder Mead
		std::vector<double> init      = convertToNelderMeadSpace(VecParameters);
		_simplexOfPreviousEMIteration = stattools::TSimplex(init, 10.0);
	} else {
		nelderMead.dynamicallyAdjustTolerance(_simplexOfPreviousEMIteration[0].value(), 0.000001, 100);
	}
	nelderMead.minimize(_simplexOfPreviousEMIteration);
	logfile().list(nelderMead.returnCode().message());

	// store simplex to start next EM iteration with this
	_simplexOfPreviousEMIteration = nelderMead.getCurrentSimplex();

	// store final parameter estimates
	auto params = convertFromNelderMeadSpace(nelderMead.coordinates());
	_updateParametersNR(params);

	++_iter;
}

void TLatentVariable_TreeSwirl::_doNewtonRaphson(const std::vector<double> &VecParameters) {
	// multidimensional Newton-Raphson for branch lengths

	// first set counters to zero
	_counterFirstDerivative  = 0;
	_counterSecondDerivative = 0;

	// set timer
	coretools::TTimer timer;

	// initialize relevant indices of Hessian (if needed)
	if (_approxHessian) { _fillIxApproxHessian(); }

	// now run NR
	auto ptrGradient = &TLatentVariable_TreeSwirl::_calcFirstDerivative;
	auto ptrHessian  = &TLatentVariable_TreeSwirl::_calcSecondDerivative;
	stattools::TMultiDimensionalNewtonRaphson NR(*this, ptrGradient, ptrHessian);
	bool converged = NR.runNewtonRaphson_withBacktracking(VecParameters); // vector of sigma and branch length

	if (!converged) {
		logfile().list("No convergence = ", NR.returnCode().message());
	} else {
		logfile().list("Convergence = ", NR.returnCode().message());
	}

	if (_timerNR.isOpen()) {
		_timerNR << _counterFirstDerivative << _counterSecondDerivative << timer.seconds() << coretools::endl;
	}
}

bool TLatentVariable_TreeSwirl::_updateParametersNR(coretools::TConstView<double> VecParameters) {
	std::vector<double> branchLengths(VecParameters.begin(), VecParameters.begin() + _tree.numBranches_K());
	double sigma2 = VecParameters.back();

	// update tree and variance covariance matrices with new lengths and prior sigma2
	_tree.update(branchLengths);
	if (!_varCovarsW.update(_tree)) { return false; }
	_varCovarsSinv.update(_sites->getSigmas(), _varCovarsW, sigma2);

	// vars needed for equation 24
	double emNumerator_mu   = 0.0;
	double emDenominator_mu = 0.0;
	arma::Row<double> ones(_tree.numPopulations_M(), arma::fill::ones);
	arma::Row<double> tmp(_tree.numPopulations_M());
	// equation 24 = estimate mu
	for (NumLociType l = 0; l < getSizeOfSubset(); l++) {
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			// calculate numerator and denominator of Mu, then sum their values for all loci and states
			tmp = _gammas[_dimGamma.getIndex({l, j})] * ones * _varCovarsSinv.getSinv(j, _sites->LociToSigmaIndex(l));
			emNumerator_mu += arma::dot(tmp, _sites->data(l));
			emDenominator_mu += arma::dot(tmp, ones);
		}
	}
	_mu = emNumerator_mu / emDenominator_mu;

	return true;
}

std::pair<std::vector<double>, bool>
TLatentVariable_TreeSwirl::_calcFirstDerivative(coretools::TConstView<double> VecParameters, size_t) {
	bool validBranches = _updateParametersNR(VecParameters);
	if (!validBranches || VecParameters.back() <= 0.0) { return {{}, false}; }

	// debugging: calculate LL
	if (_printLLInNewtonRaphson) {
		stattools::THMM<double, NumStatesType, NumLociType> hmm(*_transitionMatrices, *this);
		double LL = hmm.calculateLL(_distances->getChunkEnds<NumLociType>());
		std::cout << LL << ": " << coretools::str::toString(VecParameters) << std::endl;
	}

	return _calculateF();
}

double TLatentVariable_TreeSwirl::_calculateGradient_k(size_t k, const std::vector<double> &trace_SinvJ,
                                                       const std::vector<arma::Mat<double>> &SinvJSinv) {
	double sum = 0.0;
	for (NumLociType l = 0; l < getSizeOfSubset(); l++) {
		const arma::Col<double> dmu = _sites->data(l) - _mu;
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			// conversion from locus index to sigma index
			const size_t index = _dimF.getIndex({k, _sites->LociToSigmaIndex(l), j});
			sum += _gammas[_dimGamma.getIndex({l, j})] *
			       (0.5 * (trace_SinvJ[index] + arma::dot(dmu, SinvJSinv[index] * dmu)));
		}
	}
	return sum;
}

std::pair<std::vector<double>, bool> TLatentVariable_TreeSwirl::_calculateF() {
	++_counterFirstDerivative;

	// vars needed for equation 21,22
	arma::Mat<double> SinvJ(_tree.numPopulations_M(), _tree.numPopulations_M(), arma::fill::zeros);
	std::vector<double> trace_SinvJ(_dimF.size());
	std::vector<arma::Mat<double>> SinvJSinv(_dimF.size(),
	                                         arma::Mat<double>(_tree.numPopulations_M(), _tree.numPopulations_M()));

	// save all values, looping over all branches, sigmas and j states
	for (NumBranchType k = 0; k < _tree.numBranches_K(); k++) {
		for (NumSigmaType s = 0; s < _varCovarsSinv.numSigmas(); s++) { // replace loci l for s sigmas
			for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
				SinvJ = _varCovarsSinv.getSinv(j, s) * _varCovarsW.getW(j).J(k);

				const size_t index = _dimF.getIndex({k, s, j});

				trace_SinvJ[index] = -(arma::trace(SinvJ));

				SinvJSinv[index] = SinvJ * _varCovarsSinv.getSinv(j, s);
			}
		}
	}

	// loop over all states and loci, convert loci index to sigma index and get the right index for each vector of
	// value
	std::vector<double> gradient_Fc(_tree.numBranches_K() + 1, 0.0); //+1 to include sigma2
	for (NumBranchType k = 0; k < _tree.numBranches_K(); k++) {
		gradient_Fc[k] = _calculateGradient_k(k, trace_SinvJ, SinvJSinv);
	}

	// first derivative of sigma2
	gradient_Fc[_tree.numBranches_K()] = _calcFirstDerivativeSigma2();
	// logfile().list("gradient = " + coretools::str::toString(gradient_Fc));

	return {gradient_Fc, true};
}

double TLatentVariable_TreeSwirl::_calcFirstDerivativeSigma2() {

	double value_F{};
	// arma::Col<double> dmu(_tree.numPopulations_M());
	arma::Mat<double> T(_tree.numPopulations_M(), _tree.numPopulations_M(), arma::fill::zeros);

	for (NumLociType l = 0; l < getSizeOfSubset(); l++) {
		_dmu           = _sites->data(l) - _mu;
		NumSigmaType s = _sites->LociToSigmaIndex(l);
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			T = _varCovarsSinv.getSinv(j, s) * _ones;
			// formula 24 after mu
			value_F += _gammas[_dimGamma.getIndex({l, j})] *
			           (arma::trace(T) - arma::dot(_dmu, T * _varCovarsSinv.getSinv(j, s) * _dmu));
		}
	}

	value_F *= -0.5;

	return value_F;
}

std::pair<std::vector<double>, std::vector<arma::Mat<double>>> TLatentVariable_TreeSwirl::_calcKsInvTrace() {
	// size of vectors needed for equation 23
	size_t vec_size = _tree.numBranches_K() * (_tree.numBranches_K() + 1) / 2;
	vec_size *= _varCovarsW.numStates_J() * _varCovarsSinv.numSigmas();
	std::vector<double> trace_Kik(vec_size);
	std::vector<arma::Mat<double>> KsSinv(vec_size,
	                                      arma::Mat<double>(_tree.numPopulations_M(), _tree.numPopulations_M()));
	/// equation 23
	size_t index = 0;
	for (NumBranchType i = 0; i < _tree.numBranches_K(); i++) {
		arma::Mat<double> SJi(_tree.numPopulations_M(), _tree.numPopulations_M());
		arma::Mat<double> SJk(_tree.numPopulations_M(), _tree.numPopulations_M());
		arma::Mat<double> Kik(_tree.numPopulations_M(), _tree.numPopulations_M());
		arma::Mat<double> Kki(_tree.numPopulations_M(), _tree.numPopulations_M());
		for (NumBranchType k = i; k < _tree.numBranches_K(); k++) {
			for (NumSigmaType s = 0; s < _varCovarsSinv.numSigmas(); s++) {
				for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
					SJi = _varCovarsSinv.getSinv(j, s) * _varCovarsW.getW(j).J(i);
					SJk = _varCovarsSinv.getSinv(j, s) * _varCovarsW.getW(j).J(k);
					Kik = SJi * SJk;
					if (i != k)
						Kki = SJk * SJi;
					else
						Kki = Kik;

					trace_Kik[index] = arma::trace(Kik);

					KsSinv[index] = (Kik + Kki) * _varCovarsSinv.getSinv(j, s);

					index += 1;
				}
			}
		}
	}
	return std::make_pair(trace_Kik, KsSinv);
}

double TLatentVariable_TreeSwirl::_parallelSum2ndDerivative(size_t cumSumK, const std::vector<double> &trace_Kik,
                                                            const std::vector<arma::Mat<double>> &KsSinv) {
	double sum = 0.0;
	for (NumLociType l = 0; l < getSizeOfSubset(); l++) {
		arma::Col<double> dmu = _sites->data(l) - _mu;
		size_t tmp = (cumSumK * _varCovarsSinv.numSigmas() + _sites->LociToSigmaIndex(l)) * _varCovarsW.numStates_J();
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			size_t index = tmp + j;
			sum += _gammas[_dimGamma.getIndex({l, j})] * (trace_Kik[index] - arma::dot(dmu, KsSinv[index] * dmu));
		}
	}
	return 0.5 * sum;
}

void TLatentVariable_TreeSwirl::_fillIxApproxHessian() {
	// pre-calculate terms
	const auto [trace_Kik, KsSinv] = _calcKsInvTrace();

	// fill Hessian for branch lengths
	arma::Mat<double> hessian_Fc(_tree.numBranches_K(), _tree.numBranches_K(), arma::fill::zeros);
	_fillHessianFull(hessian_Fc, trace_Kik, KsSinv);

	// find largest absolute value of diagonal
	double m = std::numeric_limits<double>::lowest();
	for (size_t i = 0; i < hessian_Fc.n_rows; ++i) {
		const auto tmp = std::fabs(hessian_Fc(i, i));
		if (tmp > m) { m = tmp; }
	}

	const auto threshold = m * (double)_alphaHessian;

	// get all indices that are larger than m (in absolute values)
	_relevantHessianIx.clear();
	size_t cumSumK = 0;
	for (NumBranchType i = 0; i < _tree.numBranches_K(); i++) {
		for (NumBranchType k = i; k < _tree.numBranches_K(); k++, cumSumK++) {
			if (i == k || std::fabs(hessian_Fc(i, k)) >= threshold) {
				const std::array<size_t, 3> arr = {i, k, cumSumK};
				_relevantHessianIx.emplace_back(arr);
			}
		}
	}
}

void TLatentVariable_TreeSwirl::_fillHessianFull(arma::Mat<double> &hessian_Fc, const std::vector<double> &trace_Kik,
                                                 const std::vector<arma::Mat<double>> &KsSinv) {
	++_counterSecondDerivative;
	size_t cumSumK = 0;
	for (NumBranchType i = 0; i < _tree.numBranches_K(); i++) {
		for (NumBranchType k = i; k < _tree.numBranches_K(); k++, cumSumK++) {
			hessian_Fc(i, k) = _parallelSum2ndDerivative(cumSumK, trace_Kik, KsSinv);
			if (i != k) { hessian_Fc(k, i) = hessian_Fc(i, k); }
		}
	}
}

void TLatentVariable_TreeSwirl::_fillHessianApprox(arma::Mat<double> &hessian_Fc, const std::vector<double> &trace_Kik,
                                                   const std::vector<arma::Mat<double>> &KsSinv) {
	++_counterSecondDerivative;
	for (const auto &ix : _relevantHessianIx) {
		const auto i       = ix[0];
		const auto k       = ix[1];
		const auto cumSumK = ix[2];
		hessian_Fc(i, k)   = _parallelSum2ndDerivative(cumSumK, trace_Kik, KsSinv);
		if (i != k) { hessian_Fc(k, i) = hessian_Fc(i, k); }
	}
}

arma::Mat<double> TLatentVariable_TreeSwirl::_calcSecondDerivative(coretools::TConstView<double>,
                                                                   coretools::TConstView<double>, size_t) {
	// pre-calculate terms
	const auto [trace_Kik, KsSinv] = _calcKsInvTrace();

	// fill Hessian for branch lengths
	arma::Mat<double> hessian_Fc(_tree.numBranches_K() + 1, _tree.numBranches_K() + 1, arma::fill::zeros);
	if (_approxHessian) {
		_fillHessianApprox(hessian_Fc, trace_Kik, KsSinv);
	} else {
		_fillHessianFull(hessian_Fc, trace_Kik, KsSinv);
	}

	// fill Hessian for sigma2 (last row and col of Hessian)
	hessian_Fc(_tree.numBranches_K(), _tree.numBranches_K()) = _calcSecondDerivativeSigma2();

	if (_fileDetHessian.isOpen()) {
		const double detHessian = arma::det(hessian_Fc);
		_fileDetHessian << detHessian << coretools::endl;
	}

	return hessian_Fc;
}

std::pair<std::vector<arma::mat>, std::vector<double>> TLatentVariable_TreeSwirl::_calcTSquare() {
	std::vector<arma::mat> T_squared(_varCovarsSinv.numSigmas() * _varCovarsW.numStates_J());
	std::vector<double> T_squared_trace(_varCovarsSinv.numSigmas() * _varCovarsW.numStates_J());

	size_t ix = 0;
	for (NumSigmaType s = 0; s < _varCovarsSinv.numSigmas(); s++) {
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++, ++ix) {
			T_squared[ix] = _varCovarsSinv.getSinv(j, s) * _ones;
			T_squared[ix] *= T_squared[ix];
			T_squared_trace[ix] = arma::trace(T_squared[ix]);
		}
	}
	return std::make_pair(T_squared, T_squared_trace);
}

double TLatentVariable_TreeSwirl::_calcSecondDerivativeSigma2() {
	const auto [T_squared, T_squared_trace] = _calcTSquare();

	double value_D = 0.0;
	for (NumLociType l = 0; l < getSizeOfSubset(); l++) {
		_dmu           = _sites->data(l) - _mu;
		NumSigmaType s = _sites->LociToSigmaIndex(l);
		for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); j++) {
			size_t ix = s * _varCovarsW.numStates_J() + j;
			value_D +=
			    _gammas[_dimGamma.getIndex({l, j})] *
			    (T_squared_trace[ix] - (2.0 * arma::dot(_dmu, T_squared[ix] * _varCovarsSinv.getSinv(j, s) * _dmu)));
		}
	}

	value_D *= 0.5;

	return value_D;
}

void TLatentVariable_TreeSwirl::_writeEMTrace() {
	_traceEM << _mu << _varCovarsSinv.sigma2();
	for (const auto &branch : _tree.branchLengths_c()) { _traceEM << branch; }
	_traceEM << coretools::endl;
}

void TLatentVariable_TreeSwirl::_openPostMeanMigRateFile() {
	std::vector<std::string> header = {"Chr", "Position"};
	for (NumMigEdgeType m = 0; m < _tree.numInferredMigEdges(); ++m) {
		header.emplace_back("posteriorMeanMigrationRate_" + _tree.migEdgeNames()[m]);
	}
	_filePostMeanMigRate.open(_outname + "_PosteriorMeanMigrationRate.txt", header);
}

void TLatentVariable_TreeSwirl::prepareStatePosteriorEstimationAndWriting(NumStatesType,
                                                                          std::vector<std::string> &header) {
	// fill header
	header.clear();
	header = {"Chr", "Position"};
	for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); ++j) {
		std::string state = "P(z=" + coretools::str::toString(j) + "{";
		for (NumMigEdgeType m = 0; m < _tree.numInferredMigEdges(); ++m) {
			state += coretools::str::toString(_varCovarsW.getW(j).migrationWeights()[m]);
			if (m != _tree.numInferredMigEdges() - 1) state += ",";
		}
		state += "})";
		header.push_back(state);
	}

	// also open and fill header of _meanPosteriorFile
	_openPostMeanMigRateFile();
}

void TLatentVariable_TreeSwirl::_writeToPostMeanMigRateFile(
    NumLociType Index, const stattools::TDataVector<double, NumStatesType> &StatePosteriorProbabilities) {
	// calculate mean posterior migration rate
	std::vector<double> meanPerEdge(_tree.numInferredMigEdges(), 0.0);

	for (NumStatesType j = 0; j < _varCovarsW.numStates_J(); ++j) {
		for (NumMigEdgeType i = 0; i < _tree.numInferredMigEdges(); ++i) {
			meanPerEdge[i] += _varCovarsW.getW(j).migrationWeights()[i] * StatePosteriorProbabilities[j];
		}
	}

	// now write
	_filePostMeanMigRate << _sites->getChrName(Index) << _sites->getLociPos(Index);
	_filePostMeanMigRate.write(meanPerEdge);
	_filePostMeanMigRate.endln();
}

void TLatentVariable_TreeSwirl::handleStatePosteriorEstimationAndWriting(
    NumLociType Index, const stattools::TDataVector<double, NumStatesType> &StatePosteriorProbabilities,
    coretools::TOutputFile &Out) {
	Out << _sites->getChrName(Index) << _sites->getLociPos(Index);
	for (NumStatesType s = 0; s < StatePosteriorProbabilities.size(); ++s) { Out << StatePosteriorProbabilities[s]; }
	Out << coretools::endl;

	// also write to _filePostMeanMigRate
	_writeToPostMeanMigRateFile(Index, StatePosteriorProbabilities);
}
