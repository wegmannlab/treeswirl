//
// Created by madleina on 16.02.24.
//

#ifndef TREESWIRL_TGAUSSIANMIXTURE_EM_H
#define TREESWIRL_TGAUSSIANMIXTURE_EM_H

#include "TLatentVariable_initialTree.h"
#include "TNelderMeadRSS.h"
#include "stattools/EM/TEM.h"
#include "stattools/MLEInference/TNelderMead.h"
#include "stattools/Priors/TPriorCategorical.h"
#include "stattools/Priors/TPriorUniform.h"

class TGaussianMixture_EM {
private:
	TTree *_tree;
	const TGenome *_sites;

	std::string _outname;

	size_t _numClasses;

	double _mu_0;
	double _sigma2_0;
	bool _fix_mu_0;
	bool _fix_sigma2_0;

	std::vector<arma::Col<double>> _y;
	std::vector<double> _pis;

	double _runEM();
	void _runFixedMigration();

public:
	TGaussianMixture_EM(size_t NumClasses, TTree *Tree, const TGenome *Sites, const std::string &Outname, double InitialMu,
	           double InitialSigma2, bool FixMu, bool FixSigma2);

	double run();

	// getters
	double mu_0() const { return _mu_0; }
	double sigma_0() const { return _sigma2_0; }
	const auto &y() const { return _y; }
	const auto &pis() const { return _pis; }
};

#endif // TREESWIRL_TGAUSSIANMIXTURE_EM_H
