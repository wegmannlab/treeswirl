//
// Created by creyna on 10.03.21.
//

#include "TEstimator.h"
#include "TEstimatorAttractor.h"
#include "THelper.h"
#include "coretools/Strings/concatenateString.h"
#include "stattools/HMM/TLatentVariableFixed.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"

using namespace coretools::instances;

TEstimator::TEstimator() {
	_outname         = parameters().get<std::string>("out", "TreeSwirl");
	_numStatesPerMig = parameters().get("numStates", 21);
	logfile().list("Number of discrete mixture proportions per migration edge = ", _numStatesPerMig);

	// read tree and sites
	_initializeTree();
	_initializeSites();

	// open summary file
	_openSummaryFile();
}

void TEstimator::_openSummaryFile() {
	std::vector<std::string> header = {"name", "LL", "mu", "sigma2"};
	for (auto &b : _tree.branchNames()) { header.emplace_back(b); } // branch lengths
	for (size_t i = 0; i < _tree.numInferredMigEdges(); ++i) {
		const auto tmp = coretools::str::toString(i);
		header.emplace_back("kappa" + tmp);
		header.emplace_back("phi" + tmp);
		header.emplace_back("zeta" + tmp);
		header.emplace_back("attractor" + tmp);
	}

	_summaryFile.open(_outname + "_summary.txt", header);
	logfile().list("Writing summary of LL and parameter values for all configurations to file '", _summaryFile.name(),
	               "'.");
}

void TEstimator::_initializeTree() {
	std::string treeFile = parameters().get("treeFile");
	_tree.initializeFromFile_TreeSwirl(treeFile);
}

void TEstimator::_initializeSites() {
	bool addNoise = false;
	if (parameters().exists("addNoise")) addNoise = true;
	std::string dataFile = parameters().get("dataFile");
	_sites.readSitesPopulations(dataFile, _tree.populationNames(), addNoise);
	_distances = _sites.distances();
}

void TEstimator::infer() {
	const bool skipEM = false;
	_run(skipEM);
}

void TEstimator::calculateLL() {
	const bool skipEM = true;
	_run(skipEM);
}

auto TEstimator::_runGaussianMixtureRSS(bool SkipEM) {
	TInitializer_Tree gaussianMixture(&_tree, &_sites, _outname);
	gaussianMixture.run(SkipEM);
	_sites.removeNoise();
	return std::make_pair(gaussianMixture.mu_0(), gaussianMixture.sigma2_0());
}

void TEstimator::_run(bool SkipEM) {
	// run initial EM to estimate branch lengths (Gaussian Mixture)
	const auto [mu_0, Sigma2_0] = _runGaussianMixtureRSS(SkipEM);

	// create latent variable
	TLatentVariable_TreeSwirl latentVariableLadder;
	latentVariableLadder.initialize(_tree, &_sites, _numStatesPerMig, mu_0, Sigma2_0, _outname + "_ladder");

	// run EM: only ladder
	std::vector<size_t> combinations(_tree.numInferredMigEdges(), _numStatesPerMig);
	TEstimatorLadder ladder(_outname, _tree, latentVariableLadder, _distances, _numStatesPerMig, combinations);
	ladder.run(SkipEM, _summaryFile);

	// run EM: with attractor
	if (_tree.fixMigration() || parameters().exists("skipAttractorLoop")) { return; } // no attractor to be tested
	if (parameters().exists("runAttractorWithFixedEmissions")) {
		_runAttractorLoopFixedEmission(SkipEM, ladder, combinations, latentVariableLadder);
	} else {
		_runAttractorsOnBestStates(SkipEM, ladder, combinations, latentVariableLadder);
	}
}

std::vector<size_t> TEstimator::_getAttractorK(size_t k, const std::vector<size_t> &IxSorted,
                                               const std::vector<size_t> &Combinations) {
	const size_t linear_a = IxSorted[k];
	const auto attractor  = coretools::getSubscripts(linear_a, Combinations);
	logfile().number(attractor);

	return attractor;
}

std::vector<std::vector<size_t>> TEstimator::_selectAttractors(const TEstimatorLadder &Ladder,
                                                               const std::vector<size_t> &Combinations) {
	const auto &summedPost = Ladder.getSummedPosteriors();
	const auto ix_sorted   = coretools::rankSort(summedPost, true); // decreasing

	std::vector<std::vector<size_t>> attractors;
	if (parameters().exists("attractorRank")) { // Note: start counting at 1!
		std::vector<size_t> ranks;
		parameters().fill("attractorRank", ranks);
		if (ranks.empty()) { UERROR("Argument 'attractorRank' is empty!"); }

		logfile().startNumbering(
		    "Will run full Baum-Welch with attractor transition matrix on the attractors with ranks ", ranks, ":");
		logfile().addIndent();

		for (const auto &r : ranks) {
			if (r > ix_sorted.size()) {
				UERROR("Attractor ", r, " is not valid for argument 'attractorRank'! Maximum value is ",
				       ix_sorted.size());
			} else if (r == 0) {
				UERROR("Attractor ", r, " is not valid for argument 'attractorRank'! Minimum (best) value is 1.");
			}
			attractors.push_back(_getAttractorK(r - 1, ix_sorted, Combinations));
		}
		logfile().endNumbering();
		logfile().endIndent();

	} else {
		const size_t K = parameters().get("numBestStates", 5);
		logfile().startNumbering("Will run full Baum-Welch with attractor transition matrix on the top ", K,
		                         " states:");
		logfile().addIndent();

		attractors.resize(K);
		for (size_t k = 0; k < K; ++k) { attractors[k] = _getAttractorK(k, ix_sorted, Combinations); }
		logfile().endNumbering();
		logfile().endIndent();
	}

	return attractors;
}

void copyFilesBestAttractor(const std::string &Name, const std::string &End) {
	const auto in  = Name + End;
	const auto out = Name + "_best" + End;
	if (std::filesystem::exists(in)) {
		if (std::filesystem::exists(out)) { std::filesystem::remove(out); } // first remove to avoid error
		std::filesystem::copy(in, out);
	}
}

void TEstimator::_runAttractorsOnBestStates(bool SkipEM, const TEstimatorLadder &Ladder,
                                            const std::vector<size_t> &Combinations,
                                            const TLatentVariable_TreeSwirl &LatentVariableLadder) {
	// run loop the top K attractors while also inferring emission probabilities
	std::vector<std::vector<size_t>> attractors;
	if (parameters().exists("attractors")) { // only run a fixed attractor
		attractors.emplace_back(readAttractors(_numStatesPerMig, _tree.numInferredMigEdges()));
	} else {
		attractors = _selectAttractors(Ladder, Combinations);
	}

	// now run!
	TEstimatorLoopAttractor<TLatentVariable_TreeSwirl> estimatorAttractor(_outname, LatentVariableLadder, _distances,
	                                                                      _numStatesPerMig, Ladder.getFinalKappas(),
	                                                                      Combinations, attractors);
	estimatorAttractor.run(SkipEM, _summaryFile);

	// report
	const auto LLs = estimatorAttractor.LLSurface();
	logfile().list("LL per attractor: ", LLs, ".");
	const size_t ix = std::distance(LLs.begin(), std::max_element(LLs.begin(), LLs.end()));
	const auto a    = attractors[ix];
	logfile().list("Best attractor: ", a);

	// warn user if attractor >= middle state
	for (size_t i = 0; i < _tree.numInferredMigEdges(); ++i) {
		if (a[i] >= _numStatesPerMig / 2) {
			logfile().warning("Genome-wide migration rate for migration edge ", _tree.migEdgeNames()[i],
			                  " is larger than 0.5 (best attractor is ", a[i], " and the number of states is ",
			                  _numStatesPerMig, "). Consider re-defining tree!");
		}
	}

	// warn if there are negative branch lengths for best attractor
	if (estimatorAttractor.negativeBranchLengths()[ix]) {
		logfile().warning(
		    "Inferred negative branch lengths: Your tree might be mis-specified. Consider re-defining tree.");
	}

	// copy files of best attractor to some other, telling name
	const std::string name = _outname + "_attractor_" + coretools::str::concatenateString(a, ",");
	logfile().list("Writing all files for best attractor to prefix '", name, "_best_*'");
	copyFilesBestAttractor(name, "_EMParameters.txt");
	copyFilesBestAttractor(name, "_LL_trace.txt");
	copyFilesBestAttractor(name, "_TransitionParameters.txt");
	copyFilesBestAttractor(name, "_PosteriorProbabilities.txt");
	copyFilesBestAttractor(name, "_PosteriorMeanMigrationRate.txt");
}

void TEstimator::_runAttractorLoopFixedEmission(bool SkipEM, const TEstimatorLadder &Ladder,
                                                const std::vector<size_t> &Combinations,
                                                const TLatentVariable_TreeSwirl &LatentVariableLadder) {
	// run loop over all attractors (or a specific one) while fixing emission probabilities from ladder

	// create all attractor combinations
	std::vector<std::vector<size_t>> attractors;
	if (parameters().exists("attractors")) { // only run a fixed attractor
		attractors.emplace_back(readAttractors(_numStatesPerMig, _tree.numInferredMigEdges()));
	} else {
		const size_t numCombi = coretools::containerProduct(Combinations);
		attractors.resize(numCombi);
		for (size_t i = 0; i < numCombi; ++i) { attractors[i] = coretools::getSubscripts(i, Combinations); }
	}

	// now run!
	TEstimatorLoopAttractor<TLatVarFixed> estimatorAttractor(_outname, LatentVariableLadder, _distances,
	                                                         _numStatesPerMig, Ladder.getFinalKappas(), Combinations,
	                                                         attractors);
	estimatorAttractor.run(SkipEM, _summaryFile);
}
