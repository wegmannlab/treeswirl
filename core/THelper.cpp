//
// Created by madleina on 26.01.24.
//

#include "THelper.h"
#include "coretools/Main/TLog.h"
#include "coretools/Main/TParameters.h"

using namespace coretools::instances;

std::vector<coretools::StrictlyPositive> readKappas(size_t NumMigrations, const std::vector<double> &InitialKappas) {
	std::vector<coretools::StrictlyPositive> kappas;
	if (parameters().exists("kappa")) { // given over command line -> use always
		parameters().fill("kappa", kappas);
	} else if (!InitialKappas.empty()) { // given from a previous initialization
		for (const auto &it : InitialKappas) { kappas.emplace_back(it); }
	} else { // take default values
		kappas.resize(NumMigrations, 0.0001);
	}

	if (kappas.size() == 1 && NumMigrations > 1) {
		kappas.assign(NumMigrations, kappas[0]);
	} else if (kappas.size() != NumMigrations) {
		UERROR("Listed kappas  do not match number of migrations from treeFile (", NumMigrations,
		       "). Provide all values based on number of migrations: kappa=0.0001,0.0001,0.00008 or "
		       "kappa=0.0001{2},0.00008");
	}

	logfile().list("kappas = ", kappas);
	return kappas;
}

std::vector<coretools::Probability> readPhi(size_t NumMigrations) {
	std::vector<coretools::Probability> phis;
	std::vector<coretools::Probability> tmp(NumMigrations, coretools::P(0.9));
	parameters().fill("phi", phis, tmp);

	if (phis.size() == 1 && NumMigrations > 1) {
		phis.assign(NumMigrations, phis[0]);
	} else if (phis.size() != NumMigrations) {
		UERROR("Listed phis  do not match number of migrations from treeFile (", NumMigrations,
		       "). Provide all values based on number of migrations: phi=0.019,0.019,0.04 or phi=0.019{2},0.04");
	}

	logfile().list("phis = ", phis);
	return phis;
}

std::vector<coretools::Probability> readZeta(size_t NumMigrations) {
	std::vector<coretools::Probability> zetas;
	std::vector<coretools::Probability> tmp(NumMigrations, coretools::P(0.1));
	parameters().fill("zeta", zetas, tmp);

	if (zetas.size() == 1 && NumMigrations > 1) {
		zetas.assign(NumMigrations, zetas[0]);
	} else if (zetas.size() != NumMigrations) {
		UERROR("Listed zetas  do not match number of migrations from treeFile (", NumMigrations,
		       "). Provide all values based on number of migrations: zeta=0.92,0.92,0.8 or zeta=0.92{2},0.8");
	}

	logfile().list("zetas = ", zetas);
	return zetas;
}

std::vector<size_t> readAttractors(NumStatesType NumStates, NumMigEdgeType NumMigrations) {
	std::vector<size_t> tmp(NumMigrations, (NumStates - 1) / 2);
	std::vector<size_t> attractors;
	parameters().fill("attractors", attractors, tmp);

	if ((attractors.size() == 1 && NumMigrations > 1) && attractors[0] < NumStates) {
		attractors.assign(NumMigrations, attractors[0]);
	} else if (attractors.size() != NumMigrations) {
		UERROR("Listed attractors do not match number of migrations from treeFile '", NumMigrations,
		       "'. Provide all values based on number of migrations: attractors=1,1,2 or attractors=1{2},2");
	} else {
		for (const auto &attractor : attractors) {
			if (attractor >= NumStates)
				UERROR("Attractors cannot be equal or bigger that the number of states '", NumStates, "'");
		}
	}

	logfile().list("attractors = ", attractors);
	return attractors;
}

std::vector<double> getInitialTransitionMatrixParameters(bool WithAttractor, size_t NumMigrations,
                                                         const std::vector<double> &InitialKappas) {
	auto kappas = readKappas(NumMigrations, InitialKappas);

	if (WithAttractor) {
		auto phis  = readPhi(NumMigrations);
		auto zetas = readZeta(NumMigrations);

		std::vector<double> transitionMatrixParameters;
		transitionMatrixParameters.reserve(kappas.size() * 3);
		for (size_t i = 0; i < kappas.size(); i++) {
			transitionMatrixParameters.push_back(kappas[i]);
			transitionMatrixParameters.push_back(phis[i]);
			transitionMatrixParameters.push_back(zetas[i]);
		}
		return transitionMatrixParameters;
	} else {
		std::vector<double> transitionMatrixParameters;
		transitionMatrixParameters.reserve(kappas.size());
		for (auto &kappa : kappas) { transitionMatrixParameters.push_back(kappa); }
		return transitionMatrixParameters;
	}
}

bool doSQUAREM() { return !parameters().exists("noSQUAREM"); }

void setAttractor(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers, const std::vector<size_t> &Ix) {
	for (size_t i = 0; i < Optimizers.size(); ++i) {
		std::dynamic_pointer_cast<TSTransMatAttractor>(Optimizers[i])->setAttractorIx(Ix[i]);
	}
}

bool fixTransitionMatrixParams() { return parameters().exists("fixTransitionMatrixParameters"); }

bool writeTempFiles() { return parameters().exists("writeTempFiles"); }

std::vector<std::string> createHeaderTransitionMatrix(size_t NumMigrations, bool WithAttractor) {
	std::vector<std::string> header{};
	for (size_t m = 0; m < NumMigrations; m++) {
		header.push_back("kappa" + coretools::str::toString(m));
		if (WithAttractor) {
			header.push_back("phi" + coretools::str::toString(m));
			header.push_back("zeta" + coretools::str::toString(m));
		}
	}

	return header;
}

void setTransitionMatrices(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers, TSTransMat &TransitionMatrices,
                           const std::vector<double> &InitialParameters) {
	const NumMigEdgeType NumMigrations = Optimizers.size();

	// set initial values of kappas (and potentially phi and zeta)
	TransitionMatrices.setValuesEM(InitialParameters);

	// turn on reporting
	TransitionMatrices.report();

	// fix values in EM?
	if (fixTransitionMatrixParams()) {
		std::vector<bool> fix(NumMigrations, true);
		TransitionMatrices.fixTransitionMatricesDuringEM(fix, InitialParameters);
	}
}

void initializeTransitionMatrices(TSTransMat &TransitionMatrices, genometools::TDistancesBinnedBase *Distances,
                                  bool WithAttractor, const std::string &OutName,
                                  const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers,
                                  const std::vector<double> &InitialKappasPhisZetas) {

	std::string name                = OutName + "_TransitionParameters.txt";
	std::vector<std::string> header = createHeaderTransitionMatrix(Optimizers.size(), WithAttractor);
	TransitionMatrices.initialize(Distances, Optimizers, name, header);

	setTransitionMatrices(Optimizers, TransitionMatrices, InitialKappasPhisZetas);

	TransitionMatrices.setInitialization(stattools::TypeTransMatInitialization::none);
}

double runBaumWelch(TSTransMat &TransitionMatrices, TLatVarBase &LatentVariable,
                    const std::vector<NumLociType> &ChunkEnds, const std::string &OutName, size_t MaxNumIter,
                    double MinDeltaLL, bool EstimatePosteriors, bool SkipEM, bool DoSQUAREM) {
	// create HMM object
	stattools::THMM<double, NumStatesType, NumLociType> hmm(TransitionMatrices, LatentVariable, MaxNumIter, MinDeltaLL,
	                                                        true);
	hmm.report();

	// do some smoothing on smart initialization (can be changed via command line)
	hmm.getLatVarBeta().setHalfWindowSize(20);

	// run Baum-Welch to estimate parameters
	double LL = 0.0;
	if (!SkipEM) {
		const size_t numRep  = 1;
		std::string filename = "";
		if (writeTempFiles()) { filename = OutName + "_LL_trace.txt"; } // write LL trace
		if (DoSQUAREM) {
			LL = hmm.runSQUAREM(ChunkEnds, numRep, filename);
		} else {
			LL = hmm.runEM(ChunkEnds, numRep, filename);
		}
	}

	// estimating Posteriors
	if (EstimatePosteriors) {
		std::string filename = OutName + "_PosteriorProbabilities.txt";
		LL                   = hmm.estimateStatePosteriors(ChunkEnds, filename);
	}

	return LL;
}