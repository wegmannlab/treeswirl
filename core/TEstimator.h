//
// Created by creyna on 10.03.21.
//

#ifndef TREESWIRL2_TESTIMATOR_H
#define TREESWIRL2_TESTIMATOR_H

#define ARMA_WARN_LEVEL 1
#include <armadillo>

#include "TEstimatorLadder.h"
#include "TInitializer_Tree.h"
#include "TLatentVariable_TreeSwirl.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

class TEstimator {
private:
	genometools::TDistancesBinned<NumDistanceGroupType> *_distances = nullptr;
	std::string _outname{};

	size_t _numStatesPerMig = 0;

	TTree _tree;
	TGenome _sites;

	coretools::TOutputFile _summaryFile;

	// initialization
	void _initializeTree();
	void _initializeSites();
	void _openSummaryFile();

	static std::vector<std::vector<size_t>> _selectAttractors(const TEstimatorLadder &Ladder,
	                                                          const std::vector<size_t> &Combinations);
	static std::vector<size_t> _getAttractorK(size_t k, const std::vector<size_t> &IxSorted,
	                                   const std::vector<size_t> &Combinations);
	void _runAttractorsOnBestStates(bool SkipEM, const TEstimatorLadder &Ladder,
	                                const std::vector<size_t> &Combinations,
	                                const TLatentVariable_TreeSwirl &LatentVariable);
	void _runAttractorLoopFixedEmission(bool SkipEM, const TEstimatorLadder &Ladder,
	                                    const std::vector<size_t> &Combinations,
	                                    const TLatentVariable_TreeSwirl &LatentVariable);

	// run EMs
	void _run(bool SkipEM);
	auto _runGaussianMixtureRSS(bool SkipEM);

public:
	explicit TEstimator();
	~TEstimator() = default;

	void infer();
	void calculateLL();
};
#endif // TREESWIRL2_TESTIMATOR_H
