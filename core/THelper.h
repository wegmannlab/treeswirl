//
// Created by madleina on 26.01.24.
//

#ifndef TREESWIRL_THELPER_H
#define TREESWIRL_THELPER_H
#include "TreeSwirlSizeTypes.h"
#include "coretools/Types/commonWeakTypes.h"
#include "coretools/Types/probability.h"

// read from command line
std::vector<coretools::StrictlyPositive> readKappas(size_t NumMigrations,
                                                    const std::vector<double> &InitialKappas = {});
std::vector<coretools::Probability> readPhi(size_t NumMigrations);
std::vector<coretools::Probability> readZeta(size_t NumMigrations);
std::vector<size_t> readAttractors(NumStatesType NumStates, NumMigEdgeType NumMigrations);
std::vector<double> getInitialTransitionMatrixParameters(bool TransitionMatrixHasAttractor, size_t NumMigrations,
                                                         const std::vector<double> &InitialKappas = {});
bool doSQUAREM();

// fix?
bool fixTransitionMatrixParams();

// output?
bool writeTempFiles();

// initialize transition matrices
void setAttractor(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers, const std::vector<size_t> &Ix);
std::vector<std::string> createHeaderTransitionMatrix(size_t NumMigrations, bool WithAttractor);
void setTransitionMatrices(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers, TSTransMat &TransitionMatrices,
                           const std::vector<double> &InitialKappasPhisZetas);

void initializeTransitionMatrices(TSTransMat &TransitionMatrices, genometools::TDistancesBinnedBase *Distances,
                                  bool WithAttractor, const std::string &OutName,
                                  const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers,
                                  const std::vector<double> &InitialKappasPhisZetas);

double runBaumWelch(TSTransMat &TransitionMatrices, TLatVarBase &LatentVariable,
                    const std::vector<NumLociType> &ChunkEnds, const std::string &OutName, size_t MaxNumIter,
                    double MinDeltaLL, bool EstimatePosteriors, bool SkipEM, bool DoSQUAREM);

#endif // TREESWIRL_THELPER_H
