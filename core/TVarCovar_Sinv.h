//
// Created by creyna on 18.03.21.
//

#ifndef TREESWIRL2_TVARCOVAR_SINV_H
#define TREESWIRL2_TVARCOVAR_SINV_H

#include "TSigma.h"
#include "TVarCovar_W.h"

class TVarCovar_Sinv {
private:
	arma::Mat<double> _S_inverse;
	double _log_determinant_S_inverse{};

public:
	TVarCovar_Sinv(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2);
	TVarCovar_Sinv()  = default;
	~TVarCovar_Sinv() = default;

	void initialize(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2);

	void update(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2);

	[[nodiscard]] const arma::Mat<double> &Sinv() const { return _S_inverse; };

	[[nodiscard]] const double &Sinv(NumPopType Index_m, NumPopType Index_n) const {
		return _S_inverse(Index_m, Index_n);
	};

	[[nodiscard]] const double &logDetSinv() const { return _log_determinant_S_inverse; };
};

class TVarCovars_Sinv {
private:
	NumStatesType _numStates_J{};
	NumSigmaType _numSigmas{};
	double _sigma2{};

	std::vector<TVarCovar_Sinv> _varCovars_S_inverse{};

public:
	TVarCovars_Sinv(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws, const double &PriorVar_sigma2);
	TVarCovars_Sinv()  = default;
	~TVarCovars_Sinv() = default;

	void initialize(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws, const double &PriorVar_sigma2);

	// update all
	void update(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws);
	void update(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws, const double &PriorVar_sigma2);

	const TVarCovar_Sinv &operator()(const NumStatesType &Index_J, const NumSigmaType &Index_Sigma) const;
	TVarCovar_Sinv &operator()(const NumStatesType &Index_J, const NumSigmaType &Index_Sigma); // to update S matrices

	[[nodiscard]] const arma::Mat<double> &getSinv(const NumStatesType &Index_J,
	                                               const NumSigmaType &Index_Sigma) const {
		uint16_t index = Index_Sigma * _numStates_J + Index_J;
		return _varCovars_S_inverse[index].Sinv();
	}
	[[nodiscard]] const double &getLogDetSinv(const NumStatesType &Index_J, const NumSigmaType &Index_Sigma) const {
		uint16_t index = Index_Sigma * _numStates_J + Index_J;
		return _varCovars_S_inverse[index].logDetSinv();
	}

	//[[nodiscard]] const NumStatesType &numStates_J() const { return _numStates_J; }
	[[nodiscard]] const NumSigmaType &numSigmas() const { return _numSigmas; }
	[[nodiscard]] const double &sigma2() const { return _sigma2; } //
};

#endif // TREESWIRL2_TVARCOVAR_SINV_H
