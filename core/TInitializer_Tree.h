//
// Created by creyna on 07.09.22.
//

#ifndef TREESWIRL2_TINITIALIZER_TREE_H
#define TREESWIRL2_TINITIALIZER_TREE_H

#include "TLatentVariable_initialTree.h"
#include "TNelderMeadRSS.h"
#include "stattools/EM/TEM.h"
#include "stattools/MLEInference/TNelderMead.h"
#include "stattools/Priors/TPriorCategorical.h"
#include "stattools/Priors/TPriorUniform.h"

struct GaussianMixtureResults {
	size_t numClasses = 0;
	bool passed       = false;
	double rss        = std::numeric_limits<double>::max();
	double mu_0       = 0.0;
	double sigma2_0   = 0.0;
	std::vector<double> branchLengths;
	std::vector<double> pis;

	GaussianMixtureResults() = default;
	GaussianMixtureResults(bool Passed) { passed = Passed; };
	GaussianMixtureResults(bool Passed, double RSS, double Mu_0, double Sigma2_0,
	                       const std::vector<double> &BranchLengths, size_t NumClasses,
	                       const std::vector<double> &Pis) {
		passed        = Passed;
		rss           = RSS;
		mu_0          = Mu_0;
		sigma2_0      = Sigma2_0;
		branchLengths = BranchLengths;
		numClasses    = NumClasses;
		pis           = Pis;
	};
};

class TInitializer_Tree {
private:
	TTree *_tree;
	const TGenome *_sites;

	std::string _outname;

	double _mu_0;
	double _sigma2_0;
	bool _fix_mu_0;
	bool _fix_sigma2_0;

	void _run();
	void _writeInitialEmissionsParameters();
	void _initFromFile(std::string_view Filename);
	std::pair<double, std::vector<double>> _runRSS(const std::vector<arma::Col<double>> &Y,
	                                               const std::vector<double> &Pis);
	void _setMuSigma(double Mu0, double Sigma0);
	bool _checkOneGaussianMixture(const std::vector<double> &Pis, double ThresholdMinPi) const;

	GaussianMixtureResults _runOneClass(size_t NumClasses, double ThresholdMinPi, size_t NumRep);

public:
	TInitializer_Tree(TTree *Tree, const TGenome *Sites, const std::string &Outname);
	~TInitializer_Tree() = default;

	void run(bool SkipEM);

	[[nodiscard]] double sigma2_0() const { return _sigma2_0; }
	[[nodiscard]] double mu_0() const { return _mu_0; }
};

#endif // TREESWIRL2_TINITIALIZER_TREE_H
