//
// Created by creyna on 25.05.22.
//

#ifndef TREESWIRL2_TSIMULATOR_H
#define TREESWIRL2_TSIMULATOR_H

#include "TGenome.h"
#include "TVarCovar_W.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

class TSimulator {
private:
	std::string _outname{};
	std::vector<NumStatesType> _z{};
	genometools::TDistancesBinned<NumDistanceGroupType> _distances;

	std::vector<double> _kappasPhisZetas;
	TTree _tree;
	NumStatesType _numStates_J;

	// which transition matrix to use?
	bool _transitionMatrixHasAttractor = true;

	void _runSimulations();
	void _writeW(const TVarCovars_W &W);

	void _simulate_z(NumStatesType NumStates_J, NumMigEdgeType NumMigrations, const std::vector<double> &KappasNusMus);
	static arma::Col<double> _simulateData(const arma::Mat<double> &W, const double &Mu_tilde);

	void _simulateCounts(const arma::Mat<double> &W, double Mu_tilde, std::vector<NumSamplesType> N,
	                     std::vector<coretools::Probability> ProbN, coretools::TOutputFile &Output);
	void _simulateRaw(const arma::Mat<double> &W, double Mu_tilde, std::vector<NumSamplesType> N,
	                  coretools::TOutputFile &Output, coretools::TOutputFile &OutputRaw);

	void _setDistances();

	void _setAttractorIx(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers, const NumMigEdgeType &Index_M,
	                     const NumStatesType &Index_J);

	static std::vector<NumSamplesType> _setMaxN(const NumPopType &NumPopulations);
	static std::vector<coretools::Probability> _setProbN(const NumPopType &NumPopulations);

public:
	explicit TSimulator();
	~TSimulator() = default;

	void simulate();
	void printW();
};

#endif // TREESWIRL2_TSIMULATOR_H
