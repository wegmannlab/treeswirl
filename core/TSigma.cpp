//
// Created by creyna on 16.03.21.
//

#include "TSigma.h"
using namespace coretools::instances;

//------------------------------------------------
// N vector
//------------------------------------------------
TNVector::TNVector(const std::vector<double> &SampleSizes_N) {
	_N      = SampleSizes_N;
	_counts = 1;

	_setDiagOneDivN();
}

void TNVector::_setDiagOneDivN() {
	arma::vec tmp(_N.size());
	for (size_t i = 0; i < _N.size(); ++i) { tmp[i] = 1.0 / _N[i]; }

	_diagOneDivN = arma::diagmat(tmp);
}

TNVector::TNVector(std::vector<double> SampleSizes_N, const NumLociType &Counts)
    : _N{std::move(SampleSizes_N)}, _counts{Counts} {
	_setDiagOneDivN();
}

TNVector TNVector::operator*(const TNVector &other) const {
	// merge two N vectors using weights
	std::vector<double> sampleSize_N(_N.size());
	NumLociType counts = _counts + other.counts();
	for (size_t m = 0; m < _N.size(); m++) {
		sampleSize_N[m] = (_N[m] * _counts + other._N[m] * other._counts) / counts;
	}

	return TNVector{sampleSize_N, counts};
}

//------------------------------------------------
// N vectors
//------------------------------------------------

NumLociType TNVectors::addNVector(const std::vector<double> &SampleSizes_N) { // tested
	_NVectors.emplace_back(SampleSizes_N);
	TNVectorPointer ptr(_NVectors.back(),
	                    _NVectors.size() - 1); // reserve first to avoid memory reallocation and segmentations
	auto it = _orderedSet.insert(ptr);

	// if vector not inserted
	if (!it.second) {
		_NVectors.pop_back();
		// increase counts
		++_NVectors[it.first->index()]; //(*it.first)++; non mutable set values
	}
	return it.first->index();
}

NumLociType TNVectors::findMinWeightIndex() const { // tested
	auto it = std::min_element(_NVectors.begin(), _NVectors.end(), [](const TNVector &First, const TNVector &Second) {
		return First.counts() < Second.counts();
	});
	return std::distance(_NVectors.begin(), it);
}

double TNVectors::calcEuclideanDistance(const NumLociType &Index_First,
                                        const NumLociType &Index_Second) const { // tested
	std::vector<double> auxiliary;
	std::transform(_NVectors[Index_First].getN().begin(), _NVectors[Index_First].getN().end(),
	               _NVectors[Index_Second].getN().begin(), std::back_inserter(auxiliary),
	               [](double element1, double element2) { return (element1 - element2) * (element1 - element2); });

	return std::sqrt(std::accumulate(auxiliary.begin(), auxiliary.end(), 0.0));
}

TNVector TNVectors::merge(const NumLociType &Index_First, const NumLociType &Index_Second) {

	std::vector<double> sampleSize_N(_NVectors[Index_First].size());
	NumLociType counts = _NVectors[Index_First].counts() + _NVectors[Index_Second].counts();
	for (NumPopType m = 0; m < _NVectors[Index_First].size(); m++) {
		sampleSize_N[m] = (_NVectors[Index_First][m] * _NVectors[Index_First].counts() +
		                   _NVectors[Index_Second][m] * _NVectors[Index_Second].counts()) /
		                  counts;
	}

	return TNVector{sampleSize_N, counts};
}

//------------------------------------------------
// TSigmas
//------------------------------------------------
void TSigmas::clusterNVectors(NumSigmaType maxSigmaConfigurations) {
	logfile().addIndent();
	if (maxSigmaConfigurations <= 0) {
		UERROR("The max number of Sigma configurations must be >= 1");
	} else if (numSigmas() > maxSigmaConfigurations) {
		logfile().list("Clustering Sigmas ( vector of sampleSizes for each site)... ");
		while (numSigmas() > maxSigmaConfigurations) {

			NumLociType index_minWeight = _findMinWeightIndex();
			NumLociType index_minDist{};
			double minDist{};

			if (index_minWeight != 0) {
				index_minDist = 0;
				minDist       = _calcEuclideanDistance(index_minWeight, index_minDist);
			} else {
				index_minDist = 1;
				minDist       = _calcEuclideanDistance(index_minWeight, index_minDist);
			}

			for (NumSigmaType s = index_minDist + 1; s < numSigmas(); s++) {
				if (s != index_minWeight) {
					double dist = _calcEuclideanDistance(index_minWeight, s);
					if (dist < minDist) {
						minDist       = dist;
						index_minDist = s;
					}
				}
			}

			_merge(index_minWeight, index_minDist);
		}
		logfile().endIndent("Number of Sigmas after clustering = " + coretools::str::toString(numSigmas()));
	}

	// weighted avg Nvector to initialize tree
	_mergeAll();
}

void TSigmas::_merge(NumLociType Index_First, NumLociType Index_Second) {
	// merge by calculating the average between 2 Nvectors considering their counts, assign new vector to the one with
	// minimum distance... Second = First * Second;

	_NVectors[Index_Second] = _NVectors.merge(Index_First, Index_Second);

	// this part tested
	//  remove Nvector with minimum weight
	_NVectors.getNVectors().erase(_NVectors.getNVectors().begin() + Index_First);

	// reassign values that point to NVectors in NVectorsIndex
	auto it   = std::find_if(_NVectorsIndex.begin(), _NVectorsIndex.end(),
	                         [&Index_First](NumLociType l) { return l == Index_First || l > Index_First; });
	bool flag = Index_Second > Index_First;
	while (it != _NVectorsIndex.end()) {
		NumLociType index = std::distance(_NVectorsIndex.begin(), it);
		if (_NVectorsIndex[index] == Index_First) { _NVectorsIndex[index] = Index_Second; }
		if ((_NVectorsIndex[index] > Index_First) || flag) { --_NVectorsIndex[index]; }

		it = std::find_if(std::next(it), _NVectorsIndex.end(),
		                  [&Index_First](NumLociType l) { return l == Index_First || l > Index_First; });
	}
}

const TNVector &TSigmas::operator[](NumSigmaType Index_Sigma) const {
	assert(Index_Sigma < _NVectors.size());
	return _NVectors[Index_Sigma];
}
