//
// Created by madleina on 13.02.24.
//

#include "TEstimatorLadder.h"
#include "THelper.h"
#include "stattools/HMM/TLatentVariableFixed.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TTransitionMatrixDist.h"

using namespace coretools::instances;

TEstimatorLadder::TEstimatorLadder(const std::string &OutName, const TTree &Tree,
                                   TLatentVariable_TreeSwirl &LatentVariable,
                                   genometools::TDistancesBinned<NumDistanceGroupType> *Distances,
                                   size_t NumStatesPerMig, const std::vector<size_t> &Combinations)
    : _tree(Tree), _distances(Distances), _latentVariable(LatentVariable), _numStatesPerMig(NumStatesPerMig),
      _combinations(Combinations) {

	_outname = OutName + "_ladder";
}

std::vector<std::shared_ptr<TSOptimizer>> TEstimatorLadder::_createOptimizer() {
	std::vector<std::shared_ptr<TSOptimizer>> optimizers(_tree.numInferredMigEdges());

	// use numStates provided by user not the one from W (can vary for multiple migrations)
	for (NumMigEdgeType i = 0; i < _tree.numInferredMigEdges(); i++) {
		optimizers[i] = std::make_shared<TSTransMatLadder>(_numStatesPerMig);
	}
	return optimizers;
}

void TEstimatorLadder::_readKappasFromFile(std::string_view Filename) {
	// file format: only contain kappas (one per column), as written by stattools
	coretools::TInputFile file(Filename, coretools::FileType::Header);
	std::string tmp;
	for (; !file.empty(); file.popFront()) { tmp = file.frontRaw(); } // go to end of file
	coretools::str::TSplitter spl(tmp, '\t');
	_finalKappas.clear();
	for (const auto s : spl) { _finalKappas.emplace_back(coretools::str::fromStringCheck<double>(s)); }
	logfile().list("Initial kappas: ", _finalKappas);
}

void TEstimatorLadder::_readPosteriorSurfaceFromFile(std::string_view Filename) {
	// file format: one row per migration edges + posterior in last column
	_summedPosteriors.clear();
	coretools::TInputFile file(Filename, coretools::FileType::Header);
	for (; !file.empty(); file.popFront()) {
		size_t column = _tree.numInferredMigEdges();
		_summedPosteriors.push_back(file.get<double>(column));
	}
	if (_summedPosteriors.size() != coretools::containerProduct(_combinations)) {
		UERROR("Number of posterior values in file ", Filename, " (", _summedPosteriors.size(),
		       ") does not match expected number (", coretools::containerProduct(_combinations), ")!");
	}
}

void TEstimatorLadder::_readLatentVariable(std::string_view Filename) { _latentVariable.initializeFromFile(Filename); }

std::string findFile(std::string_view Path, std::string_view Extension) {
	for (auto &p : std::filesystem::recursive_directory_iterator(Path)) {
		const auto f = p.path().string();
		std::cout << f << std::endl;
		if (coretools::str::stringContains(f, Extension)) { return f; }
	}
	UERROR("No file found in path ", Path, " with extension ", Extension, ".");
}

void TEstimatorLadder::_initFromFile(std::string_view Path) {
	logfile().list("Skipping Baum-Welch on attractor-free transition matrix, initializing all parameters from path ",
	               Path, "'.");

	// kappas
	_readKappasFromFile(findFile(Path, "ladder_TransitionParameters.txt"));

	// posterior surface
	_readPosteriorSurfaceFromFile(findFile(Path, "_statePosteriorSurface.txt"));

	// latent variable
	_readLatentVariable(findFile(Path, "ladder_EMParameters.txt"));
}

void TEstimatorLadder::_run(bool SkipEM) {
	logfile().startIndent("Running Baum-Welch on attractor-free transition matrix...");

	// create optimizers and transition matrix
	const bool withAttractor = false;
	auto optimizers          = _createOptimizer();
	TSTransMat transitionMatrices;
	auto initKappa = getInitialTransitionMatrixParameters(withAttractor, _tree.numInferredMigEdges());
	initializeTransitionMatrices(transitionMatrices, _distances, withAttractor, _outname, optimizers, initKappa);

	if (parameters().exists("printLL")) { _latentVariable.printLLInNewtonRaphson(&transitionMatrices, _distances); }

	// run EM!
	size_t numIter                     = parameters().get("maxNumIterLadder", 1000);
	double deltaLL                     = parameters().get("deltaLLLadder", 1e-05);
	const bool estimatePosterior       = true;
	std::vector<NumLociType> chunkEnds = _distances->getChunkEnds<NumLociType>();
	_finalLL = runBaumWelch(transitionMatrices, _latentVariable, chunkEnds, _outname, numIter, deltaLL,
	                        estimatePosterior, SkipEM, doSQUAREM());

	// get kappa
	_finalKappas = transitionMatrices.getFinalParameterEstimatesEM();
	_calculateSummedPosteriors();

	// report to logfile
	logfile().list("Final estimate of kappa(s) of transition matrix: ", _finalKappas, ".");
	logfile().endIndent("Finished Baum-Welch on attractor-free transition matrix!");
}

std::vector<std::string> TEstimatorLadder::_createHeader(std::string_view Name) {
	std::vector<std::string> header;
	for (size_t i = 0; i < _tree.numInferredMigEdges(); ++i) {
		header.emplace_back("attractor_" + coretools::str::toString(i));
	}
	header.emplace_back(Name);
	return header;
}

void TEstimatorLadder::_calculateSummedPosteriors() {
	// read estimated state posterior probabilities
	// number of columns = number of possible attractor combinations + 2 (for Index and Rep)
	coretools::TInputFile file(_outname + "_PosteriorProbabilities.txt", coretools::FileType::Header);

	const size_t numStates = file.numCols() - 2;
	_summedPosteriors.resize(numStates, 0.0);
	for (; !file.empty(); file.popFront()) { // loop over all loci
		for (size_t j = 0; j < numStates; j++) { _summedPosteriors[j] += file.get<double>(2 + j); }
	}

	// normalize
	for (auto &s : _summedPosteriors) { s /= (double)file.curLine(); }

	// write to output
	coretools::TOutputFile out(_outname + "_statePosteriorSurface.txt", _createHeader("posterior"));
	for (size_t i = 0; i < numStates; ++i) {
		const auto cur_a = coretools::getSubscripts(i, _combinations);
		out.write(cur_a);
		out << _summedPosteriors[i] << coretools::endl;
	}
}

void TEstimatorLadder::run(bool SkipEM, coretools::TOutputFile &SummaryFile) {
	// run HMM on ladder transition matrix
	// use TreeSwirl latent variable
	if (parameters().exists("skipLadder")) { return; }

	if (parameters().exists("pathLadder")) {
		_initFromFile(parameters().get("pathLadder"));
	} else {
		_run(SkipEM);
	}

	// write to summary file
	SummaryFile << "no_attractor" << _finalLL << _latentVariable.mu() << _latentVariable.sigma2();
	for (auto b : _tree.branchLengths_c()) { SummaryFile << b; }
	for (size_t i = 0; i < _tree.numInferredMigEdges(); ++i) {
		SummaryFile << _finalKappas[i] << "NA" // NA for phi
		            << "NA"                    // NA for zeta
		            << "NA";                   // NA for attractor
	}
	SummaryFile.endln();
}