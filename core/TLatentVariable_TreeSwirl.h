//
// Created by creyna on 06.04.21.
//

#ifndef TREESWIRL2_TLATENTVARIABLE_TREESWIRL_H
#define TREESWIRL2_TLATENTVARIABLE_TREESWIRL_H

#include "TGenome.h"
#include "TVarCovar_Sinv.h"
#include "stattools/EM/TLatentVariable.h"
#include "stattools/MLEInference/TNelderMead.h"
#include "stattools/MLEInference/TNewtonRaphson.h"

#define LTWO_PI 1.837877066409345339082 // = log(2*pi)

//------------------------------------------------
// TLatentVariable_TreeSwirl
//------------------------------------------------
class TLatentVariable_TreeSwirl : public stattools::TLatentVariable<double, NumStatesType, NumLociType> {
private:
	TTree _tree;
	const TGenome *_sites = nullptr;
	TVarCovars_W _varCovarsW;
	TVarCovars_Sinv _varCovarsSinv;
	double _mu = 0.0;

	arma::Mat<double> _ones;
	arma::Col<double> _dmu;
	std::string _outname;
	coretools::TOutputFile _traceEM;
	coretools::TOutputFile _fileDetHessian;
	coretools::TOutputFile _filePostMeanMigRate;
	coretools::TOutputFile _timerNR;

	std::vector<double> _gammas;
	coretools::TDimension<3> _dimF;
	coretools::TDimension<2> _dimGamma;

	bool _update = true;

	// strategies for calculating Hessian
	bool _approxHessian                    = false;
	coretools::ZeroOneClosed _alphaHessian = 0.0;
	std::vector<std::array<size_t, 3>> _relevantHessianIx;
	size_t _counterFirstDerivative  = 0;
	size_t _counterSecondDerivative = 0;

	// debugging Newton-Raphson
	bool _printLLInNewtonRaphson                                    = false;
	TSTransMat *_transitionMatrices                                 = nullptr;
	genometools::TDistancesBinned<NumDistanceGroupType> *_distances = nullptr;

	// debugging: Nelder-Mead
	size_t _iter                                        = 0;
	stattools::TSimplex<> _simplexOfPreviousEMIteration = {};

	void _openEMTrace();
	void _writeEMTrace();
	void _openPostMeanMigRateFile();
	void _writeToPostMeanMigRateFile(NumLociType Index,
	                                 const stattools::TDataVector<double, NumStatesType> &StatePosteriorProbabilities);
	void _openFileDetHessian();
	void _readCommandLineParamsHessian();

	// update parameters
	void _updateParameters();
	void _doNewtonRaphson(const std::vector<double> &VecParameters);

	bool _updateParametersNR(coretools::TConstView<double> VecParameters);
	std::pair<std::vector<double>, bool> _calcFirstDerivative(coretools::TConstView<double> VecParameters,
	                                                          size_t Iterations);
	arma::Mat<double> _calcSecondDerivative(coretools::TConstView<double> VecParameters,
	                                        coretools::TConstView<double> ValuesF, size_t Iterations);
	std::pair<std::vector<double>, std::vector<arma::Mat<double>>> _calcKsInvTrace();
	double _parallelSum2ndDerivative(size_t cumSumK, const std::vector<double> &trace_Kik,
	                                 const std::vector<arma::Mat<double>> &KsSinv);
	std::pair<std::vector<arma::mat>, std::vector<double>> _calcTSquare();
	double _calcFirstDerivativeSigma2();
	double _calcSecondDerivativeSigma2();
	std::pair<std::vector<double>, bool> _calculateF();
	double _calculateGradient_k(size_t k, const std::vector<double> &trace_SinvJ,
	                            const std::vector<arma::Mat<double>> &SinvJSinv);

	// Hessian
	void _fillIxApproxHessian();
	void _fillHessianFull(arma::Mat<double> &hessian_Fc, const std::vector<double> &trace_Kik,
	                      const std::vector<arma::Mat<double>> &KsSinv);
	void _fillHessianApprox(arma::Mat<double> &hessian_Fc, const std::vector<double> &trace_Kik,
	                        const std::vector<arma::Mat<double>> &KsSinv);

	// debugging: Nelder-Mead
	void _doNelderMead(const std::vector<double> &VecParameters);
	double _calcMinusQ(coretools::TConstView<double> Values);

public:
	TLatentVariable_TreeSwirl()           = default;
	~TLatentVariable_TreeSwirl() override = default;

	void initialize(const TTree &Tree, const TGenome *Sites, size_t NumStatesPerMig, double Mu, double Sigma2_0,
	                const std::string &Outname);
	void initializeFromFile(std::string_view Filename);

	std::vector<double> getParameters() const override;
	bool setParameters(coretools::TConstView<double> Params) override;

	void calculateEmissionProbabilities(NumLociType Index,
	                                    stattools::TDataVector<double, NumStatesType> &Emission) const override;

	[[nodiscard]] double emissionProbability(NumLociType l, NumStatesType j, const arma::Col<double> &DMu) const;

	// EM functions
	void prepareEMParameterEstimationInitial() override;
	void prepareEMParameterEstimationOneIteration() override; // set to 0s every iteration
	void handleEMParameterEstimationOneIteration(NumLociType Index,
	                                             const stattools::TDataVector<double, NumStatesType> &Weights) override;
	void finalizeEMParameterEstimationOneIteration() override;
	void finalizeEMParameterEstimationFinal() override;

	// write state posteriors
	void handleStatePosteriorEstimationAndWriting(
	    NumLociType Index, const stattools::TDataVector<double, NumStatesType> &StatePosteriorProbabilities,
	    coretools::TOutputFile &Out) override;
	void prepareStatePosteriorEstimationAndWriting(NumStatesType _numStates_J,
	                                               std::vector<std::string> &header) override;

	void reportEMParameters() override;
	void printLLInNewtonRaphson(TSTransMat *TransitionMatrices,
	                            genometools::TDistancesBinned<NumDistanceGroupType> *Distances);

	[[nodiscard]] double mu() const { return _mu; }
	[[nodiscard]] double sigma2() const { return _varCovarsSinv.sigma2(); }
	[[nodiscard]] const TVarCovars_Sinv &Sinv() const { return _varCovarsSinv; }
	[[nodiscard]] const TVarCovars_W &varCovarsW() const { return _varCovarsW; }
	[[nodiscard]] const TGenome *sites() const { return _sites; }
	[[nodiscard]] const TTree &tree() const { return _tree; }
};

#endif // TREESWIRL2_TLATENTVARIABLE_TREESWIRL_H
