//
// Created by creyna on 16.03.21.
//

#ifndef TREESWIRL2_TGENOME_H
#define TREESWIRL2_TGENOME_H

#include "TSigma.h"
#include "coretools/Distributions/TBetaDistr.h"
#include "coretools/Storage/TDataFile.h"
#include "genometools/GenomePositions/TDistances.h"
#include "genometools/GenomePositions/TNamesPositions.h"
#define PI_HALF 1.570796

class TLocus {
private:
	arma::Col<double> _data{};
	std::vector<double> _freqData{};

public:
	explicit TLocus(const arma::Col<double> &Data, bool Transform);
	TLocus(const arma::Col<double> &Data, coretools::StrictlyPositive Concentration);
	explicit TLocus(const arma::Row<double> &SimData);

	~TLocus() = default;

	static double arcTrans(double Freq);

	static double invArcTrans(double TransformedData_d);

	void removeNoise();

	[[nodiscard]] const double &data(NumPopType Index_P) const { return _data[Index_P]; }
	[[nodiscard]] const arma::Col<double> &data() const { return _data; }
};

class TGenome {
private:
	std::vector<TLocus> _loci{};
	genometools::TDistancesBinned<NumDistanceGroupType> _distances; // chrname, position, distanceGroup
	TSigmas _sigmas;                                                // default constructor

	NumLociType _numLoci{};
	NumPopType _numPop{};

	bool _addNoise{};

	void _assignValuestoMembers(coretools::TMultiDimensionalStorage<coretools::UnsignedInt16, 3> &Storage);
	NumLociType _storeDatainLoci(coretools::TMultiDimensionalStorage<coretools::UnsignedInt16, 3> &Storage);
	void _readCounts(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise);

	void _readRaw(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise);
	void _assignValuestoMembersRaw(coretools::TMultiDimensionalStorage<double, 2> &Storage);
	NumLociType _storeRaw(coretools::TMultiDimensionalStorage<double, 2> &Storage);

public:
	explicit TGenome(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise);
	TGenome() = default;

	~TGenome() = default;

	void readSitesPopulations(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise);

	void removeNoise();

	[[nodiscard]] size_t getLociPos(const NumLociType index_L) const { return _distances.getPosition(index_L); }

	[[nodiscard]] std::string getChrName(const NumLociType index_L) const { return _distances.getChunkName(index_L); }

	[[nodiscard]] genometools::TDistancesBinned<NumDistanceGroupType> *distances() { return &_distances; }

	[[nodiscard]] NumSigmaType LociToSigmaIndex(NumLociType Index_L) const { return _sigmas.LociToSigmaIndex(Index_L); }

	[[nodiscard]] size_t numSigmas() const { return _sigmas.numSigmas(); }

	[[nodiscard]] const TSigmas &getSigmas() const { return _sigmas; }

	[[nodiscard]] const arma::vec &getFlattendWeightedAvgSigma() const { return _sigmas.getFlattendWeightedAvgSigma(); }
	[[nodiscard]] const arma::mat &getWeightedAvgSigma() const { return _sigmas.getWeightedAvgSigma(); }

	[[nodiscard]] const TLocus &locus(NumLociType Index_L) const { return _loci[Index_L]; }

	[[nodiscard]] const arma::Col<double> &data(NumLociType Index_L) const { return _loci[Index_L].data(); }

	const TLocus &operator[](NumLociType Index_L) const { return _loci[Index_L]; }

	[[nodiscard]] NumLociType numLoci() const { return _numLoci; }
	[[nodiscard]] NumPopType numPop() const { return _numPop; }
};

#endif // TREESWIRL2_TGENOME_H
