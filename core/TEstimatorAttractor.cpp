//
// Created by madleina on 13.02.24.
//

#include "TEstimatorAttractor.h"

#include "THelper.h"
#include "coretools/devtools.h"
#include "stattools/HMM/TLatentVariableFixed.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TOptimizeTransMatNelderMeadAttractorShift.h"

using namespace coretools::instances;

//-----------------------------------
// TEstimatorSingleAttractor
//-----------------------------------

TEstimatorSingleAttractor::TEstimatorSingleAttractor(const std::string &OutName, TLatVarBase &LatentVariable,
                                                     genometools::TDistancesBinned<NumDistanceGroupType> *Distances,
                                                     size_t NumInferredMigEdges, size_t NumStatesPerMig,
                                                     const std::vector<size_t> &Attractors,
                                                     const std::vector<size_t> &Combinations,
                                                     const std::vector<double> &InitialValuesKappasPhisZetas)
    : _distances(Distances), _latentVariable(LatentVariable), _numInferredMigEdges(NumInferredMigEdges),
      _numStatesPerMig(NumStatesPerMig), _outname(OutName), _attractors(Attractors), _combinations(Combinations),
      _initialValuesKappasPhisZetas(InitialValuesKappasPhisZetas) {

	// define settings
	_maxNumIter = parameters().get("maxNumIterAttractor", 1000);
	_minDeltaLL = parameters().get("deltaLLAttractor", 1e-05);
}

std::vector<std::shared_ptr<TSOptimizer>> TEstimatorSingleAttractor::_createOptimizers() {
	std::vector<std::shared_ptr<TSOptimizer>> optimizers(_numInferredMigEdges);

	// use numStates provided by user not the one from W (can vary for multiple migrations)
	for (size_t i = 0; i < _numInferredMigEdges; i++) {
		optimizers[i] = std::make_shared<TSTransMatAttractor>(_numStatesPerMig);
	}
	return optimizers;
}

void TEstimatorSingleAttractor::run(bool SkipEM) {
	logfile().startIndent("Using attractor(s): ", _attractors, ".");
	logfile().startIndent("Initial kappa, phi, zeta = ", _initialValuesKappasPhisZetas);

	// create optimizers
	auto optimizers = _createOptimizers();
	setAttractor(optimizers, _attractors);

	// create transition matrices
	const bool withAttractor = true;
	TSTransMat transitionMatrices;
	initializeTransitionMatrices(transitionMatrices, _distances, withAttractor, _outname, optimizers,
	                             _initialValuesKappasPhisZetas);

	// run Baum-Welch!
	const bool estimatePosterior       = true;
	std::vector<NumLociType> chunkEnds = _distances->getChunkEnds<NumLociType>();
	_finalLL = runBaumWelch(transitionMatrices, _latentVariable, chunkEnds, _outname, _maxNumIter, _minDeltaLL,
	                        estimatePosterior, SkipEM, doSQUAREM());

	_finalKappasPhisZetas = transitionMatrices.getFinalParameterEstimatesEM();

	logfile().endIndent("Finished attractor(s) ", _attractors, ". Final kappa, phi, zeta : ", _finalKappasPhisZetas,
	                    ".");
}
