//
// Created by creyna on 16.03.21.
//

#ifndef TREESWIRL2_TSIGMA_H
#define TREESWIRL2_TSIGMA_H

#include "TTree.h"
#include <set>

//------------------------------------------------
// N vector
//------------------------------------------------

class TNVector {
private:
	std::vector<double> _N;
	NumLociType _counts;

	arma::mat _diagOneDivN;

	void _setDiagOneDivN();

public:
	explicit TNVector(const std::vector<double> &SampleSizes_N);
	TNVector(std::vector<double> SampleSizes_N, const NumLociType &Counts);

	~TNVector() = default;

	// setters
	[[nodiscard]] NumPopType size() const { return _N.size(); }

	[[nodiscard]] const NumLociType &counts() const { return _counts; }

	[[nodiscard]] const arma::mat &diagonalOneDivN() const { return _diagOneDivN; }

	[[nodiscard]] const std::vector<double> &getN() const { return _N; }

	double operator[](NumLociType Index_m) const { return _N[Index_m]; }

	TNVector operator*(const TNVector &other) const;

	TNVector &operator=(const TNVector &other) = default;

	bool operator<(const TNVector &other) const {
		// expect equal vector sizes
		return _N < other._N;
	}

	void operator++() { ++_counts; }
};

//------------------------------------------------
// N vectorPointer
//------------------------------------------------

class TNVectorPointer {
private:
	TNVector *_ptr;
	NumLociType _index;

public:
	TNVectorPointer(TNVector &NVector, NumLociType Index) {
		_ptr   = &NVector;
		_index = Index;
	}

	bool operator<(const TNVectorPointer &other) const { return *_ptr < *other._ptr; }

	[[nodiscard]] NumLociType index() const { return _index; }
};

//------------------------------------------------
// N vectors
//------------------------------------------------

class TNVectors {
private:
	std::vector<TNVector> _NVectors;
	std::set<TNVectorPointer, std::less<>> _orderedSet{};

public:
	TNVectors()  = default;
	~TNVectors() = default;

	void reserve(NumLociType NumLoci) { _NVectors.reserve(NumLoci); }
	void clear() { _orderedSet.clear(); }

	NumLociType addNVector(const std::vector<double> &SampleSizes_N);

	[[nodiscard]] NumLociType findMinWeightIndex() const;
	[[nodiscard]] double calcEuclideanDistance(const NumLociType &Index_First, const NumLociType &Index_Second) const;

	TNVector merge(const NumLociType &Index_First, const NumLociType &Index_Second);

	[[nodiscard]] size_t size() const { return _NVectors.size(); }
	[[nodiscard]] NumPopType numPop() const { return _NVectors[0].size(); }
	[[nodiscard]] const std::vector<TNVector> &getNVectors() const { return _NVectors; }
	std::vector<TNVector> &getNVectors() { return _NVectors; }

	// keep track of index loci to sigma
	const TNVector &operator[](NumLociType index) const { return _NVectors[index]; }
	TNVector &operator[](NumLociType index) { return _NVectors[index]; }
};

//------------------------------------------------
// TSigmas
//------------------------------------------------

class TSigmas {
private:
	TNVectors _NVectors;
	std::vector<NumLociType> _NVectorsIndex{};

	// weighted avg of all Nvectors left to use for initialization
	arma::mat _weightedAvgSigma;
	arma::vec _flattenWeightedAvgSigma;

	[[nodiscard]] NumLociType _findMinWeightIndex() const { return _NVectors.findMinWeightIndex(); }
	[[nodiscard]] double _calcEuclideanDistance(const NumLociType &Index_First, const NumLociType &Index_Second) const {
		return _NVectors.calcEuclideanDistance(Index_First, Index_Second);
	}

	void _merge(NumLociType Index_First, NumLociType Index_Second);

	void _mergeAll() {
		size_t total = 0;
		std::vector<double> avg(numPop(), 0.0);

		// calculate weighted average of 1/N over all Sigmas
		for (const auto &Ns : _NVectors.getNVectors()) {
			total += Ns.counts();
			for (size_t pop = 0; pop < numPop(); pop++) { avg[pop] += Ns.counts() * Ns[pop]; }
		}
		for (size_t pop = 0; pop < numPop(); pop++) { avg[pop] /= (double)total; }

		_weightedAvgSigma        = arma::diagmat(1.0 / arma::vec(avg));
		_flattenWeightedAvgSigma = arma::vectorise(_weightedAvgSigma);
	}

public:
	TSigmas() = default;

	~TSigmas() = default;

	void reserve(NumLociType NumLoci) {
		_NVectors.reserve(NumLoci);
		_NVectorsIndex.reserve(NumLoci);
	}

	void clearPointers() { _NVectors.clear(); }

	// when reading data, call this on every site
	void addNVector(const std::vector<double> &SampleSizes_N) {
		_NVectorsIndex.push_back(
		    _NVectors.addNVector(SampleSizes_N)); // return index of Sigma to use for the locus that was just pushed
	}

	void clusterNVectors(NumSigmaType maxSigmaConfigurations);

	// getters
	[[nodiscard]] NumPopType numPop() const { return _NVectors.numPop(); }
	[[nodiscard]] NumSigmaType numSigmas() const { return _NVectors.getNVectors().size(); }
	[[nodiscard]] const NumSigmaType &LociToSigmaIndex(const NumLociType &Index_L) const {
		return _NVectorsIndex[Index_L];
	}
	[[nodiscard]] const arma::vec &getFlattendWeightedAvgSigma() const { return _flattenWeightedAvgSigma; }
	[[nodiscard]] const arma::mat &getWeightedAvgSigma() const { return _weightedAvgSigma; }
	const TNVector &operator[](NumSigmaType Index_Sigma) const;
};

#endif // TREESWIRL2_TSIGMA_H
