//
// Created by creyna on 18.03.21.
//

#include "TVarCovar_Sinv.h"
using namespace coretools::instances;

TVarCovar_Sinv::TVarCovar_Sinv(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2) {
	initialize(Sigma, VarCovar_W, PriorVar_sigma2);
}

void TVarCovar_Sinv::initialize(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2) {
	_S_inverse.resize(VarCovar_W.size(), VarCovar_W.size());
	_log_determinant_S_inverse = 0.0;
	update(Sigma, VarCovar_W, PriorVar_sigma2);
}

void TVarCovar_Sinv::update(const TNVector &Sigma, const TVarCovar_W &VarCovar_W, const double &PriorVar_sigma2) {
	// fill matrix S
	arma::Mat<double> S = VarCovar_W.varCovar() + Sigma.diagonalOneDivN() + PriorVar_sigma2;

	// calculate inverse and log determinant
	arma::inv(_S_inverse, S);
	_log_determinant_S_inverse = log(arma::det(_S_inverse));
}

//------------------------------------------------
// TVarCovars_Sinv
//------------------------------------------------

TVarCovars_Sinv::TVarCovars_Sinv(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws,
                                 const double &PriorVar_sigma2) {
	initialize(Sigmas, VarCovar_Ws, PriorVar_sigma2);
}

void TVarCovars_Sinv::initialize(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws,
                                 const double &PriorVar_sigma2) {
	_numStates_J = VarCovar_Ws.numStates_J();
	_numSigmas   = Sigmas.numSigmas();
	_sigma2      = PriorVar_sigma2;

	_varCovars_S_inverse.reserve(_numStates_J * _numSigmas);
	update(Sigmas, VarCovar_Ws);
}

void TVarCovars_Sinv::update(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws) {
	for (NumSigmaType s = 0; s < _numSigmas; s++) {
		for (NumStatesType j = 0; j < _numStates_J; j++) {
			_varCovars_S_inverse.emplace_back(Sigmas[s], VarCovar_Ws[j], _sigma2);
		}
	}
}

void TVarCovars_Sinv::update(const TSigmas &Sigmas, const TVarCovars_W &VarCovar_Ws, const double &PriorVar_sigma2) {
	_sigma2 = PriorVar_sigma2;
	// calculate S_inverse for each combination
	for (NumSigmaType s = 0; s < _numSigmas; s++) {
		uint16_t tmp = s * _numStates_J;
		for (NumStatesType j = 0; j < _numStates_J; j++) {
			_varCovars_S_inverse[tmp + j] = TVarCovar_Sinv(Sigmas[s], VarCovar_Ws[j], _sigma2);
		}
	}
}

const TVarCovar_Sinv &TVarCovars_Sinv::operator()(const NumStatesType &Index_J, const NumSigmaType &Index_Sigma) const {
	uint16_t index = Index_Sigma * _numStates_J + Index_J;
	/*if (index >= (_varCovars_S_inverse.size())) {
	    throw std::runtime_error("TVarCovar_Sinv& TVarCovars_Sinv::operator()(const NumStatesType & Index_J, const "
	                             "NumSigmaType & Index_Sigma) : index out of bound!");
	}*/
	return _varCovars_S_inverse[index];
}

TVarCovar_Sinv &TVarCovars_Sinv::operator()(const NumStatesType &Index_J, const NumSigmaType &Index_Sigma) {
	uint16_t index = Index_Sigma * _numStates_J + Index_J;
	/*if (index >= (_varCovars_S_inverse.size())) {
	    throw std::runtime_error("TVarCovar_Sinv& TVarCovars_Sinv::operator()(const NumStatesType & Index_J, const "
	                             "NumSigmaType & Index_Sigma) : index out of bound!");
	}*/
	return _varCovars_S_inverse[index];
}
