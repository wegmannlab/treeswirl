
// Created by creyna on 09.03.21.
//

#include "TTree.h"
#include "coretools/Strings/fromString.h"
#include "coretools/Strings/toString.h"
#include "coretools/devtools.h"

using namespace coretools::instances;

TTree::TTree(const std::string &Filename) { initializeFromFile_TreeSwirl(Filename); }

TTree::~TTree() { clear(); }

TNode::TNode(const std::vector<std::string_view> &Line)
    : _child{Line[0]},  // different to NA or not just special characters
      _parent{Line[1]}, // root can be NA or any special character combination
      _value{Line[2]},  // branch length, strength, 0 if unknown or for mig branches
      _type{Line[3]} {} // write Mig for the mig edges

TIMatrix::TIMatrix(const NumBranchType &NumBranches, const size_t &RootIndex,
                   const std::vector<NumPopType> &VecTipIndexes, const std::vector<std::vector<TNode>> &Nodes,
                   bool MultiMigration) {
	_numBranches = NumBranches;
	// resize I matrix: add pop and set branches leading to the pop to 1
	for (size_t tipIndex = 0; tipIndex < VecTipIndexes.size(); tipIndex++) {
		addPopulation();
		// start at a tip
		NumPopType n = VecTipIndexes[tipIndex];
		auto cur     = Nodes[n].front();
		// break until parent of child is the root
		if (MultiMigration) {
			std::vector<NumBranchType> parentIds{};
			while (cur.parentIndex() != RootIndex) {
				// if parent Ids are repeated then there is a loop in the tree
				parentIds.push_back(cur.parentIndex());
				if (parentIds.size() > 1) {
					std::sort(parentIds.begin(), parentIds.end());
					// check for cycles
					const bool hasCycles = std::adjacent_find(parentIds.begin(), parentIds.end()) != parentIds.end();
					if (hasCycles) {
						UERROR("The migration edges should be placed such that there are no cycles in the tree."
						       "These nodes:" +
						       cur.child() + "-" + cur.parent() + "are connecting to a loop (cycle)");
					}
				}
				// if mig edge, jump to the next
				if (cur.type() != "Mig") { set(tipIndex, cur.branchIndex(), true); }
				// climb to next parent node
				const auto &tmp = Nodes[cur.parentIndex()]; // all possible parents
				bool found      = false;
				for (const auto &t : tmp) {
					if (t.parent() != cur.child()) { // don't go back if bi-directional migration
						cur   = t;
						found = true;
						break;
					}
				}
				if (!found) { DEVERROR("Something went wrong in tree"); }
			}
		} else {
			while (cur.parentIndex() != RootIndex) {
				// if mig edge, jump to the next
				if (cur.type() != "Mig") { set(tipIndex, cur.branchIndex(), true); }
				// climb to next parent node
				cur = Nodes[cur.parentIndex()].front();
			}
		}
		// add 1 to node connecting directly to the root
		set(tipIndex, cur.branchIndex(), true);
	}
}

void TTree::_sortCheckTreeFile(coretools::TInputFile &File, std::vector<TNode> &Nodes, std::vector<TNode> &MigEdges) {

	// read nodes (line(s)) in memory
	for (; !File.empty(); File.popFront()) {
		Nodes.emplace_back(File.front());
		// check that the parent and child node are different
		if (Nodes.back().child() == Nodes.back().parent()) {
			UERROR("The parent and child node cannot be the same !"
			       "These node names are the same:" +
			       Nodes.back().child() + "-" + Nodes.back().parent());
		}
		// if found a migration edge, remove it from Nodes and add it to corresponding vector
		if (Nodes.back().type() == "Mig") {
			Nodes.pop_back();
			MigEdges.emplace_back(File.front());
		}
	}

	// check if child names (first column) are unique
	std::sort(Nodes.begin(), Nodes.end());                    //"<" comparing child names
	auto it = std::adjacent_find(Nodes.begin(), Nodes.end()); //"==" child comparison
	if (it != Nodes.end()) {
		UERROR("A duplicate child name node was found '" + it->child() + "'. Is it a migration edge?");
	}

	// check if there is at least one migration edge and no duplicated ones
	if (MigEdges.empty()) {
		logfile().warning("No migration edge was found! Will infer branch lengths only.");
		_fixMigration = true;
		_fixedMigrationRates.resize(1, 0.0);
	} else if (MigEdges.size() > 1) {
		std::sort(MigEdges.begin(), MigEdges.end());
		for (size_t i = 0; i < MigEdges.size(); i++) {
			for (size_t j = i + 1; j < MigEdges.size(); j++) {
				// allow to point to the same child more than once
				if (MigEdges[i].child() == MigEdges[j].child() && MigEdges[i].parent() == MigEdges[j].parent()) {
					UERROR("A duplicate migration edge was found with child '" + MigEdges[i].child() + "' parent '" +
					       MigEdges[i].parent() + "' ");
				}
			}

			// check if migration edges and nodes with no migration are duplicates or reverse of each other
			for (auto &node : Nodes) {
				if (node.parent() == MigEdges[i].parent() && node.child() == MigEdges[i].child()) {
					UERROR("Migration edge cannot be the same as a no migration edge. No migration edge: " +
					       node.parent() + "-" + node.child() + ". MigEdge: " + MigEdges[i].parent() + "-" +
					       MigEdges[i].child());
				}
				if (node.parent() == MigEdges[i].child() && node.child() == MigEdges[i].parent()) {
					UERROR("Found a cycle ! Migration edge cannot be placed in the exact opposite direction of a "
					       "no migration edge. No migration edge: " +
					       node.parent() + "-" + node.child() + ". MigEdge: " + MigEdges[i].parent() + "-" +
					       MigEdges[i].child());
				}
			}
		}
	} else {
		// check if the migration edge and nodes with no migration are duplicates or reverse of each other
		for (auto &node : Nodes) {
			if (node.parent() == MigEdges[0].parent() && node.child() == MigEdges[0].child()) {
				UERROR("Migration edge cannot be the same as a no migration edge. No migration edge: " + node.parent() +
				       "-" + node.child() + ". MigEdge: " + MigEdges[0].parent() + "-" + MigEdges[0].child());
			}
			if (node.parent() == MigEdges[0].child() && node.child() == MigEdges[0].parent()) {
				UERROR("Found a cycle ! Migration edge cannot be placed in the exact opposite direction of a no "
				       "migration edge. No migration edge: " +
				       node.parent() + "-" + node.child() + ". MigEdge: " + MigEdges[0].parent() + "-" +
				       MigEdges[0].child());
			}
		}
	}
}

void TTree::_setIndexes(std::vector<TNode> &Nodes, size_t &RootIndex, std::string &RootName,
                        std::vector<TNode> &MainNodes, bool MigrationNodes) {

	size_t counter   = 0;
	size_t nodeIndex = 0;

	// check if parents exist in column names excluding the NA or special character values (Root). If so, set indexes,
	// and assign branch lengths
	const std::regex pattern("[^a-zA-Z0-9]+");
	for (auto &node : Nodes) {
		// double check if a migration edge contains the root as child otherwise set branch index to know which edge to
		// replace with the mig edge
		if (MigrationNodes) {
			if (node.child() == RootName) { UERROR("Migration edge is pointing to root" + node.child()); }
			std::vector<std::string> childNames{};
			childNames.reserve(MainNodes.size());
			for (auto &n : MainNodes) { childNames.push_back(n.child()); }
			// find index in MainNodes (the ones wit no migration) that matches the child name of Nodes (mig nodes)
			auto found = std::find(childNames.begin(), childNames.end(), node.child());
			if (found != childNames.end()) {
				size_t index = std::distance(childNames.begin(), found);
				// this will help to know which index in MainNodes must be replaced by the migration node values.. not
				// the actual branch index anymore!
				node.setBranchIndex(index);

				auto val = coretools::str::fromString<double>(node.value());
				if (val < 0.0 || val >= 0.5) { // 0.0 && 1.0 ?
					UERROR("Migration rate must be > 0.0 && < 0.5. The incorrect migration value was " + node.value());
				}
				_migrationWeights.push_back(val);

			} else {
				UERROR("Migration edge is pointing to an invalid node (" + node.child() +
				       ") that is not found in the child name column!!");
			}
		}

		// set root index and name otherwise assign branch lengths and set branch and parent indexes for the other nodes
		// that are not root or migration
		if (!(MigrationNodes) && (node.parent() == "NA" || std::regex_match(node.parent(), pattern))) {
			// Root index and name was found, no branchlength in this case
			RootIndex = nodeIndex;
			RootName  = node.child();
		} else if (!std::any_of(MainNodes.begin(), MainNodes.end(),
		                        [&node, &counter, &MigrationNodes, this](TNode &mainNode) {
			                        if (node.parent() == mainNode.child()) {
				                        if (!MigrationNodes) {
					                        // a parent was found
					                        if (!mainNode.isParent()) { mainNode.isParent(true); }
					                        // convert string to double and check if value is not negative, in case
					                        // other chars than numbers are provided the returned value is 0, i.e. "NA"
					                        // = 0
					                        auto val = coretools::str::fromString<double>(node.value());
					                        if (val < 0.0) {
						                        UERROR("Branch length cannot be negative " + node.value());
					                        }
					                        _branches.addLength(val);
					                        _branches.addName(node.parent() + "_" + node.child());
					                        node.setBranchIndex(_branches.size() - 1);
				                        }
				                        node.setParentIndex(counter);
				                        counter = 0;
				                        return true;
			                        }
			                        counter++;
			                        return false;
		                        })) {
			UERROR("Parent name '" + node.parent() + "' does not exist in child name column!");
		}
		nodeIndex++;
	}
}

void TTree::_assignTipIndexes(std::vector<NumPopType> &VecTipIndexes, const std::vector<TNode> &Nodes) {
	for (size_t i = 0; i < Nodes.size(); i++) {
		if (!Nodes[i].isParent()) {
			_tips.addLabel(Nodes[i].child());
			VecTipIndexes.push_back(i);
		}
	}
}

void TTree::_setBranches() {
	std::vector<coretools::Positive> branchLengths{};
	// std::vector<double> branchLengths{};
	parameters().fill("branchLengths", branchLengths);

	if (branchLengths.size() == 1 && _numBranches_K > 1)
		branchLengths.assign(_numBranches_K, branchLengths[0]);
	else if (branchLengths.size() != _numBranches_K) {
		UERROR("Number of branch lengths provided: " + coretools::str::toString(branchLengths.size()) +
		       " does not match number of branches from treeFile: " + coretools::str::toString(_numBranches_K) +
		       " Either provide lengths for each branch or one length value for all branches like '0.5{numBranches}' ");
	}

	std::vector<double> lengths{};
	lengths.reserve(_numBranches_K);
	for (auto &l : branchLengths) lengths.push_back(l);
	update(lengths);
	logfile().listFlush("Branch lengths = ");
	writeBranchLengths_c();
}

void TTree::_buildBinaryArray(const NumMigEdgeType &NumMigrations, std::string const &Prefix) {
	if (!NumMigrations) {
		_binaryArray.push_back(Prefix);
		return;
	}
	_buildBinaryArray(NumMigrations - 1, Prefix + '0');
	_buildBinaryArray(NumMigrations - 1, Prefix + '1');
}

auto copyNodes(const std::vector<TNode> &Nodes) {
	std::vector<std::vector<TNode>> cpNodes;
	cpNodes.reserve(Nodes.size());
	for (const auto &node : Nodes) { cpNodes.push_back({node}); }
	return cpNodes;
}

void TTree::_fillIs(const size_t &RootIndex, const std::vector<NumPopType> &VecTipIndexes,
                    const std::vector<TNode> &Nodes, const std::vector<TNode> &MigEdges) {
	// I matrix with no migration - all mig edges are 0 (closed) for first index in binary array
	_Is.emplace_back(_numBranches_K, RootIndex, VecTipIndexes, copyNodes(Nodes));
	NumPopType openMigrations{};
	for (size_t v = 1; v < _binaryArray.size(); v++) {
		const std::string &b                    = _binaryArray[v];
		// reset copy for each binary configuration
		std::vector<std::vector<TNode>> cpNodes = copyNodes(Nodes);
		openMigrations                          = 0;
		for (NumMigEdgeType i = 0; i < _numMigrations; i++) {
			// if open migration edge
			if (b[i] == '1') {
				auto &n = cpNodes[MigEdges[i].branchIndex()];
				n.insert(n.begin(), MigEdges[i]);
				openMigrations++;
			}
		}
		if (openMigrations > 1) {
			// I matrices with multiple migration edges
			_Is.emplace_back(_numBranches_K, RootIndex, VecTipIndexes, cpNodes, true);
		} else {
			// I matrices with one migration edge
			_Is.emplace_back(_numBranches_K, RootIndex, VecTipIndexes, cpNodes, false);
		}
	}
}

size_t numChildrenOfNode(const TNode &Node, const std::vector<TNode> &Nodes) {
	size_t numChildrenNode = 0;
	for (const auto &other : Nodes) {
		if (other.parent() == Node.child()) { ++numChildrenNode; }
	}
	return numChildrenNode;
}

bool nodeHasMigration(const TNode &Node, const std::vector<TNode> &MigEdges) {
	for (const auto &other : MigEdges) {
		if (other.parent() == Node.child() || other.child() == Node.child()) { return true; }
	}
	return false;
}

void TTree::_fillBranchesToEqualize(const std::vector<TNode> &Nodes, const std::vector<TNode> &MigEdges,
                                    size_t RootIndex) {
	for (size_t i = 0; i < Nodes.size(); ++i) {
		if (numChildrenOfNode(Nodes[i], Nodes) != 1 && i != RootIndex) {
			// node has zero or multiple children and is not root
			std::vector<size_t> ix = {Nodes[i].branchIndex()};
			auto parent            = Nodes[Nodes[i].parentIndex()];
			while (numChildrenOfNode(parent, Nodes) == 1) {
				if (!nodeHasMigration(parent, MigEdges)) {
					UERROR("Node ", parent.child(),
					       " is superficial: splits a branch, but only has one child and no migration");
				}
				// parent has only 1 child and is migration node (receives or sends migration)
				ix.emplace_back(parent.branchIndex());
				parent = Nodes[parent.parentIndex()];
			}
			if (ix.size() > 1) { _branchesToEqualize.emplace_back(ix); }
		}
	}

	// report
	logfile().startIndent(
	    "Will equalize the branch lengths of the following (confounded) branches after initialization:");
	for (const auto& it : _branchesToEqualize) {
		std::vector<std::string> out;
		for (auto ix : it) { out.push_back(_branches.names(ix)); }
		logfile().list(out);
	}
	logfile().endIndent();
}

void TTree::initializeFromFile_TreeSwirl(const std::string &Filename) {

	coretools::TInputFile File(Filename, coretools::FileType::Header);
	std::vector<TNode> nodes{};
	std::vector<TNode> migEdges{};
	// sort nodes and assign the mig edges. Error if there are duplicates, no migEdges or same migEdges as nodes.
	logfile().startIndent("Checking if nodes from file '" + Filename + "' are valid...");
	_sortCheckTreeFile(File, nodes, migEdges);

	clear();
	size_t rootIndex{};
	std::string rootName{};
	// set root index, root name, parent indexes, branch indexes, branch lengths and if they are parent or not
	_setIndexes(nodes, rootIndex, rootName, nodes, false);

	// all nodes that are no parents are tips
	std::vector<NumPopType> vecTipIndexes{};
	_assignTipIndexes(vecTipIndexes, nodes);

	// assign all Tree attributes
	_numBranches_K = _branches.size();
	if (parameters().exists("branchLengths")) {
		_setBranches();
	} else if (parameters().exists("keepTreeFileBranches")) {
		std::vector<double> tmp(_numBranches_K, 0.);
		if (tmp == _branches.lengths()) {
			// tmp.assign(_numBranches_K, 0.1);
			//_branches.lengths() = tmp;
			UERROR("No branch lengths found in treeFile ! If initial branch lengths are known, use 'branchLengths' or "
			       "assign valid values in the treeFile...");
		} else {
			logfile().listFlush("Branch lengths from TreeFile = ");
			writeBranchLengths_c();
		}
	}

	_numPopulations_M = _tips.size();
	_numMigrations    = migEdges.size();
	_migrationWeights.reserve(_numMigrations);
	// build binary vectors for close and open migration edges combinations
	_buildBinaryArray(_numMigrations);
	_Is.reserve(_binaryArray.size()); // std::pow(2.0, _numMigrations)

	// set parent index for migEdges and the index (~brancIndex) where a no migration edge will be replaced by a mig
	// edge
	_setIndexes(migEdges, rootIndex, rootName, nodes, true);

	// fill Is matrices based on binary array configuration
	_fillIs(rootIndex, vecTipIndexes, nodes, migEdges);

	if (!parameters().exists("branchLengths")) {
		logfile().list("Branch names = ", _branches.names());
		logfile().listFlush("Branch lengths = ");
		writeBranchLengths_c();
	}

	// store names of migration edges
	for (const auto &migEdge : migEdges) { _migEdgeNames.emplace_back(migEdge.parent() + "_" + migEdge.child()); }

	// get branches that contain migration edges
	_fillBranchesToEqualize(nodes, migEdges, rootIndex);

	// read fixed migration rates
	_fixMigration        = parameters().exists("fixMigrationRates");
	_fixedMigrationRates = parameters().get("fixMigrationRates", std::vector<double>(_numMigrations, 0.0));
	if (_fixedMigrationRates.size() != _numMigrations) {
		UERROR("Wrong size of fixed migration rates: expected ", _numMigrations, ", found ",
		       _fixedMigrationRates.size(), ".");
	}

	logfile().list("Number of Populations = ", _numPopulations_M);
	logfile().list("Number of Migrations = ", _numMigrations);

	logfile().endIndent();
}
