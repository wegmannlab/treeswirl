//
// Created by madleina on 13.02.24.
//

#ifndef TREESWIRL_TESTIMATORLADDER_H
#define TREESWIRL_TESTIMATORLADDER_H

#include "TInitializer_Tree.h"
#include "TLatentVariable_TreeSwirl.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

class TEstimatorLadder {
private:
	const TTree &_tree;
	genometools::TDistancesBinned<NumDistanceGroupType> *_distances = nullptr;
	TLatentVariable_TreeSwirl &_latentVariable;

	size_t _numStatesPerMig;
	std::string _outname;
	std::vector<size_t> _combinations;

	std::vector<double> _finalKappas;
	std::vector<double> _summedPosteriors;
	double _finalLL = 0.0;

	std::vector<std::shared_ptr<TSOptimizer>> _createOptimizer();
	void _run(bool SkipEM);
	void _initFromFile(std::string_view Filename);
	void _calculateSummedPosteriors();
	std::vector<std::string> _createHeader(std::string_view Name);
	void _readKappasFromFile(std::string_view Filename);
	void _readPosteriorSurfaceFromFile(std::string_view Filename);
	void _readLatentVariable(std::string_view Filename);

public:
	explicit TEstimatorLadder(const std::string &OutName, const TTree &Tree, TLatentVariable_TreeSwirl &LatentVariable,
	                          genometools::TDistancesBinned<NumDistanceGroupType> *Distances, size_t NumStatesPerMig,
	                          const std::vector<size_t> &Combinations);
	~TEstimatorLadder() = default;

	void run(bool SkipEM, coretools::TOutputFile &SummaryFile);

	const std::vector<double> &getFinalKappas() const { return _finalKappas; };
	const std::vector<double> &getSummedPosteriors() const { return _summedPosteriors; };
};

#endif // TREESWIRL_TESTIMATORLADDER_H
