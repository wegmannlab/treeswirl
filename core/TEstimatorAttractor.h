//
// Created by madleina on 13.02.24.
//

#ifndef TREESWIRL_TESTIMATORATTRACTOR_H
#define TREESWIRL_TESTIMATORATTRACTOR_H

#include "THelper.h"
#include "TInitializer_Tree.h"
#include "TLatentVariable_TreeSwirl.h"
#include "coretools/Strings/concatenateString.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/HMMDistances/TTransitionMatrixHMMDistances.h"

//-----------------------------------
// TEstimatorSingleAttractor
//-----------------------------------

class TEstimatorSingleAttractor {
private:
	genometools::TDistancesBinned<NumDistanceGroupType> *_distances = nullptr;
	TLatVarBase &_latentVariable;

	size_t _numInferredMigEdges;
	size_t _numStatesPerMig;
	std::string _outname;

	// settings
	size_t _maxNumIter;
	double _minDeltaLL;

	// attractor information
	std::vector<size_t> _attractors;
	std::vector<size_t> _combinations;

	// initial values
	std::vector<double> _initialValuesKappasPhisZetas;

	// final values (after Baum-Welch)
	std::vector<double> _finalKappasPhisZetas;
	double _finalLL = 0.0;

	std::vector<std::shared_ptr<TSOptimizer>> _createOptimizers();

public:
	TEstimatorSingleAttractor(const std::string &OutName, TLatVarBase &LatentVariable,
	                          genometools::TDistancesBinned<NumDistanceGroupType> *Distances,
	                          size_t NumInferredMigEdges, size_t NumStatesPerMig, const std::vector<size_t> &Attractors,
	                          const std::vector<size_t> &Combinations,
	                          const std::vector<double> &InitialValuesKappasPhisZetas);

	void run(bool SkipEM);

	// getters
	[[nodiscard]] double LL() const { return _finalLL; }
	[[nodiscard]] const std::vector<double> &finalKappasPhisZetas() const { return _finalKappasPhisZetas; }
};

//-----------------------------------
// TEstimatorLoopAttractor
//-----------------------------------

template<typename TypeLatVar> class TEstimatorLoopAttractor {
private:
	genometools::TDistancesBinned<NumDistanceGroupType> *_distances = nullptr;
	const TLatentVariable_TreeSwirl &_latentVariableFromLadder;

	size_t _numStatesPerMig;
	std::string _outname;

	// settings
	bool _usePreviousKPZ;

	// initial values
	std::vector<double> _initialValuesKappas;

	// LL surface of attractors
	std::vector<double> _LLSurface;
	std::vector<size_t> _combinations;
	std::vector<std::vector<size_t>> _attractors;

	// negative branch lengths
	std::vector<bool> _negativeBranchLengths;

	auto _runOneLoop(bool SkipEM, const std::vector<size_t> &Attractors, const std::vector<double> &Init,
	                 coretools::TOutputFile &SummaryFile) {
		const auto name = _outname + "_attractor_" + coretools::str::concatenateString(Attractors, ",");

		// create latent variable
		TypeLatVar latentVariable;
		if constexpr (std::is_same_v<TypeLatVar, TLatVarFixed>) {
			// create latent variable with fixed emissions
			latentVariable.initialize(_latentVariableFromLadder.varCovarsW().numStates_J(),
			                          _distances->getChunkEnds<NumLociType>().back(), &_latentVariableFromLadder);
		} else {
			// create latent variable treeSwirl
			latentVariable.initialize(_latentVariableFromLadder.tree(), _latentVariableFromLadder.sites(),
			                          _numStatesPerMig, _latentVariableFromLadder.mu(),
			                          _latentVariableFromLadder.sigma2(), name);
		}

		// run one attractor
		TEstimatorSingleAttractor estimator(name, latentVariable, _distances,
		                                    _latentVariableFromLadder.tree().numInferredMigEdges(), _numStatesPerMig,
		                                    Attractors, _combinations, Init);
		estimator.run(SkipEM);

		_writeToSummaryFile(Attractors, estimator.LL(), latentVariable, estimator.finalKappasPhisZetas(), SummaryFile);

		// store negative branch lengths
		if constexpr (std::is_same_v<TypeLatVar, TLatVarFixed>) {
			_negativeBranchLengths.push_back(false);
		} else {
			bool negative = false;
			for (auto &b : latentVariable.tree().branchLengths_c()) {
				if (b < 0.0) { negative = true; }
			}
			_negativeBranchLengths.push_back(negative);
		}

		return std::make_tuple(estimator.LL(), estimator.finalKappasPhisZetas());
	};

	void _writeToSummaryFile(const std::vector<size_t> &Attractors, double LL, const TypeLatVar &LatentVariable,
	                         const std::vector<double> &FinalKappasPhisZetas, coretools::TOutputFile &SummaryFile) {
		// write to summary file
		const std::string name = "attractor_" + coretools::str::concatenateString(Attractors, ",");
		SummaryFile << name << LL;

		if constexpr (std::is_same_v<TypeLatVar, TLatVarFixed>) {
			// fixed -> use parameters from ladder latent variable
			SummaryFile << _latentVariableFromLadder.mu() << _latentVariableFromLadder.sigma2();
			for (auto b : _latentVariableFromLadder.tree().branchLengths_c()) { SummaryFile << b; }
		} else {
			// write actual parameters
			SummaryFile << LatentVariable.mu() << LatentVariable.sigma2();
			for (auto b : LatentVariable.tree().branchLengths_c()) { SummaryFile << b; }
		}
		// write transition matrix parameters
		if (FinalKappasPhisZetas.size() != _latentVariableFromLadder.tree().numInferredMigEdges() * 3) {
			DEVERROR("Size does not match: ", FinalKappasPhisZetas.size(), " vs ",
			         _latentVariableFromLadder.tree().numInferredMigEdges() * 3);
		}
		size_t c = 0;
		for (size_t i = 0; i < _latentVariableFromLadder.tree().numInferredMigEdges(); ++i) {
			SummaryFile << FinalKappasPhisZetas[c] << FinalKappasPhisZetas[c + 1] << FinalKappasPhisZetas[c + 2]
			            << Attractors[i];
			c += 3;
		}
		SummaryFile.endln();
	}

	static void _startReporting() {
		coretools::instances::logfile().startIndent("Running Baum-Welch on transition matrix with attractor.");
	}
	static void _endReporting() {
		coretools::instances::logfile().list("Finished Baum-Welch on transition matrix with attractor!");
		coretools::instances::logfile().endIndent();
	}

public:
	TEstimatorLoopAttractor(std::string OutName, const TLatentVariable_TreeSwirl &LatentVariableFromLadder,
	                        genometools::TDistancesBinned<NumDistanceGroupType> *Distances, size_t NumStatesPerMig,
	                        const std::vector<double> &InitialValuesKappas, const std::vector<size_t> &Combinations,
	                        const std::vector<std::vector<size_t>> &Attractors)
	    : _distances(Distances), _latentVariableFromLadder(LatentVariableFromLadder), _numStatesPerMig(NumStatesPerMig),
	      _outname(std::move(OutName)), _initialValuesKappas(InitialValuesKappas), _combinations(Combinations),
	      _attractors(Attractors) {

		_LLSurface.resize(_attractors.size());

		// settings for re-setting transition matrix parameters after each attractor
		_usePreviousKPZ = coretools::instances::parameters().exists("usePreviousKPZ");
	};

	void run(bool SkipEM, coretools::TOutputFile &SummaryFile) {
		_startReporting();

		// get initial values: kappas of ladder + default phi and zeta
		const bool withAttractor = true;
		auto oldKPZ              = getInitialTransitionMatrixParameters(
            withAttractor, _latentVariableFromLadder.tree().numInferredMigEdges(), _initialValuesKappas);

		// loop over all combinations of attractors
		for (size_t i = 0; i < _attractors.size(); ++i) {
			const auto [LL, final] = _runOneLoop(SkipEM, _attractors[i], oldKPZ, SummaryFile);
			_LLSurface[i]          = LL;
			if (_usePreviousKPZ) { oldKPZ = final; }
		}

		_endReporting();
	}

	// getters
	[[nodiscard]] const std::vector<double> &LLSurface() const { return _LLSurface; }
	[[nodiscard]] const std::vector<size_t> &combinations() const { return _combinations; }
	[[nodiscard]] const std::vector<bool> &negativeBranchLengths() const { return _negativeBranchLengths; }
};

#endif // TREESWIRL_TESTIMATORATTRACTOR_H
