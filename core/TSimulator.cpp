//
// Created by creyna on 25.05.22.
//

#include "TSimulator.h"
#include "THelper.h"
#include "coretools/Distributions/TMultivariateNormalDistr.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TOptimizeTransMatNelderMeadAttractorShift.h"

using namespace coretools::instances;

TSimulator::TSimulator() {
	_distances.initialize(maxDist);
	_runSimulations();
}

void TSimulator::_runSimulations() {
	logfile().startIndent("Reading parameters to conduct simulation... ");
	_outname = parameters().get<std::string>("out", "TreeSwirl");

	std::string treeFile = parameters().get("treeFile");
	_tree.initializeFromFile_TreeSwirl(treeFile);

	std::vector<double> tmp(_tree.numBranches_K(), 0.);
	if (tmp == _tree.branchLengths_c())
		UERROR("All branch lengths provided in treeFile were set to 0! If initial branch lengths are known, use "
		       "'branchLengths' or assign valid values in the treeFile...");

	_numStates_J = parameters().get("numStates", 21);
	logfile().list("Number of discrete mixture proportions = " + coretools::str::toString(_numStates_J));

	if (parameters().exists("noAttractor")) { _transitionMatrixHasAttractor = false; }

	_kappasPhisZetas = getInitialTransitionMatrixParameters(_transitionMatrixHasAttractor, _tree.numInferredMigEdges());

	_setDistances();

	logfile().endIndent();

	logfile().startIndent("Conducting simulations... ");

	logfile().endIndent();
}

void TSimulator::printW() {
	if (parameters().exists("emissionParameters")) { // overwrite tree branches from file?
		coretools::TInputFile file(parameters().get("emissionParameters"), coretools::FileType::Header);
		std::string tmp;
		for (; !file.empty(); file.popFront()) { tmp = file.frontRaw(); } // go to last line
		coretools::str::TSplitter spl(tmp, '\t');
		spl.popFront(); // remove mu
		spl.popFront(); // remove sigma2

		std::vector<double> values;
		for (auto it : spl) { values.emplace_back(coretools::str::fromString<double>(it)); }

		_tree.update(values);
	}

	TVarCovars_W W(_tree, _numStates_J);
	_writeW(W);
}

void TSimulator::simulate() {
	// writing output to files
	std::string filename = _outname + "_simulations_z-muTilde.txt";
	logfile().list("Writing states and prior in file '" + filename + "' ...");
	std::vector<std::string> header = {"Chr", "Position", "z", "muTilde"};
	coretools::TOutputFile outputFile_z(filename, header);

	std::string filename_data = _outname + "_simulations_data.txt";
	logfile().list("Writing data in file '" + filename_data + "' ...");
	std::vector<std::string> headerData = {"Chr", "Position"};
	for (NumPopType m = 0; m < _tree.numPopulations_M(); ++m) { headerData.push_back(_tree.populationNames(m)); }
	coretools::TOutputFile outputFile_data(filename_data, headerData);
	coretools::TOutputFile outputFile_data_raw;
	if (parameters().exists("raw")) { outputFile_data_raw.open(_outname + "_simulations_raw_data.txt", headerData); }

	// get states
	_simulate_z(_numStates_J, _tree.numInferredMigEdges(), _kappasPhisZetas);

	// get simulated Data
	double muTilde_mu     = parameters().get("meanPrior", 0.0);
	double muTilde_sigma2 = parameters().get("varPrior", 0.1);
	logfile().list("Prior mean = ", muTilde_mu);
	logfile().list("Prior variance =  ", muTilde_sigma2);

	std::vector<NumSamplesType> N             = _setMaxN(_tree.numPopulations_M());
	std::vector<coretools::Probability> probN = _setProbN(_tree.numPopulations_M());
	logfile().list("Max sample size (N) = ", N);
	logfile().listFlush("Probability binomial sampling N = ");
	for (auto prob : probN) { logfile().flush(coretools::str::toString(prob) + " "); }
	logfile().newLine();

	TVarCovars_W W(_tree, _numStates_J);
	if (parameters().exists("writeW")) { _writeW(W); }

	size_t pos = 0;
	for (const auto &j : _z) {
		double muTilde = randomGenerator().getNormalRandom(muTilde_mu, std::sqrt(muTilde_sigma2));

		outputFile_z << _distances.getChunkName(pos) << _distances.getPosition(pos) << j << muTilde << coretools::endl;
		outputFile_data << _distances.getChunkName(pos) << _distances.getPosition(pos);

		if (parameters().exists("raw")) {
			outputFile_data_raw << _distances.getChunkName(pos) << _distances.getPosition(pos);
			_simulateRaw(W[j].varCovar(), muTilde, N, outputFile_data, outputFile_data_raw);
		} else {
			_simulateCounts(W[j].varCovar(), muTilde, N, probN, outputFile_data);
		}
		++pos;
	}
}

void TSimulator::_writeW(const TVarCovars_W &W) {
	for (size_t j = 0; j < W.numStates_J(); ++j) {
		// get coordinates of states
		std::vector<size_t> dim(_tree.numInferredMigEdges(), _numStates_J);
		const auto coord = coretools::getSubscripts(j, dim);

		coretools::TOutputFile file(_outname + "_" + coretools::str::concatenateString(coord, ",") + "_W.txt",
		                            _tree.populationNames());

		// write W
		for (size_t i = 0; i < W[j].varCovar().n_rows; ++i) {
			for (size_t k = 0; k < W[j].varCovar().n_cols; ++k) { file << W[j].varCovar()(i, k); }
			file.endln();
		}
	}
}

///////////////////////////////////////////////////////////////////

void TSimulator::_simulate_z(NumStatesType NumStates_J, NumMigEdgeType NumMigrations,
                             const std::vector<double> &TransitionMatrixParameters) {
	// create transition Matrices
	std::vector<std::shared_ptr<TSOptimizer>> optimizers(NumMigrations);
	if (_transitionMatrixHasAttractor) {
		for (NumMigEdgeType i = 0; i < NumMigrations; i++) {
			optimizers[i] = std::make_shared<TSTransMatAttractor>(NumStates_J);
		}

	} else {
		for (NumMigEdgeType i = 0; i < NumMigrations; i++) {
			optimizers[i] = std::make_shared<TSTransMatLadder>(NumStates_J);
		}
	}

	TSTransMat transitionMatrices(&_distances, optimizers);

	if (_transitionMatrixHasAttractor) {
		std::vector<size_t> js = readAttractors(NumStates_J, NumMigrations);
		for (NumMigEdgeType i = 0; i < NumMigrations; i++) _setAttractorIx(optimizers, i, js[i]);
	}

	_z.resize(_distances.size());
	if (_tree.numInferredMigEdges() > 0) {
		transitionMatrices.fillProbabilities(TransitionMatrixParameters);
		transitionMatrices.simulateStates(_z);
	} else {
		std::fill(_z.begin(), _z.end(), 0);
	}

	if (parameters().exists("useZFromFile")) {
		std::string filename = parameters().get("useZFromFile");
		coretools::TInputFile file(filename, coretools::FileType::NoHeader);
		size_t c = 0;
		for (; !file.empty(); file.popFront(), ++c) {
			if (c >= _z.size()) { UERROR("Too many entries in file ", filename, " (", c, ")!"); }
			_z[c] = file.get<size_t>(0);
		}
	} else if (parameters().exists("simulateFixedZ")) { // overwrite simulated z
		std::vector<size_t> states;
		parameters().fill("simulateFixedZ", states);
		if (states.size() == 1) {
			for (auto &z : _z) { z = states[0]; }
		} else if (states.size() == _z.size()) {
			for (size_t i = 0; i < states.size(); ++i) { _z[i] = states[i]; }
		} else {
			UERROR("Size of z from 'simulateFixedZ' (", states.size(), ") does not match expected size (", _z.size(),
			       ").");
		}
	}
}

arma::Col<double> TSimulator::_simulateData(const arma::Mat<double> &W, const double &Mu_tilde) {
	// mu = mu_tilde
	std::vector<double> vec_mu(W.n_rows, Mu_tilde);

	// Sigma = W + some noise (to account for cases where a state is the highest possible one)
	auto W_noise   = W;
	W_noise.diag() = W_noise.diag() + randomGenerator().getRand(0.00001, 0.00002);

	coretools::probdist::TMultivariateNormalDistr<arma::Row<double>> distr(vec_mu, W_noise);
	arma::Row<double> tmp = distr.sample();

	//  convert to frequencies - invArcTrans
	TLocus locus(tmp); // class TLocus constructor for simData needs arma::row
	return locus.data();
}

////////////////////////////////////////////////////////////////////////////////
void TSimulator::_simulateCounts(const arma::Mat<double> &W, double Mu_tilde, std::vector<NumSamplesType> N,
                                 std::vector<coretools::Probability> ProbN, coretools::TOutputFile &Output) {
	arma::Col<double> freqData = _simulateData(W, Mu_tilde);

	for (size_t m = 0; m < N.size(); m++) {
		auto tmp = round(randomGenerator().getBinomialRand(ProbN[m], N[m])); // avoid missing data N == 0
		auto c   = randomGenerator().getBinomialRand(coretools::P(freqData[m]), tmp);
		Output << coretools::str::toString(c) + "/" + coretools::str::toString(static_cast<size_t>(tmp));
	}
	Output << coretools::endl;
}

void TSimulator::_simulateRaw(const arma::Mat<double> &W, double Mu_tilde, std::vector<NumSamplesType> N,
                              coretools::TOutputFile &Output, coretools::TOutputFile &OutputRaw) {
	// mu = mu_tilde
	std::vector<double> vec_mu(W.n_rows, Mu_tilde);

	// Sigma = W + some noise (to account for cases where a state is the highest possible one)
	auto W_noise   = W;
	W_noise.diag() = W_noise.diag() + randomGenerator().getRand(0.00001, 0.00002);

	// add Sigma
	arma::vec tmp(N.size());
	for (size_t i = 0; i < N.size(); ++i) { tmp[i] = 1.0 / (double)N[i]; }
	auto Sigma = arma::diagmat(tmp);

	const auto S = W_noise + Sigma;

	coretools::probdist::TMultivariateNormalDistr<arma::Row<double>> distr(vec_mu, S);
	arma::Row<double> freqs = distr.sample();

	// write
	OutputRaw << freqs << coretools::endl;

	// write counts
	TLocus locus(freqs); // class TLocus constructor for simData needs arma::row
	auto transformed_freqs = locus.data();
	for (size_t m = 0; m < N.size(); m++) {
		auto c = std::round(transformed_freqs[m] * N[m]);
		Output << coretools::str::toString(c) + "/" + coretools::str::toString(N[m]);
	}
	Output << coretools::endl;
}

///////////////////////////////////////////////////////////////////////////////////////

void TSimulator::_setAttractorIx(const std::vector<std::shared_ptr<TSOptimizer>> &Optimizers,
                                 const NumMigEdgeType &Index_M, const NumStatesType &Index_J) {
	if (!_transitionMatrixHasAttractor) { return; }
	std::dynamic_pointer_cast<TSTransMatAttractor>(Optimizers[Index_M])->setAttractorIx(Index_J);
}

void TSimulator::_setDistances() {

	if (parameters().exists("numChr")) {
		auto numChr = parameters().get<coretools::UnsignedInt16>("numChr");
		logfile().list("Number of chromosomes = ", numChr);

		std::vector<coretools::UnsignedInt16> tmp(numChr, 10000);
		std::vector<coretools::UnsignedInt16> numLoci{};
		parameters().fill("numLoci", numLoci, tmp);

		if (numLoci.size() == 1 && numChr > 1)
			numLoci.assign(numChr, numLoci[0]);
		else if (tmp.size() != numLoci.size()) {
			UERROR(
			    "Listed loci size provided (", numLoci.size(), ") does not match number of chr from numChr (",
			    tmp.size(),
			    "). Provide numChr and numLoci with correct size like: numChr=2 numLoci=1000,1000 or numLoci=1000{2}");
		}

		logfile().listFlush("Number of sites per chr = ");
		for (const auto &locus : numLoci) { logfile().flush(coretools::str::toString(locus) + " "); }
		logfile().newLine();

		std::vector<size_t> loci{};
		loci.reserve(numLoci.size());
		for (const auto &l : numLoci) { loci.push_back(l); }

		if (parameters().exists("distance")) {
			size_t fixedDistance = parameters().get<size_t>("distance");
			logfile().list("Fixed distance between sites = ", fixedDistance);
			_distances.simulate(loci, 2, true, fixedDistance);
		} else {
			_distances.simulate(loci, 2, false, {});
		}

	} else {
		std::vector<size_t> numLoci{};
		parameters().fill("numLoci", numLoci, {10000});
		size_t fixedDistance = parameters().get("distance", 1000);
		logfile().list("Number of chromosomes = 1");
		logfile().list("Number of sites per chr = ", numLoci);
		logfile().list("Fixed distance between sites = ", fixedDistance);
		_distances.simulate(numLoci, 2, true, fixedDistance);
	}
}

std::vector<NumSamplesType> TSimulator::_setMaxN(const NumPopType &NumPopulations) {
	std::vector<NumSamplesType> tmp(NumPopulations, 100);
	std::vector<NumSamplesType> Ns{};
	parameters().fill("N", Ns, tmp);

	if (Ns.size() == 1 && NumPopulations > 1 && Ns[0] > 0)
		Ns.assign(NumPopulations, Ns[0]);
	else if (Ns.size() != NumPopulations)
		throw "Listed sample sizes N do not match number of populations from treeFile '" +
		    coretools::str::toString(NumPopulations) +
		    "'. Provide all values based on number of populations: N=100,100,90 or N=100{2},90";
	else {
		for (const auto &N : Ns) {
			if (N == 0) throw "Sample size 'N' cannot be 0";
		}
	}

	return Ns;
}

std::vector<coretools::Probability> TSimulator::_setProbN(const NumPopType &NumPopulations) {
	std::vector<coretools::Probability> tmp(NumPopulations, coretools::P(1.0));
	std::vector<coretools::Probability> probNs{};
	parameters().fill("probN", probNs, tmp);

	if (probNs.size() == 1 && NumPopulations > 1)
		probNs.assign(NumPopulations, probNs[0]);
	else if (probNs.size() != NumPopulations)
		throw "Listed probability sample sizes N do not match number of populations from treeFile '" +
		    coretools::str::toString(NumPopulations) +
		    "'. Provide all values based on number of populations: probN=1,1,0.5 or N=1{2},0.5";
	return probNs;
}
