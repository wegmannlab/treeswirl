//
// Created by creyna on 07.09.22.
//

#include "TInitializer_Tree.h"
#include "TGaussianMixture_EM.h"
#include "THelper.h"
#include "coretools/Types/TStringHash.h"
#include "stattools/ParametersObservations/TObservation.h"

using namespace coretools::instances;

TInitializer_Tree::TInitializer_Tree(TTree *Tree, const TGenome *Sites, const std::string &Outname) {
	_outname = Outname + "_gaussianMixture";
	_tree    = Tree;
	_sites   = Sites;

	_mu_0         = 0.0;
	_sigma2_0     = 0.0;
	_fix_mu_0     = false;
	_fix_sigma2_0 = false;
}

void TInitializer_Tree::run(bool SkipEM) {
	_mu_0     = parameters().get("meanPrior", 0.);
	_sigma2_0 = parameters().get("varPrior", 0.1);

	_fix_mu_0     = parameters().exists("meanPrior");
	_fix_sigma2_0 = parameters().exists("varPrior");

	if (SkipEM || parameters().exists("skipInitializationTree") || parameters().exists("pathLadder")) {
		logfile().list("Skipping EM on branch lengths, prior mean and sigma. Will use default or user-defined values.");
	} else if (parameters().exists("initTreeFromFile")) {
		const auto filename = parameters().get("initTreeFromFile");
		_initFromFile(filename);
	} else {
		_run();
	}

	// report to logfile
	logfile().list("Initial prior mean = ", _mu_0);
	logfile().list("Initial prior variance = ", _sigma2_0);
	logfile().listFlush("Initial branch lengths = ");
	_tree->writeBranchLengths_c();

	// write file with starting values
	_writeInitialEmissionsParameters();
}

void TInitializer_Tree::_initFromFile(std::string_view Filename) {
	// file format: mu0, sigma2_0, then all branches (just as written below)
	logfile().list("Skipping EM on branch lengths, prior mean and sigma. Reading values from file '", Filename, ".");

	coretools::TInputFile file(Filename, coretools::FileType::Header);
	for (; !file.empty(); file.popFront()) {
		_mu_0     = file.get<double>(0);
		_sigma2_0 = file.get<double>(1);
		std::vector<double> branchLengths(_tree->numBranches_K());
		for (NumBranchType b = 0; b < _tree->numBranches_K(); b++) { branchLengths[b] = file.get<double>(b + 2); }
		_tree->update(branchLengths);
	}
}

void TInitializer_Tree::_setMuSigma(double Mu0, double Sigma0) {
	if (!_fix_mu_0) { _mu_0 = Mu0; }
	if (!_fix_sigma2_0) { _sigma2_0 = Sigma0; }
}

bool TInitializer_Tree::_checkOneGaussianMixture(const std::vector<double> &Pis, double ThresholdMinPi) const {
	// check if any pi violates threshold
	for (auto p : Pis) {
		if (p < ThresholdMinPi) { return false; }
	}
	return true;
}

void TInitializer_Tree::_run() {
	// Run EM for Wr* (aka vectorized Wr* = _y), muTilde_0, sigma2Tilde_0 and  pis
	logfile().list("Initializing branch lengths, prior mean and sigma2 with an EM algorithm...");

	// initial number of classes
	size_t numClasses  = 1;
	bool fixNumClasses = false;
	if (parameters().exists("fixedNumClasses")) {
		numClasses    = std::max(1UL, parameters().get<size_t>("fixedNumClasses"));
		fixNumClasses = true;
		logfile().list("Will use with ", numClasses, " classes (argument 'fixedNumClasses').");
	}

	// threshold for minimal pi
	const double thresholdMinPi = parameters().get("thresholdMinPi", 0.2);
	logfile().list("Will reject Gaussian Mixture EM if any pi is smaller than ", thresholdMinPi,
	               " (argument 'thresholdMinPi')");

	size_t numRep = parameters().get("numRepGaussianMixture", 3);
	logfile().list("Will run ", numRep, " Gaussian Mixture EM replicates (argument 'numRepGaussianMixture')");

	GaussianMixtureResults res;
	if (fixNumClasses) {
		res = _runOneClass(numClasses, 0.0, numRep);
	} else {
		// start with one class -> always passes
		res = _runOneClass(1, thresholdMinPi, numRep);

		// try with more classes
		while (true) {
			numClasses++;
			auto curRes = _runOneClass(numClasses, thresholdMinPi, numRep);
			if (!curRes.passed) { break; }       // did not meet threshold of pi
			if (curRes.rss > res.rss) { break; } // rss is worse than before
			res = curRes;                        // we're better, continue!
		}
	}

	// set final results
	_setMuSigma(res.mu_0, res.sigma2_0);
	_tree->updateWithEqualizingBranches(res.branchLengths);

	// report
	logfile().list("Final number of classes: ", res.numClasses);
	logfile().list("Estimated pis (categorical distribution) = ", res.pis);
}

std::pair<double, std::vector<double>> TInitializer_Tree::_runRSS(const std::vector<arma::Col<double>> &Y,
                                                                  const std::vector<double> &Pis) {
	// Run Nelder Mead
	size_t numRepRSS = parameters().get("numRepRSS", 1000);
	logfile().startIndent("Will run Nelder-Mead on RSS ", numRepRSS, " times and keep best solution");

	double bestRSS = std::numeric_limits<double>::max();
	size_t bestIx  = 0;
	std::vector<double> bestBranchLengths;
	std::vector<double> bestMigrationRates;
	for (size_t i = 0; i < numRepRSS; ++i) {
		TNelderMeadRSS rss(_tree, _sites, Y, Pis, _outname + "_RSS_" + coretools::str::toString(i));
		const double cur_rss = rss.runNelderMead();
		if (cur_rss < bestRSS) {
			bestRSS            = cur_rss;
			bestIx             = i;
			bestBranchLengths  = _tree->branchLengths_c();
			bestMigrationRates = rss.migrationRates();
		}
	}

	_tree->updateWithEqualizingBranches(bestBranchLengths);

	logfile().list("Best RSS run: ", bestIx, " with RSS = ", bestRSS);
	logfile().list("Nelder-Mead migration rates = ", bestMigrationRates);
	logfile().list("Nelder-Mead branch lengths = ", bestBranchLengths);
	logfile().endIndent();
	return {bestRSS, bestBranchLengths};
}

GaussianMixtureResults TInitializer_Tree::_runOneClass(size_t NumClasses, double ThresholdMinPi, size_t NumRep) {
	logfile().startIndent("Running Gaussian Mixture EM with ", NumClasses, " classes for ", NumRep, " replicates...");

	double LL = std::numeric_limits<double>::lowest();
	std::vector<arma::Col<double>> y;
	std::vector<double> pis;
	bool passed = false;

	for (size_t i = 0; i < NumRep; ++i) {
		TGaussianMixture_EM em(NumClasses, _tree, _sites, _outname + coretools::str::toString(i), _mu_0, _sigma2_0,
		                       _fix_mu_0, _fix_sigma2_0);
		double curLL     = em.run();
		double curPassed = _checkOneGaussianMixture(em.pis(), ThresholdMinPi);

		if (curLL > LL && curPassed) {
			LL     = curLL;
			passed = true;
			y      = em.y();
			pis    = em.pis();
			_setMuSigma(em.mu_0(), em.sigma_0());
		}
	}

	if (passed) {
		logfile().list("Finished Gaussian Mixture EM with ", NumClasses, " classes, best LL = ", LL);
		auto [bestRSS, bestBranchLengths] = _runRSS(y, pis);
		logfile().endIndent();
		return GaussianMixtureResults(true, bestRSS, _mu_0, _sigma2_0, bestBranchLengths, NumClasses, pis);
	}
	logfile().endIndent("Finished Gaussian Mixture EM with ", NumClasses, ", did not reach threshold on minimum pi.");
	return GaussianMixtureResults(false);
}

void TInitializer_Tree::_writeInitialEmissionsParameters() {
	if (writeTempFiles()) {
		// create header
		std::vector<std::string> header = {"mu", "sigma2"};
		for (NumBranchType b = 0; b < _tree->numBranches_K(); b++) {
			header.push_back("Branch:" + _tree->branchNames(b));
		}

		// write
		coretools::TOutputFile file(_outname + "_emissionParameters.txt", header);
		file << _mu_0 << _sigma2_0;
		for (NumBranchType b = 0; b < _tree->numBranches_K(); b++) { file << _tree->branchLengths_c(b); }
		file.endln();
	}
}
