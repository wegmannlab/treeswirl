//
// Created by creyna on 10.03.21.
//

#ifndef TREESWIRL2_TVARCOVAR_W_H
#define TREESWIRL2_TVARCOVAR_W_H

#include "TTree.h"
#include <armadillo>

//------------------------------------------------
// TVarCovar_W
//------------------------------------------------

class TVarCovar_W {
private:
	bool _initializationBranches{};

	std::vector<double> _migration_w{};
	std::vector<double> _vector_wb{};

	std::vector<arma::Mat<double>> _matrix_J{};
	arma::Mat<double> _vector_J;
	arma::Mat<double> _varCovar_W;

	void _update_wb(const TTree &Tree);

	void _update_J(const TTree &Tree);

public:
	TVarCovar_W(const TTree &Tree, const std::vector<double> &Migration_w, bool InitializationBranches = false);
	TVarCovar_W()  = default;
	~TVarCovar_W() = default;

	void initialize(const TTree &Tree, const std::vector<double> &Migration_w, bool InitializationBranches = false);

	void update(const TTree &Tree);
	bool updateAndCheck(const TTree &Tree);

	[[nodiscard]] NumPopType size() const { return _varCovar_W.n_rows; }

	[[nodiscard]] const std::vector<double> &migrationWeights() const { return _migration_w; }

	[[nodiscard]] const arma::Mat<double> &J(const NumBranchType &Index_k) const { return _matrix_J[Index_k]; }

	[[nodiscard]] const arma::Mat<double> &vectorized_J() const { return _vector_J; }

	[[nodiscard]] const arma::Mat<double> &varCovar() const { return _varCovar_W; }

	[[nodiscard]] const double &varCovar(const NumPopType &Index_m, const NumPopType &Index_n) const {
		return _varCovar_W(Index_m, Index_n);
	}

	const double &operator()(const NumPopType &Index_m, const NumPopType &Index_n) const {
		return _varCovar_W(Index_m, Index_n);
	}
};

//------------------------------------------------
// TVarCovars_W
//------------------------------------------------
class TVarCovars_W {
private:
	NumStatesType _numStates_J{};
	NumStatesType _numStates_J_perEdge{};
	std::vector<TVarCovar_W> _varCovar_Ws{};

	std::vector<size_t> _ixToCheck;

public:
	TVarCovars_W(const TTree &Tree, const NumStatesType &NumStates_J);
	TVarCovars_W()  = default;
	~TVarCovars_W() = default;

	void initialize(const TTree &Tree, const NumStatesType &NumStates_J);

	bool update(const TTree &Tree);

	void permutate_migrations_w(std::vector<std::vector<double>> &Result, const std::vector<double> &elements,
	                            std::vector<double> &Permutation, NumMigEdgeType k);
	// void permutate_migrations_w(std::vector<std::vector<double>> &Permutations, const std::vector<double> &w,
	//                             const std::vector<double> &Concat, size_t n, NumMigEdgeType k);

	[[nodiscard]] NumStatesType numStates_J() const { return _numStates_J; };

	[[nodiscard]] const TVarCovar_W &getW(NumStatesType Index_J) const;
	const TVarCovar_W &operator[](NumStatesType Index_J) const;
};

#endif // TREESWIRL2_TVARCOVAR_W_H
