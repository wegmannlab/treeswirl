//
// Created by madleina on 05.02.24.
//

#include "TNelderMeadRSS.h"

#include "THelper.h"
#include "coretools/devtools.h"
#include <utility>

using namespace coretools::instances;

TNelderMeadRSS::TNelderMeadRSS(TTree *Tree, const TGenome *Sites, const std::vector<arma::Col<double>> &Y,
                               const std::vector<double> &Pis, std::string Outname)
    : _tree(Tree), _sites(Sites), _y(Y), _pis(Pis), _outname(std::move(Outname)) {

	// overwrite y and pis based on truth?
	if (parameters().exists("initRSSFromTruth")) { _overwriteFromFile(); }
}

void TNelderMeadRSS::_overwriteFromFile() {
	std::string name = parameters().get("initRSSFromTruth");
	coretools::TInputFile file(name, coretools::FileType::NoHeader);

	// which states to keep?
	auto states = parameters().get<std::vector<size_t>>("initRSSKeepStates");

	// fill y from simulated file (already vectorised)
	_y.clear();
	for (; !file.empty(); file.popFront()) {
		if (std::find(states.begin(), states.end(), file.curLine()) == states.end()) { continue; }
		arma::colvec vec(file.numCols());
		for (size_t i = 0; i < file.numCols(); ++i) { vec[i] = file.get<double>(i); }
		_y.emplace_back(vec);
	}

	// fill pis
	_pis = parameters().get<std::vector<double>>("pis");
	if (_pis.size() != _y.size()) { UERROR("Size mismatch pis and initRSSKeepStates!"); }
}

double TNelderMeadRSS::runNelderMead() {
	std::vector<double> migrationWeights = _getMigrationWeights(_tree, _y.size());

	// Use NelderMead to update all ws and branch lengths based on residual sum of squares between _y and _y_hat
	if (!parameters().exists("branchLengths") && !parameters().exists("keepTreeFileBranches")) {
		_initializeBranches(migrationWeights);
	}

	// add migration weights to vector
	std::vector<double> initialValues = _tree->branchLengths_c();
	if (!_tree->fixMigration()) {
		initialValues.insert(initialValues.end(), migrationWeights.begin(), migrationWeights.end());
	}

	// run Nelder Mead
	return _iterateNelderMead(initialValues);
}

void TNelderMeadRSS::_initializeBranches(const std::vector<double> &Mig) {
	//  y = XBeta -> w = Jc where w is vectorized W*, J vectorized Js and c branch lengths
	arma::Col<double> branchLengths_c(_tree->numBranches_K(), arma::fill::zeros);

	// extract migration rates for class with highest pi
	const auto maxPi = std::distance(_pis.begin(), std::max_element(_pis.begin(), _pis.end()));
	std::vector<double> mig;
	if (_tree->fixMigration()) {
		mig = _tree->fixedMigrationRates();
	} else {
		mig.insert(mig.begin(), Mig.begin() + maxPi * _tree->numInferredMigEdges(),
		           Mig.begin() + (maxPi + 1) * _tree->numInferredMigEdges());
	}

	TVarCovar_W varCovarW = TVarCovar_W(*_tree, mig, true);

	arma::solve(branchLengths_c, varCovarW.vectorized_J(), _y[maxPi]);

	// Set negative branches to 0.001
	for (double &branch : branchLengths_c) {
		if (branch < 0.0) branch = 0.001;
	}

	// update branches
	_tree->update(arma::conv_to<std::vector<double>>::from(branchLengths_c));
}

void TNelderMeadRSS::_openOutputFile(size_t NumValues) {
	if (writeTempFiles()) {
		std::string filename = _outname + "_initialBranchesMigrations.txt";
		std::vector<std::string> header{"RSS"};
		for (NumBranchType i = 0; i < _tree->numBranches_K(); i++) header.push_back("Branch:" + _tree->branchNames(i));

		size_t c = 0;
		for (size_t j = _tree->numBranches_K(); j < NumValues; j++) {
			for (NumMigEdgeType m = 0; m < _tree->numInferredMigEdges(); m++) {
				header.push_back("w" + coretools::str::toString(c) + "_" + coretools::str::toString(m));
				if (m > 0) ++j;
			}
			++c;
		}

		_outputNelderMead.open(filename, header);
	}
}

double TNelderMeadRSS::_iterateNelderMead(std::vector<double> &InitialValues) {
	// set up output file and write first line
	_openOutputFile(InitialValues.size());
	if (_outputNelderMead.isOpen()) {
		_outputNelderMead << _RSS(InitialValues);
		for (const auto &m : InitialValues) _outputNelderMead << m;
		_outputNelderMead << coretools::endl;
	}

	// convert branch lengths and weights to -inf:+inf space
	_transform(InitialValues);

	// initialized parameters with transformed branches and migration weights
	auto ptr = &TNelderMeadRSS::_calcRSS;
	stattools::TNelderMead nelderMead(*this, ptr);
	nelderMead.setMaxNumFunctionEvaluations(1e06);
	double displacement = 10.0;

	// updating initial values (branches and migration weights) and get minimum
	nelderMead.minimize(InitialValues, displacement);

	// get back the minimum values
	const auto &foundMinimum = nelderMead.coordinates();

	// convert transformed weights back to logit space [0,1] and branches to positive values
	std::vector<double> NM_InitialValues = _invTransform(foundMinimum);

	// store migration weights
	_weights.clear();
	_weights.insert(_weights.end(), NM_InitialValues.begin() + _tree->numBranches_K(), NM_InitialValues.end());

	// avoid to initialize with branch lengths = 0.0
	std::vector<double> branchLengths(NM_InitialValues.begin(), NM_InitialValues.begin() + _tree->numBranches_K());
	std::replace_if(branchLengths.begin(), branchLengths.end(), [](double val) { return val == 0.0; }, 0.0001);
	_tree->update(branchLengths);

	return _calcRSS(foundMinimum);
}

auto TNelderMeadRSS::_getMig(coretools::TConstView<double>::const_iterator end) const {
	if (_tree->fixMigration()) {
		return _tree->fixedMigrationRates();
	} else {
		std::vector<double> migrationWeights(end, end + _tree->numInferredMigEdges());
		return migrationWeights;
	}
}

double TNelderMeadRSS::_RSS(coretools::TConstView<double> Values) const {
	// get branches
	auto end                        = Values.begin() + _tree->numBranches_K();
	arma::Col<double> branchLengths = std::vector<double>(Values.begin(), end);

	// RSS for multiple Ws and diff mig rates
	double weightedRSS = 0.0;
	for (size_t i = 0; i < _y.size(); i++) {
		std::vector<double> migrationWeights = _getMig(end);
		if (_counts > 1 && !_tree->fixMigration()) {
			for (auto mig : migrationWeights) {
				if (mig > 0.999 || mig < 0.001) return std::numeric_limits<double>::max();
			}
		}

		// provide new Migrations_w to create new vectorised J matrix
		TVarCovar_W varCovarW(*_tree, migrationWeights, true);

		arma::Col<double> y_hat = (varCovarW.vectorized_J() * branchLengths) + _sites->getFlattendWeightedAvgSigma();

		// Residuals Square Sum = sum((expected - observed)²)
		weightedRSS += (arma::sum((_y[i] - y_hat) % (_y[i] - y_hat)) * _pis[i]);
		end += _tree->numInferredMigEdges();
	}
	return weightedRSS;
}

double TNelderMeadRSS::_calcRSS(coretools::TConstView<double> TransformedInitialValues) {

	++_counts;
	// convert transformed weights back to logit space [0,1] and branches to weak positive values
	std::vector<double> NM_InitialValues = _invTransform(TransformedInitialValues);
	double weightedRSS                   = _RSS(NM_InitialValues);

	// write to file
	if (_outputNelderMead.isOpen()) {
		_outputNelderMead << weightedRSS;
		for (const auto &m : NM_InitialValues) { _outputNelderMead << m; }
		_outputNelderMead << coretools::endl;
	}

	return weightedRSS;
}

void TNelderMeadRSS::_transform(std::vector<double> &InitialValues) {
	for (size_t i = 0; i < _tree->numBranches_K(); ++i) {
		InitialValues[i] = std::max(0.001, InitialValues[i]);
		InitialValues[i] = log(InitialValues[i]);
	}

	for (size_t i = _tree->numBranches_K(); i < InitialValues.size(); ++i) {
		InitialValues[i] = std::max(0.001, InitialValues[i]);
		InitialValues[i] = std::min(0.999, InitialValues[i]);
		InitialValues[i] = coretools::logit(InitialValues[i]);
	}
}

std::vector<double> TNelderMeadRSS::_invTransform(coretools::TConstView<double> TransformedInitialValues) {
	std::vector<double> result(TransformedInitialValues.size());

	for (size_t i = 0; i < _tree->numBranches_K(); ++i) { result[i] = exp(TransformedInitialValues[i]); }

	for (size_t i = _tree->numBranches_K(); i < TransformedInitialValues.size(); ++i) {
		result[i] = coretools::logistic(TransformedInitialValues[i]);
	}

	return result;
}

std::vector<double> TNelderMeadRSS::_getMigrationWeights(const TTree *Tree, NumStatesType NumClasses) const {
	std::vector<double> Container{};

	if (Tree->fixMigration()) { return Tree->fixedMigrationRates(); }

	if (parameters().exists("migration")) {
		parameters().fill("migration", Container);

		if (Container.size() == 1 && (Tree->numInferredMigEdges() * NumClasses) > 1) {
			Container.assign(NumClasses * Tree->numInferredMigEdges(), Container[0]);
		} else if (Tree->numInferredMigEdges() * NumClasses != Container.size()) {
			UERROR("Number of migration rates provided: ", Container.size(),
			       " does not match 'numMigrations * numClasses': ", Tree->numInferredMigEdges() * NumClasses,
			       " Either provide all migration rates or a value followed by '{reps}' indicating number of "
			       "repetitions: 0.4{3},0.2,0.1");
		}

		for (auto &w : Container) {
			if (w <= 0.0 || w >= 1.0) {
				UERROR("Migration rate must be must be > 0 && < 1. The incorrect migration value "
				       "was:",
				       w, "!");
			}
		}

	} else if (NumClasses == 1) {
		std::vector<double> tmp(Tree->numInferredMigEdges(), 0.);
		if (Tree->migrationWeights() == tmp) {
			for (double &m : tmp)
				m = coretools::instances::randomGenerator().getRand(0.01, 0.99); // getBetaRandom(1, 1);

			Container = tmp;

		} else {
			Container = Tree->migrationWeights();
		}
	} else {
		std::vector<double> tmp(Tree->numInferredMigEdges() * NumClasses);
		for (double &m : tmp) m = coretools::instances::randomGenerator().getRand(0.01, 0.99); // getBetaRandom(1, 1);

		Container = tmp;
	}

	return Container;
}
