//
// Created by madleina on 05.02.24.
//

#ifndef TREESWIRL_TNELDERMEADRSS_H
#define TREESWIRL_TNELDERMEADRSS_H

#include "TLatentVariable_initialTree.h"
#include "stattools/MLEInference/TNelderMead.h"
#include <armadillo>

class TNelderMeadRSS {
private:
	TTree *_tree;
	const TGenome *_sites;

	size_t _counts = 0;
	std::vector<arma::Col<double>> _y{};
	std::vector<double> _pis{};

	std::vector<double> _weights;

	std::string _outname;
	coretools::TOutputFile _outputNelderMead;

	// methods for updating migrations and branches
	void _initializeBranches(const std::vector<double> &Mig);
	double _iterateNelderMead(std::vector<double> &InitialValues);
	void _transform(std::vector<double> &InitialValues);

	// initialize from file
	void _overwriteFromFile();

	// get RSS
	double _RSS(coretools::TConstView<double> Values) const;
	double _calcRSS(coretools::TConstView<double> TransformedInitialValues);
	auto _getMig(coretools::TConstView<double>::const_iterator end) const;

	// auxiliar functions
	std::vector<double> _invTransform(coretools::TConstView<double> TransformedInitialValues);
	std::vector<double> _getMigrationWeights(const TTree *Tree, NumStatesType NumClasses) const;
	void _openOutputFile(size_t NumValues);

public:
	TNelderMeadRSS(TTree *Tree, const TGenome *Sites, const std::vector<arma::Col<double>> &Y,
	               const std::vector<double> &Pis, std::string Outname);
	~TNelderMeadRSS() = default;

	double runNelderMead();

	const std::vector<double> &migrationRates() const { return _weights; }
};

#endif // TREESWIRL_TNELDERMEADRSS_H
