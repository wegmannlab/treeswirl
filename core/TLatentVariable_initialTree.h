//
// Created by reynac on 9/6/22.
//

#ifndef TREESWIRL2_TLATENTVARIABLE_INITIALTREE_H
#define TREESWIRL2_TLATENTVARIABLE_INITIALTREE_H

#include "TGenome.h"
#include "TVarCovar_W.h"
#include "coretools/Distributions/TMultivariateNormalDistr.h"
#include "stattools/EM/TLatentVariable.h"

class TLatentVariable_initialTree : public stattools::TLatentVariable<double, size_t, size_t> {

private:
	TTree *_tree;
	const TGenome *_sites;

	std::vector<coretools::probdist::TMultivariateNormalDistr<arma::Col<double>, false>> _mvn{};

	// var for estimating
	NumStatesType _numStates_R{};

	arma::Mat<double> _onesWtildeinv{}; // 1*W'r⁻¹
	double _div_muSigma2{};
	arma::Col<double> _rho2{};

	arma::Col<double> _sumWeight{};
	double _sumMu_0{};

	//
	double _mu_0{};
	double _sigma2_0{};
	std::vector<arma::Mat<double>> _W_tilde{};

	// initialize mu_0,sigma2_0 and Wtilde
	void _initializeMu0_Sigma2_Wtilde();
	void _setMVN(size_t r, const std::vector<double> &mu_tilde_0);
	arma::mat _generateRandomWTilde(const arma::mat &W);

public:
	TLatentVariable_initialTree(TTree *Tree, const TGenome *Sites, size_t NumClasses);
	~TLatentVariable_initialTree() override = default;

	// important for em
	void calculateEmissionProbabilities(size_t Index, stattools::TDataVector<double, size_t> &Emission) const override;
	void prepareEMParameterEstimationOneIteration() override; // set to 0s every iteration
	void handleEMParameterEstimationOneIteration(size_t Index,
	                                             const stattools::TDataVector<double, size_t> &Weights) override;
	void finalizeEMParameterEstimationOneIteration() override; // mvn update

	// get estimates parameters
	[[nodiscard]] const NumStatesType &numStates_R() const { return _numStates_R; }
	[[nodiscard]] const double &sigma2_0() const { return _sigma2_0; }
	[[nodiscard]] const double &mu_0() const { return _mu_0; }
	[[nodiscard]] std::vector<arma::Col<double>> vectorised_Wr() const {
		std::vector<arma::Col<double>> tmp(_numStates_R);
		for (NumStatesType r = 0; r < _numStates_R; r++) { tmp[r] = arma::vectorise(_W_tilde[r]); }
		return tmp;
	}

	arma::mat getWTilde(size_t r) { return _W_tilde[r]; }
	void print() const;
};

#endif // TREESWIRL2_TLATENTVARIABLE_INITIALTREE_H
