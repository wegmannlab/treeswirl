//
// Created by madleina on 16.02.24.
//

#include "TGaussianMixture_EM.h"
#include "THelper.h"
#include "coretools/Types/TStringHash.h"
#include "stattools/ParametersObservations/TObservation.h"

using TypeCat = coretools::UnsignedInt8WithMax<0>;
using namespace coretools::instances;

TGaussianMixture_EM::TGaussianMixture_EM(size_t NumClasses, TTree *Tree, const TGenome *Sites,
                                         const std::string &Outname, double InitialMu, double InitialSigma2, bool FixMu,
                                         bool FixSigma2) {
	_numClasses = NumClasses;
	_outname    = Outname;
	_tree       = Tree;
	_sites      = Sites;

	_mu_0         = InitialMu;
	_sigma2_0     = InitialSigma2;
	_fix_mu_0     = FixMu;
	_fix_sigma2_0 = FixSigma2;
}

double TGaussianMixture_EM::run() {
	if (_tree->fixMigration()) {
		_runFixedMigration();
		return 0.0;
	} else {
		return _runEM();
	}
}

double TGaussianMixture_EM::_runEM() {
	TLatentVariable_initialTree latentVariable(_tree, _sites, _numClasses);
	TypeCat::setMax(_numClasses - 1);

	using TypePi                     = coretools::ZeroOpenOneClosed;
	constexpr static size_t NumDimPi = 1;

	using BoxOnPi = stattools::prior::TUniformFixed<stattools::TParameterBase, TypePi, NumDimPi>;
	using SpecPi =
	    stattools::ParamSpec<TypePi, stattools::Hash<coretools::toHash("pi")>, BoxOnPi, stattools::SumOne<0>>;
	using BoxOnObs = stattools::prior::TCategoricalInferred<stattools::TObservationBase, TypeCat, 1, SpecPi>;

	BoxOnPi boxOnPi;
	stattools::TParameter<SpecPi, BoxOnObs> pi("pis", &boxOnPi, {});
	BoxOnObs categorical(&pi, _numClasses);

	categorical.initialize();

	size_t maxNumIterationsEM = parameters().get("maxNumIterGaussianMixture", 5000);
	double minDeltaLLEM       = parameters().get("minDeltaLLGaussianMixture", 1e-03);

	stattools::TEM<double, size_t, size_t> em(categorical, latentVariable, maxNumIterationsEM, minDeltaLLEM, true);
	if (parameters().exists("reportGaussianMixture")) { em.report(); }
	double LL = em.runEM({_sites->numLoci()});

	if (parameters().exists("estimateStatePosteriorGaussianMixture")) {
		em.estimateStatePosteriors({_sites->numLoci()}, _outname + "_PosteriorProbabilities.txt");
	}

	// get parameters from latentVariable
	if (!_fix_mu_0) { _mu_0 = latentVariable.mu_0(); }
	if (!_fix_sigma2_0) { _sigma2_0 = latentVariable.sigma2_0(); }

	// store stuff
	_y = latentVariable.vectorised_Wr(); // size = numStates_R
	_pis.resize(latentVariable.numStates_R());
	for (NumStatesType r = 0; r < latentVariable.numStates_R(); r++) { _pis[r] = pi.value(r); }

	stattools::instances::dagBuilder().clear();

	return LL;
}

void TGaussianMixture_EM::_runFixedMigration() {
	// no migration or fixed migration rate -> initialize latent variable with single class and just use genome-wide W
	_numClasses = 1;
	TLatentVariable_initialTree latentVariable(_tree, _sites, _numClasses);

	_y   = latentVariable.vectorised_Wr();
	_pis = {1.0};

	// set parameters from latentVariable
	if (!_fix_mu_0) { _mu_0 = latentVariable.mu_0(); }
	if (!_fix_sigma2_0) { _sigma2_0 = latentVariable.sigma2_0(); }
}