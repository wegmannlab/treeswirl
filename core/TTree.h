//
// Created by creyna on 09.03.21.
//

#ifndef TREESWIRL2_TTREE_H
#define TREESWIRL2_TTREE_H

#include "TreeSwirlSizeTypes.h"
#include "coretools/Files/TInputFile.h"
#include "coretools/Main/TLog.h"
#include "coretools/Main/TParameters.h"
#include "coretools/Types/commonWeakTypes.h"
#include <regex>

class TNode {
private:
	std::string _child{};
	std::string _parent{};
	std::string _value{};
	std::string _type{};

	NumBranchType _parentIndex{};
	NumBranchType _branchIndex{};
	bool _isParent = false;

public:
	explicit TNode(const std::vector<std::string_view> &Line);

	~TNode() = default;

	// setters
	void isParent(const bool &value) { _isParent = value; }

	void setParentIndex(const NumBranchType &Index_parent) { _parentIndex = Index_parent; }

	void setBranchIndex(const NumBranchType &Index_branch) { _branchIndex = Index_branch; }

	// getters
	[[nodiscard]] const bool &isParent() const { return _isParent; }

	[[nodiscard]] const NumBranchType &parentIndex() const { return _parentIndex; }

	[[nodiscard]] const NumBranchType &branchIndex() const { return _branchIndex; }

	[[nodiscard]] const std::string &child() const { return _child; }

	[[nodiscard]] const std::string &parent() const { return _parent; }

	[[nodiscard]] const std::string &value() const { return _value; }

	[[nodiscard]] const std::string &type() const { return _type; }

	// operators
	bool operator<(const TNode &other) const { return _child < other._child; }

	bool operator==(const TNode &other) const { return _child == other._child; }
};

class TBranches {
private:
	std::vector<double> _branchLengths{};
	std::vector<std::string> _names{};

public:
	explicit TBranches() = default;

	~TBranches() = default;

	// setters
	void addLength(const double &BranchLength) { _branchLengths.push_back(BranchLength); }

	void addName(const std::string &Name) { _names.push_back(Name); }

	// getters
	[[nodiscard]] const std::vector<double> &lengths() const { return _branchLengths; }

	[[nodiscard]] std::vector<double> &lengths() { return _branchLengths; }

	[[nodiscard]] NumBranchType size() const { return _branchLengths.size(); }

	[[nodiscard]] const std::vector<std::string> &names() const { return _names; }

	[[nodiscard]] const std::string &names(const NumBranchType &Index_k) const { return _names[Index_k]; }

	// operators
	const double &operator[](const NumBranchType &Index_k) const { return _branchLengths[Index_k]; }

	double &operator[](const NumBranchType &Index_k) { return _branchLengths[Index_k]; };

	void update(const std::vector<double> &BranchLengths_c) { _branchLengths = BranchLengths_c; };

	// clear
	void clear() {
		_branchLengths.clear();
		_names.clear();
	}
};

class TTips {
private:
	std::vector<std::string> _tips{};

public:
	explicit TTips() = default;

	~TTips() = default;

	// setters
	void addLabel(const std::string &Tip) { _tips.push_back(Tip); }

	// getters
	[[nodiscard]] const std::vector<std::string> &names() const { return _tips; }

	[[nodiscard]] NumPopType size() const { return _tips.size(); }

	// operators
	const std::string &operator[](const NumPopType &Index_t) const { return _tips[Index_t]; }

	std::string &operator[](const NumPopType &Index_t) { return _tips[Index_t]; };

	void update(const std::vector<std::string> &names) { _tips = names; };

	// clear
	void clear() { _tips.clear(); }
};

class TIMatrix {
private:
	NumBranchType _numBranches{};
	std::vector<bool> _I{};

public:
	TIMatrix(const NumBranchType &NumBranches, const size_t &RootIndex, const std::vector<NumPopType> &VecTipIndexes,
	         const std::vector<std::vector<TNode>> &Nodes, bool multiMigration = false);

	explicit TIMatrix(const NumBranchType &NumBranches) { _numBranches = NumBranches; }

	~TIMatrix() = default;

	void addPopulation() { _I.resize(_I.size() + _numBranches, false); }

	void set(const NumPopType &Population, const NumBranchType &Branch, const bool &value) {
		_I[Population * _numBranches + Branch] = value;
	}

	// operators
	bool operator()(const NumPopType &Population, const NumBranchType &Branch) const {
		return _I[Population * _numBranches + Branch];
	}
};

class TTree {
private:
	NumPopType _numPopulations_M{};  // size of tips
	NumBranchType _numBranches_K{};  // size of branches
	NumMigEdgeType _numMigrations{}; // size of migration edges

	TTips _tips;
	TBranches _branches;
	std::vector<std::string> _migEdgeNames;

	std::vector<double> _migrationWeights{};
	std::vector<std::string> _binaryArray{};
	std::vector<TIMatrix> _Is;

	// fix migration rate?
	bool _fixMigration = false;
	std::vector<double> _fixedMigrationRates;

	// branches with migration edges that need to be equalized
	std::vector<std::vector<size_t>> _branchesToEqualize;

	// methods
	// TreeSwirl reading helpers
	void _sortCheckTreeFile(coretools::TInputFile &File, std::vector<TNode> &Nodes, std::vector<TNode> &MigEdges);

	void _setIndexes(std::vector<TNode> &Nodes, size_t &RootIndex, std::string &RootName, std::vector<TNode> &MainNodes,
	                 bool MigrationNodes = false);

	void _assignTipIndexes(std::vector<NumPopType> &VecTipIndexes, const std::vector<TNode> &Nodes);

	void _setBranches();

	void _buildBinaryArray(const NumMigEdgeType &NumMigrations, std::string const &Prefix = "");

	void _fillIs(const size_t &RootIndex, const std::vector<NumPopType> &VecTipIndexes, const std::vector<TNode> &Nodes,
	             const std::vector<TNode> &MigEdges);
	void _fillBranchesToEqualize(const std::vector<TNode> &Nodes, const std::vector<TNode> &MigEdges, size_t RootIndex);

public:
	explicit TTree(const std::string &Filename);
	TTree() = default;

	~TTree();

	// readers
	void initializeFromFile_TreeSwirl(const std::string &Filename);

	// setters
	void update(const std::vector<double> &BranchLengths_c) { _branches.update(BranchLengths_c); }
	void updateWithEqualizingBranches(std::vector<double> BranchLengths) {
		if (!coretools::instances::parameters().exists("dontEqualizeBranches")) {
			// equalize confounded branches!
			for (size_t i = 0; i < _branchesToEqualize.size(); ++i) {
				// sum all "confounded" branch lengths
				double sum = 0.0;
				for (size_t j = 0; j < _branchesToEqualize[i].size(); ++j) {
					sum += BranchLengths[_branchesToEqualize[i][j]];
				}
				// calculate average and sett all confounded branch lengths to that value
				double len = sum / (double)_branchesToEqualize[i].size();
				for (size_t j = 0; j < _branchesToEqualize[i].size(); ++j) {
					BranchLengths[_branchesToEqualize[i][j]] = len;
				}
			}
		}
		_branches.update(BranchLengths);
	}

	// getters
	[[nodiscard]] NumPopType numPopulations_M() const { return _numPopulations_M; }

	[[nodiscard]] NumBranchType numBranches_K() const { return _numBranches_K; }

	[[nodiscard]] NumMigEdgeType numMigrationEdges() const { return _numMigrations; }
	[[nodiscard]] NumMigEdgeType numInferredMigEdges() const {
		if (_fixMigration) { return 0; }
		return _numMigrations;
	}

	[[nodiscard]] const std::string &populationNames(const NumPopType &Index_p) const { return _tips[Index_p]; }

	[[nodiscard]] const std::vector<std::string> &populationNames() const { return _tips.names(); }

	[[nodiscard]] const std::string &branchNames(const NumBranchType &Index_k) const {
		return _branches.names(Index_k);
	}

	[[nodiscard]] const std::vector<std::string> &branchNames() const { return _branches.names(); }
	[[nodiscard]] const std::vector<std::string> &migEdgeNames() const { return _migEdgeNames; }

	[[nodiscard]] const double &branchLengths_c(const NumBranchType &Index_k) const { return _branches[Index_k]; }

	[[nodiscard]] const std::vector<double> &branchLengths_c() const { return _branches.lengths(); }

	[[nodiscard]] std::vector<double> &branchLengths_c() { return _branches.lengths(); }

	[[nodiscard]] const std::vector<double> &migrationWeights() const { return _migrationWeights; }

	[[nodiscard]] const double &migrationWeights(const size_t &index_row) const { return _migrationWeights[index_row]; }

	[[nodiscard]] const std::vector<std::string> &binaryArray() const { return _binaryArray; }

	[[nodiscard]] const std::string &binaryArray(const size_t &index_row) const { return _binaryArray[index_row]; }

	[[nodiscard]] const char &binaryArray(const size_t &index_row, const NumMigEdgeType &index_col) const {
		return _binaryArray[index_row][index_col];
	}

	[[nodiscard]] NumMigEdgeType numIMatrices() const { return _Is.size(); }

	[[nodiscard]] const TIMatrix &getI(const NumMigEdgeType &Index) const { return _Is[Index]; }

	// operators
	const double &operator[](const NumBranchType &Index_k) const {
		/*if (Index_k >= _branches.size()) {
		    throw "Cannot get branch length: wrong number of index provided! Expect less than " +
		        coretools::str::toString(_branches.size()) + " but got " + coretools::str::toString(Index_k) + ".";
		}*/
		return _branches[Index_k];
	}

	double &operator[](const NumBranchType &Index_k) {
		/*if (Index_k >= _branches.size()) {
		    throw "Cannot get branch length: wrong number of index provided! Expect less than " +
		        coretools::str::toString(_branches.size()) + " but got " + coretools::str::toString(Index_k) + ".";
		}*/
		return _branches[Index_k];
	}

	// writers
	void writeBranchLengths_c() {
		coretools::instances::logfile().flush(coretools::str::toString(_branches.lengths()));
		coretools::instances::logfile().newLine();
	}

	// clear
	void clear() {
		_numPopulations_M = 0;
		_numBranches_K    = 0;
		_numMigrations    = 0;
		_tips.clear();
		_branches.clear();
		_Is.clear();
	}

	// fixed migration rates?
	bool fixMigration() const { return _fixMigration; }
	const std::vector<double> fixedMigrationRates() const { return _fixedMigrationRates; }
};

#endif // TREESWIRL2_TTREE_H
