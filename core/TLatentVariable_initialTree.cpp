//
// Created by reynac on 9/6/22.
//

#include "TLatentVariable_initialTree.h"
#include "THelper.h"

using namespace coretools::instances;

TLatentVariable_initialTree::TLatentVariable_initialTree(TTree *Tree, const TGenome *Sites, size_t NumClasses) {
	_tree  = Tree;
	_sites = Sites;

	_numStates_R = NumClasses;

	_onesWtildeinv.resize(_numStates_R, _tree->numPopulations_M()); // 1*W'r⁻¹
	_rho2.resize(_numStates_R);
	_sumWeight.resize(_numStates_R);
	_mvn.resize(_numStates_R);

	_initializeMu0_Sigma2_Wtilde();

	std::vector<double> mu_tilde_0(_tree->numPopulations_M(), _mu_0);
	for (NumStatesType r = 0; r < _numStates_R; r++) { _setMVN(r, mu_tilde_0); }
}

void TLatentVariable_initialTree::_setMVN(size_t r, const std::vector<double> &mu_tilde_0) {
	_mvn[r].set<true>(mu_tilde_0, _W_tilde[r] + _sigma2_0);
}

void TLatentVariable_initialTree::_initializeMu0_Sigma2_Wtilde() {
	arma::Col<double> meanPopulations = arma::zeros(_tree->numPopulations_M());
	arma::Mat<double> W               = arma::zeros(_tree->numPopulations_M(), _tree->numPopulations_M());

	for (NumLociType l = 0; l < _sites->numLoci(); l++) { meanPopulations += _sites->data(l); }
	meanPopulations /= _sites->numLoci();

	_mu_0 = arma::mean(meanPopulations);

	for (NumLociType l = 0; l < _sites->numLoci(); l++) {
		for (NumPopType m = 0; m < _tree->numPopulations_M(); m++) {
			double X_EX = _sites->data(l)[m] - meanPopulations[m];
			W(m, m) += (X_EX * X_EX);
			for (NumPopType n = m + 1; n < _tree->numPopulations_M(); n++) {
				W(m, n) += (X_EX * (_sites->data(l)[n] - meanPopulations[n]));
				W(n, m) = W(m, n);
			}
		}
	}

	W /= (_sites->numLoci() - 1);

	if (W.min() > 0.0) {
		_sigma2_0 = W.min();
		W -= _sigma2_0;
	} else {
		_sigma2_0 = 0.001;
	}

	_W_tilde.resize(_numStates_R);
	_W_tilde[0] = W;

	// all other W: same as _W_tilde[0], but add noise on covariances while making sure that thy are not bigger than
	// variance
	for (NumStatesType r = 1; r < _numStates_R; r++) {
		arma::mat W_prime = _generateRandomWTilde(_W_tilde[0]);
		while (!W_prime.is_sympd()) { W_prime = _generateRandomWTilde(_W_tilde[0]); }
		_W_tilde[r] = W_prime;
	}
}

void TLatentVariable_initialTree::print() const {
	for (NumStatesType r = 0; r < _numStates_R; r++) {
		std::cout << r << std::endl;
		_W_tilde[r].print();
	}
}

arma::mat TLatentVariable_initialTree::_generateRandomWTilde(const arma::mat &W) {
	arma::mat W_prime = W;
	for (NumPopType m = 0; m < _tree->numPopulations_M(); m++) {
		for (NumPopType n = m + 1; n < _tree->numPopulations_M(); n++) {
			W_prime(n, m) *= exp(randomGenerator().getNormalRandom(0, 0.1));
			W_prime(n, m) = coretools::min(W_prime(n, m), W_prime(n, n), W_prime(m, m));
			W_prime(m, n) = W_prime(n, m);
		}
	}
	return W_prime;
}

void TLatentVariable_initialTree::calculateEmissionProbabilities(
    size_t Index, stattools::TDataVector<double, size_t> &Emission) const {

	Emission.resize(_numStates_R);
	for (NumStatesType r = 0; r < _numStates_R; r++) { Emission[r] = _mvn[r].density(_sites->data(Index)); }
}

void TLatentVariable_initialTree::prepareEMParameterEstimationOneIteration() {
	// equation 26, values independent of loci index
	arma::Mat<double> W_tilde_r_inv(_tree->numPopulations_M(), _tree->numPopulations_M()); // W'r⁻¹
	arma::Row<double> ones(_tree->numPopulations_M(), arma::fill::ones);
	for (NumStatesType r = 0; r < _numStates_R; r++) {

		// avoid issues with singular matrices: add small noise on diagonal
		arma::mat tmp(_W_tilde[r]);
		tmp.diag() += 1e-10;
		arma::inv_sympd(W_tilde_r_inv, tmp);

		_onesWtildeinv.row(r) = ones * W_tilde_r_inv; //  1*W'r⁻¹ -- checked

		_rho2[r] = 1.0 / (arma::cdot(_onesWtildeinv.row(r), ones) + (1.0 / _sigma2_0)); // p²r
	}

	_div_muSigma2 = _mu_0 / _sigma2_0;

	// reset values
	_sumWeight.zeros();
	for (auto &W : _W_tilde) { W.zeros(); }
	_sigma2_0 = 0;
	_sumMu_0  = 0;
}

void TLatentVariable_initialTree::handleEMParameterEstimationOneIteration(
    size_t l, const stattools::TDataVector<double, size_t> &Weights) {

	arma::Col<double> dnu(_tree->numPopulations_M());
	for (NumStatesType r = 0; r < _numStates_R; r++) {
		double nu_lr = (arma::cdot(_onesWtildeinv.row(r), _sites->data(l)) + _div_muSigma2) * _rho2[r]; // eq26

		_sumWeight[r] += Weights[r]; // eq 27

		dnu = _sites->data(l) - nu_lr;

		_W_tilde[r] += Weights[r] * (dnu * dnu.t()); // sum eq 29

		_sumMu_0 += (Weights[r] * nu_lr); // eq 30

		// eq 31  _mu_0' instead of _mu_0* for practical purposes
		_sigma2_0 += Weights[r] * (_rho2[r] + ((nu_lr - _mu_0) * (nu_lr - _mu_0)));
	}
}

void TLatentVariable_initialTree::finalizeEMParameterEstimationOneIteration() {
	_mu_0 = _sumMu_0 / _sites->numLoci(); // sum eq 30
	_sigma2_0 /= _sites->numLoci();       // sum eq 31

	std::vector<double> mu_0_tilde(_tree->numPopulations_M(), _mu_0);
	for (NumStatesType r = 0; r < _numStates_R; r++) {
		_W_tilde[r] = _rho2[r] + (_W_tilde[r] / _sumWeight[r]); // sum eq 29
		_setMVN(r, mu_0_tilde);
	}
}
