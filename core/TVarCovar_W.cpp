//
// Created by creyna on 10.03.21.
//

#include "TVarCovar_W.h"
using namespace coretools::instances;

//------------------------------------------------
// TVarCovar_W
//------------------------------------------------

TVarCovar_W::TVarCovar_W(const TTree &Tree, const std::vector<double> &Migration_w, bool InitializationBranches) {
	initialize(Tree, Migration_w, InitializationBranches);
}

void TVarCovar_W::initialize(const TTree &Tree, const std::vector<double> &Migration_w, bool InitializationBranches) {
	_initializationBranches = InitializationBranches;
	// initialize all vector and matrices
	_migration_w            = Migration_w;
	_vector_wb.reserve(Tree.binaryArray().size());

	_matrix_J.resize(Tree.numBranches_K(),
	                 arma::Mat<double>(Tree.numPopulations_M(), Tree.numPopulations_M(), arma::fill::zeros));
	_varCovar_W.resize(Tree.numPopulations_M(), Tree.numPopulations_M());

	// check if initialization is true, if so reserve size for vectorized J and update J, otherwise update Tree (both J
	// and W)
	if (_initializationBranches) {
		_vector_J.resize(Tree.numPopulations_M() * Tree.numPopulations_M(), Tree.numBranches_K());
		_update_J(Tree);
	} else {
		_update_J(Tree);
		update(Tree);
	}
}

void TVarCovar_W::_update_wb(const TTree &Tree) {
	for (size_t v = 0; v < Tree.binaryArray().size(); v++) {
		const std::string &b = Tree.binaryArray(v);
		double wb            = 1.0;
		for (NumMigEdgeType i = 0; i < Tree.numMigrationEdges(); i++) {
			if (b[i] == '1') {
				wb *= _migration_w[i];
			} else if (_migration_w[i] == 1.) {
				wb = 0.0;
				break;
			} else {
				wb *= (1 - _migration_w[i]);
			}
		}
		_vector_wb.push_back(wb);
	}
}

void TVarCovar_W::_update_J(const TTree &Tree) {
	_update_wb(Tree);
	for (NumBranchType k = 0; k < Tree.numBranches_K(); k++) {
		arma::Mat<double> &J_k = _matrix_J[k];
		for (NumPopType m = 0; m < Tree.numPopulations_M(); m++) {
			double tmp{};
			for (size_t b = 0; b < Tree.binaryArray().size(); b++) {
				// calculate half of Jks for m population first
				tmp += _vector_wb[b] * Tree.getI(b)(m, k);
			}
			J_k(m, m) = tmp * tmp;
			for (NumPopType n = m + 1; n < Tree.numPopulations_M(); n++) {
				double tmp2{};
				for (size_t b = 0; b < Tree.binaryArray().size(); b++) { tmp2 += (_vector_wb[b] * Tree.getI(b)(n, k)); }
				J_k(m, n) = tmp * tmp2;
				J_k(n, m) = J_k(m, n);
			}
		}
		if (_initializationBranches) _vector_J.col(k) = arma::vectorise(J_k);
	}
}

void TVarCovar_W::update(const TTree &Tree) {
	_varCovar_W.zeros();
	for (NumBranchType k = 0; k < Tree.numBranches_K(); ++k) { _varCovar_W += Tree.branchLengths_c(k) * _matrix_J[k]; }
}

bool TVarCovar_W::updateAndCheck(const TTree &Tree) {
	arma::Mat<double> tmp(Tree.numPopulations_M(), Tree.numPopulations_M(), arma::fill::zeros);
	for (NumBranchType k = 0; k < Tree.numBranches_K(); ++k) { tmp += Tree.branchLengths_c(k) * _matrix_J[k]; }

	// check if diagonal is >= 0
	for (size_t m = 0; m < Tree.numPopulations_M(); ++m) {
		if (tmp(m, m) <= 0.0) { return false; }
	}

	// check if matrix is symmetric and positive semi-definite
	arma::vec eigval;
	const bool valid = arma::eig_sym(eigval, tmp);
	if (!valid) { return false; }

	for (const auto v : eigval) {
		// M is positive semi-definite if and only if all of its eigenvalues are non-negative
		if (v < -1e-08) { return false; }
	}

	// else: ok -> store!
	_varCovar_W = tmp;
	return true;
}

//------------------------------------------------
// TVarCovars_W
//------------------------------------------------

TVarCovars_W::TVarCovars_W(const TTree &Tree, const NumStatesType &NumStates_J) { initialize(Tree, NumStates_J); }

void TVarCovars_W::initialize(const TTree &Tree, const NumStatesType &NumStates_J) {
	_numStates_J_perEdge = NumStates_J;
	_numStates_J         = coretools::uPow(_numStates_J_perEdge, Tree.numInferredMigEdges());
	// if numMigrations  == 1 then keep it as before,  just discretized steps of w,
	// else increase states to numStatesJ^numMigrations by getting all possible permutations from discretized ws of
	// length k
	if (Tree.fixMigration()) {
		_numStates_J = 1;
		_varCovar_Ws.emplace_back(Tree, Tree.fixedMigrationRates());
	} else if (Tree.numInferredMigEdges() == 0) {
		_numStates_J = 1;
		_varCovar_Ws.emplace_back(Tree, std::vector<double>(1, 0.0));
	} else if (Tree.numInferredMigEdges() == 1) {
		_varCovar_Ws.reserve(_numStates_J);
		std::vector<double> w(1);
		for (NumStatesType j = 0; j < _numStates_J; j++) {
			// discretized steps of w
			w[0] = static_cast<double>(j) / (_numStates_J - 1);
			_varCovar_Ws.emplace_back(Tree, w);
		}
	} else {
		std::vector<double> ws(_numStates_J_perEdge);
		for (NumStatesType j = 0; j < _numStates_J_perEdge; j++) {
			ws[j] = static_cast<double>(j) / (_numStates_J_perEdge - 1);
		}
		_varCovar_Ws.reserve(_numStates_J);
		std::vector<std::vector<double>> permutations{};
		std::vector<double> tmp{};
		permutate_migrations_w(permutations, ws, tmp, Tree.numInferredMigEdges());
		for (const auto &w : permutations) { _varCovar_Ws.emplace_back(Tree, w); }
	}

	// fill indices to check
	if (!Tree.fixMigration() && Tree.numInferredMigEdges() > 0) {
		int numIxToCheck = coretools::uPow(2, Tree.numInferredMigEdges());
		std::vector<size_t> dim(Tree.numInferredMigEdges(), _numStates_J_perEdge);
		_ixToCheck.resize(numIxToCheck);
		for (int i = 0; i < numIxToCheck; ++i) {
			std::bitset<8> b(i);
			std::vector<size_t> tmp(Tree.numInferredMigEdges());
			for (size_t j = 0; j < Tree.numInferredMigEdges(); ++j) { tmp[j] = b[j] * (_numStates_J_perEdge - 1); }
			_ixToCheck[i] = coretools::getLinearIndex(tmp, dim);
		}
	}
}

bool TVarCovars_W::update(const TTree &Tree) {
	// first (0) and last (1) are tested for validity
	for (auto ix : _ixToCheck) {
		if (!_varCovar_Ws[ix].updateAndCheck(Tree)) { return false; }
	}

	// all others (intermediate migration rates) are valid if 0 and 1 are valid -> no need to check
	for (size_t i = 1; i < _varCovar_Ws.size() - 1; ++i) { _varCovar_Ws[i].update(Tree); }
	return true;
}

void TVarCovars_W::permutate_migrations_w(std::vector<std::vector<double>> &Result, const std::vector<double> &elements,
                                          std::vector<double> &Permutation, NumMigEdgeType k) {
	if (k == 0) {
		Result.push_back(Permutation);
	} else {
		for (size_t i = 0; i < elements.size(); i++) {
			Permutation.push_back(elements[i]);
			permutate_migrations_w(Result, elements, Permutation, k - 1);
			Permutation.pop_back();
		}
	}
}

const TVarCovar_W &TVarCovars_W::getW(NumStatesType Index_J) const {
	assert(Index_J < _numStates_J);
	return _varCovar_Ws[Index_J];
}

const TVarCovar_W &TVarCovars_W::operator[](NumStatesType Index_J) const {
	assert(Index_J < _numStates_J);
	return _varCovar_Ws[Index_J];
}
