//
// Created by creyna on 16.03.21.
//

#include "TGenome.h"
using namespace coretools::instances;

TLocus::TLocus(const arma::Col<double> &Data, bool Transform) {
	if (Transform) {
		_data.resize(Data.size());
		for (size_t i = 0; i < Data.size(); i++) _data[i] = arcTrans(Data[i]);
	} else {
		_data = Data;
	}
}

TLocus::TLocus(const arma::Col<double> &Data, coretools::StrictlyPositive Concentration) {

	_data.resize(Data.size());
	_freqData.resize(Data.size());

	for (size_t i = 0; i < Data.size(); i++) {

		_freqData[i] = Data[i];

		auto [alpha, beta] = coretools::probdist::TBetaDistr::calculateAlphaBetaForGivenModeConcentration(
		    coretools::P(Data[i]), Concentration);
		_data[i] = arcTrans(coretools::instances::randomGenerator().getBetaRandom(alpha, beta));
	}
}

TLocus::TLocus(const arma::Row<double> &SimData) {

	_data.resize(SimData.size());

	for (size_t i = 0; i < SimData.size(); i++) {
		if (SimData[i] < -PI_HALF) {
			_data[i] = invArcTrans(-PI_HALF);
		} else if (SimData[i] > PI_HALF) {
			_data[i] = invArcTrans(PI_HALF);
		} else {
			_data[i] = invArcTrans(SimData[i]);
		}
	}
}

double TLocus::arcTrans(double Freq) { return (asin(2.0 * Freq - 1.0)); }

double TLocus::invArcTrans(double TransformedData_d) { return ((sin(TransformedData_d) + 1.0) / 2.0); }

void TLocus::removeNoise() {
	for (size_t i = 0; i < _data.size(); i++) _data[i] = arcTrans(_freqData[i]);

	std::vector<double>().swap(_freqData);
}

//------------------------------------------------
// TGenome
//------------------------------------------------

TGenome::TGenome(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise) {
	readSitesPopulations(DataFile, PopNames, AddNoise);
}

void TGenome::readSitesPopulations(const std::string &DataFile, const std::vector<std::string> &PopNames,
                                   bool AddNoise) {
	if (parameters().exists("raw")) {
		_readRaw(DataFile, PopNames, AddNoise);
	} else {
		_readCounts(DataFile, PopNames, AddNoise);
	}
}

void TGenome::_readCounts(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise) {
	_addNoise = AddNoise;

	logfile().startIndent("Reading data from file '" + DataFile + "' ...");

	coretools::TDataReaderBase<coretools::UnsignedInt16, 3> reader;
	coretools::TMultiDimensionalStorage<coretools::UnsignedInt16, 3> storage;
	auto popNames = std::make_shared<coretools::TNamesStrings>(); // header save pop names

	_distances.initialize(maxDist);
	auto positions = std::make_shared<genometools::TNamesPositions>(&_distances); // class to read chr and position
	positions->setDelimName('\t');

	auto countsNames         = std::make_shared<coretools::TNamesEmpty>(); // third column name
	NumLociType guessNumLoci = parameters().get("guessNumLoci", 10000000); // num of expected rows

	reader.read(DataFile, &storage, {positions, popNames, countsNames}, {'\n', '\t', '/'}, {true, true, false},
	            {0, 0, 2}, {false, false, true}, guessNumLoci, coretools::colNames_multiline);

	// check files
	if (popNames->size() != PopNames.size())
		UERROR("Number of populations do not match between the treeFile and dataFile!");
	for (const auto &name : PopNames) {
		if (!popNames->exists(name)) UERROR("Name of populations do not match between the treeFile and dataFile!");
	}

	_numLoci = storage.dimensions()[0];
	_numPop  = storage.dimensions()[1];

	logfile().list("Number of loci: ", _numLoci);
	logfile().endIndent();

	_assignValuestoMembers(storage);
}

void TGenome::_readRaw(const std::string &DataFile, const std::vector<std::string> &PopNames, bool AddNoise) {
	_addNoise = AddNoise;

	logfile().startIndent("Reading raw data from file '" + DataFile + "' ...");

	coretools::TDataReaderBase<double, 2> reader;
	coretools::TMultiDimensionalStorage<double, 2> storage;
	auto popNames = std::make_shared<coretools::TNamesStrings>(); // header save pop names

	_distances.initialize(maxDist);
	auto positions = std::make_shared<genometools::TNamesPositions>(&_distances); // class to read chr and position
	positions->setDelimName('\t');

	NumLociType guessNumLoci = parameters().get("guessNumLoci", 10000000); // num of expected rows

	reader.read(DataFile, &storage, {positions, popNames}, {'\n', '\t'}, {true, true}, {0, 0}, {false, false},
	            guessNumLoci, coretools::colNames_multiline);

	// check files
	if (popNames->size() != PopNames.size())
		UERROR("Number of populations do not match between the treeFile and dataFile!");
	for (const auto &name : PopNames) {
		if (!popNames->exists(name)) UERROR("Name of populations do not match between the treeFile and dataFile!");
	}

	_numLoci = storage.dimensions()[0];
	_numPop  = storage.dimensions()[1];

	logfile().list("Number of loci: ", _numLoci);
	logfile().endIndent();

	_assignValuestoMembersRaw(storage);
}

void TGenome::_assignValuestoMembers(coretools::TMultiDimensionalStorage<coretools::UnsignedInt16, 3> &Storage) {
	logfile().startIndent("Estimating frequencies per locus...");

	_loci.reserve(_numLoci);
	_sigmas.reserve(_numLoci);

	NumLociType ignoreSites = _storeDatainLoci(Storage);
	_numLoci -= ignoreSites;

	logfile().addIndent();
	logfile().list("Number of included loci: ", _numLoci);

	_sigmas.clearPointers();
	logfile().list("Number of Sigmas: " + coretools::str::toString(_sigmas.numSigmas()));

	NumSigmaType maxSigmaConfigurations = parameters().get("maxSigmaConfigurations", 3);
	_sigmas.clusterNVectors(maxSigmaConfigurations);

	logfile().endIndent();
	logfile().endIndent();
	logfile().endIndent();
}

void TGenome::_assignValuestoMembersRaw(coretools::TMultiDimensionalStorage<double, 2> &Storage) {
	logfile().startIndent("Estimating frequencies per locus...");

	_loci.reserve(_numLoci);
	_sigmas.reserve(_numLoci);

	NumLociType ignoreSites = _storeRaw(Storage);
	_numLoci -= ignoreSites;

	logfile().addIndent();
	logfile().list("Number of included loci: ", _numLoci);

	_sigmas.clearPointers();
	logfile().list("Number of Sigmas: " + coretools::str::toString(_sigmas.numSigmas()));

	NumSigmaType maxSigmaConfigurations = parameters().get("maxSigmaConfigurations", 3);
	_sigmas.clusterNVectors(maxSigmaConfigurations);

	logfile().endIndent();
	logfile().endIndent();
}

NumLociType TGenome::_storeDatainLoci(coretools::TMultiDimensionalStorage<coretools::UnsignedInt16, 3> &Storage) {

	coretools::StrictlyPositive concentration{};
	if (_addNoise) {
		concentration = parameters().get("concentrationBeta", 1e12);
		if (concentration < 2)
			UERROR(
			    "Concentration of beta distribution must be 2 or more, where 'concentrationBeta' = 2 is uniform (all "
			    "possibilities are equally likely). "
			    "The distribution tighten about the expected frequency as concentration increases. Default = 1e12.");

		logfile().list("Concentration Beta = " + coretools::str::toString(concentration));
	}

	bool filterFixed                = !parameters().exists("keepFixed");
	size_t minNumPopWithBothAlleles = parameters().get("minNumPopWithBothAlleles", 0);

	NumLociType IgnoreSites{};
	genometools::TDistancesBinned<NumDistanceGroupType> distances;
	distances.initialize(maxDist);
	for (NumLociType l = 0; l < _numLoci; l++) {

		bool flag = true;
		arma::Col<double> popFreq(_numPop);
		std::vector<double> Ns(_numPop);

		bool fixedSite_0       = true;
		bool fixedSite_1       = true;
		size_t numPop_larger0  = 0;
		size_t numPop_smaller1 = 0;
		for (NumPopType m = 0; m < _numPop; m++) {
			Ns[m] = (uint16_t)Storage[Storage.getIndex({l, m, 1})];
			if (Ns[m] < 1) {
				flag = false;
				break; // ignore loci with missing Ns
			}
			size_t n   = Storage[Storage.getIndex({l, m, 0})];
			popFreq[m] = (double)n / (double)Ns[m];

			// check if fixed
			if (n > 0) {
				fixedSite_0 = false;
				numPop_larger0++;
			}
			if (n < Ns[m]) {
				fixedSite_1 = false;
				numPop_smaller1++;
			}
		}

		// filter on fixed sites?
		bool keep = true;
		if (filterFixed) { keep = (!fixedSite_0 && !fixedSite_1); }

		// filter on min number of populations with both alleles?
		if (minNumPopWithBothAlleles > 0) {
			size_t nPop = std::min(numPop_larger0, numPop_smaller1);
			if (nPop < minNumPopWithBothAlleles) { keep = false; }
		}

		if (flag && keep) {
			if (_addNoise) {
				_loci.emplace_back(popFreq, concentration);
			} else
				_loci.emplace_back(popFreq, true);

			_sigmas.addNVector(Ns);
			distances.add(_distances.getPosition(l), _distances.getChunkName(l));
		} else {
			++IgnoreSites;
		}
	}

	distances.finalizeFilling();
	_distances = distances;

	return IgnoreSites;
}

void TGenome::removeNoise() {
	if (_addNoise) {
		for (NumLociType l = 0; l < _numLoci; l++) _loci[l].removeNoise();
		_addNoise = false;
	} else
		logfile().list("No noise to remove from data!");
}

NumLociType TGenome::_storeRaw(coretools::TMultiDimensionalStorage<double, 2> &Storage) {
	const auto N = parameters().get<double>("N");
	std::vector<double> Ns(_numPop, N);

	for (NumLociType l = 0; l < _numLoci; l++) {
		arma::Col<double> popFreq(_numPop);
		for (NumPopType m = 0; m < _numPop; m++) { popFreq[m] = Storage[Storage.getIndex({l, m})]; }
		_loci.emplace_back(popFreq, false);
		_sigmas.addNVector(Ns);
	}

	return 0; // no filter
}
