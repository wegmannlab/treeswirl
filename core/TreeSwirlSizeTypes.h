/*
 * TreeSwirlSizeTypes.h
 *
 *  Created on: Mar 11, 2021
 *      Author: phaentu
 */

#ifndef CORE_TREESWIRLSIZETYPES_H_
#define CORE_TREESWIRLSIZETYPES_H_

#include "coretools/Math/TMatrix.h"
#include "stattools/HMM/TLatentVariableFixed.h"
#include "stattools/HMMDistances/TCombinedStatesTransitionMatrices.h"
#include "stattools/HMMDistances/TOptimizeTransMatLineSearch.h"
#include "stattools/HMMDistances/TOptimizeTransMatNelderMeadAttractorShift.h"
#include "stattools/HMMDistances/TTransitionMatrixDistancesExponential.h"
#include <cstddef>
#include <cstdint>

typedef uint8_t NumPopType;
typedef uint8_t NumBranchType;
typedef uint8_t NumMigEdgeType;
typedef uint16_t NumStatesType;
typedef uint32_t NumSigmaType;
typedef uint16_t NumSamplesType;
typedef uint32_t NumLociType;
typedef uint8_t NumDistanceGroupType;

const size_t maxDist = 1000000;

using TSTransMat          = stattools::TCombinedStatesTransitionMatrices<double, NumStatesType, NumLociType>;
using TSOptimizer         = stattools::TTransitionMatrixDistancesOptimizerBase<double, NumStatesType, NumLociType>;
using TSTransMatAttractor = stattools::TOptimizeTransMatNelderMead<
    double, NumStatesType, NumLociType, stattools::TTransitionMatrixScaledLadderAttractorShift2<double, NumStatesType>>;
using TSTransMatLadder =
    stattools::TOptimizeTransMatLineSearch<double, NumStatesType, NumLociType,
                                           stattools::TTransitionMatrixLadder<double, NumStatesType>>;

using TLatVarBase  = stattools::TLatentVariableWithRep<double, NumStatesType, NumLociType>;
using TLatVarFixed = stattools::TLatentVariableFixed<double, NumStatesType, NumLociType>;

#endif /* CORE_TREESWIRLSIZETYPES_H_ */
