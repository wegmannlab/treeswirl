//
// Created by reynac on 7/5/22.
//

#ifndef TREESWIRL2_TTASK_TREESWIRL_H
#define TREESWIRL2_TTASK_TREESWIRL_H

#ifdef WITH_PROFILING
#include <gperftools/profiler.h>
#endif // WITH_PROFILING

#include "TEstimator.h"
#include "TSimulator.h"
#include "coretools/Main/TTask.h"

//--------------------------------------
// Tasks
//--------------------------------------
class TTask_estimator : public coretools::TTask {
public:
	TTask_estimator()
	    : coretools::TTask("Inference of mixture proportions given a topology and allele counts",
	                       "Reyna-Blanco et al. 2024") {}

	void run() override {

#ifdef WITH_PROFILING
		ProfilerStart("profile.prof");
#endif // WITH_PROFILING

		TEstimator estimator;
		estimator.infer();

#ifdef WITH_PROFILING
		ProfilerStop();
#endif // WITH_PROFILING
	}
};

class TTask_simulator : public coretools::TTask {
public:
	TTask_simulator()
	    : coretools::TTask("Simulation of allele counts under TreeSwirl model", "Reyna-Blanco et al. 2024") {}

	void run() override {
		TSimulator simulator;
		simulator.simulate();
	}
};

class TTask_LL : public coretools::TTask {
public:
	TTask_LL()
	    : coretools::TTask("Calculating the likelihood and state posteriors for a given set of parameters.",
	                       "Reyna-Blanco et al. 2024") {}

	void run() override {
		TEstimator estimator;
		estimator.calculateLL();
	}
};

class TTask_PrintW : public coretools::TTask {
public:
	TTask_PrintW()
	    : coretools::TTask("Calculating the variance-covariance matrix W for a given set of parameters.",
	                       "Reyna-Blanco et al. 2024") {}

	void run() override {
		TSimulator simulator;
		simulator.printW();
	}
};

#endif // TREESWIRL2_TTASK_TREESWIRL_H
