/*
 * estimHet.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#include "coretools/Main/TMain.h"

//---------------------------------------------------------------------------
// Includes for tasks
//---------------------------------------------------------------------------
#include "TTask_TreeSwirl.h"

//---------------------------------------------------------------------------
// Existing Tasks
//---------------------------------------------------------------------------

void addTask(coretools::TMain &main) {
	main.addRegularTask("simulate", new TTask_simulator());
	main.addRegularTask("estimate", new TTask_estimator());

	main.addDebugTask("calculateLL", new TTask_LL());
	main.addDebugTask("printW", new TTask_PrintW());
};

//---------------------------------------------------------------------------
// Existing Integration tests
//---------------------------------------------------------------------------

void addTests(coretools::TMain &) {};

//---------------------------------------------------------------------------
// Main function
//---------------------------------------------------------------------------

int main(int argc, char *argv[]) {
	// Create main by providing a program name, a version, link to repo and contact email
	coretools::TMain main("TreeSwirl", "1.0", "https://bitbucket.org/wegmannlab/treeswirl/",
	                      "daniel.wegmann@unifr.ch");

	// add existing tasks
	addTask(main);
	addTests(main);

	// now run program
	return main.run(argc, argv);
};
