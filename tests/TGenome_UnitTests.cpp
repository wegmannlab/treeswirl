//
// Created by creyna on 24.02.23.
//


#include "TGenome.h"
#include "TTree.h"
#include "gtest/gtest.h"

using namespace coretools::instances;

class TGenome_Tests : public testing::Test {
public:

	std::string dataFile = "../tests/test_simulations_data.txt";
	std::string treeFile = "../tests/tree1.txt";
	TTree tree = TTree(treeFile);
	TGenome sites = TGenome(dataFile, tree.populationNames(), false);
};


TEST_F(TGenome_Tests, constructor) {

	EXPECT_EQ(sites.numLoci(), 994);
	EXPECT_EQ(sites.numSigmas(), 3);
	EXPECT_EQ(sites.numPop(), 3);
	EXPECT_EQ(sites.getChrName(1), "junk_1");
	EXPECT_NEAR(sites.locus(257).data(0), -0.2526803, 0.0001);
}