//
// Created by creyn on 6/17/2021.
//

#include "TSigma.h"
#include "gtest/gtest.h"

using namespace coretools::instances;

class TSigmas_Tests : public testing::Test {
public:

	std::vector<std::vector<double>> s{};
	size_t _numLoci = {};
	TSigmas _sigmas;

	void SetUp() override {
		std::vector<double> s0{0.1, 0.1, 0.1};
		std::vector<double> s1{0.1, 0.1, 0.2};
		std::vector<double> s2{0.2, 0.2, 0.2};
		std::vector<double> s3{0.1, 0.2, 0.2};
		std::vector<double> s4{0.2, 0.2, 0.2};
		std::vector<double> s5{0.1, 0.2, 0.2};

		s.push_back(s0);
		s.push_back(s1);
		s.push_back(s2);
		s.push_back(s3);
		s.push_back(s4);
		s.push_back(s5);
		_numLoci = s.size();

		_sigmas.reserve(_numLoci);

		for (NumLociType l = 0; l < _numLoci; l++) {
			_sigmas.addNVector(s[l]);
		}
		_sigmas.clearPointers();

	};


};


TEST_F(TSigmas_Tests, constructor_Noclustering) {

	NumSigmaType maxSigmaConfigurations = 6;
	_sigmas.clusterNVectors(maxSigmaConfigurations);

	EXPECT_EQ(_sigmas.numSigmas(), 4);
	EXPECT_EQ(_sigmas[0].counts(), 1);
	EXPECT_EQ(_sigmas[1].counts(), 1);
	EXPECT_EQ(_sigmas[2].counts(), 2);
	EXPECT_EQ(_sigmas[3].counts(), 2);

	EXPECT_NEAR(_sigmas.weightedNVector().diag()[0], 0.1333,0.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[1], 0.1667,.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[2], 0.1833,.0001);
}

TEST_F(TSigmas_Tests, constructor_clustering) {

	NumSigmaType maxSigmaConfigurations = 3;

	_sigmas.clusterNVectors(maxSigmaConfigurations);

	EXPECT_EQ(_sigmas.numSigmas(), 3);
	EXPECT_DOUBLE_EQ(_sigmas[0].getN(0), 0.1);
	EXPECT_DOUBLE_EQ(_sigmas[0].getN(1), 0.1);
	EXPECT_DOUBLE_EQ(_sigmas[0].getN(2), 0.15);

	EXPECT_NEAR(_sigmas.weightedNVector().diag()[0], 0.1333,0.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[1], 0.1667,.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[2], 0.1833,.0001);
}


TEST_F(TSigmas_Tests, constructor_clusteringAll) {

	NumSigmaType maxSigmaConfigurations = 1;

	_sigmas.clusterNVectors(maxSigmaConfigurations);

	EXPECT_EQ(_sigmas.numSigmas(), 1);
	EXPECT_NEAR(_sigmas[0].getN(0), 0.1333,0.0001);
	EXPECT_NEAR(_sigmas[0].getN(1), 0.1667,.0001);
	EXPECT_NEAR(_sigmas[0].getN(2), 0.1833,.0001);

	EXPECT_NEAR(_sigmas.weightedNVector().diag()[0], 0.1333,0.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[1], 0.1667,.0001);
	EXPECT_NEAR(_sigmas.weightedNVector().diag()[2], 0.1833,.0001);
}
