//
// Created by creyna on 14.06.21.
//

#include "coretools/Main/TParameters.h"
#include "TTree.h"
#include "TVarCovar_W.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"

//-------------------------------------------
// TVarCovar_W
//-------------------------------------------
class TVarCovar_W_tests : public testing::Test {
public:
	std::string treeFile = "../tests/tree1.txt";
	TTree tree = TTree(treeFile);
	std::vector<arma::Mat<double>> J{};
	std::vector<arma::Mat<double>> J2{};

	NumStatesType j = 3;
	NumPopType numPop = tree.numPopulations_M();
	NumBranchType numBranches = tree.numBranches_K();


	void SetUp() {
		J.resize(numBranches);

		for (NumBranchType b = 0; b < numBranches; b++) {
			J[b].zeros(numPop, numPop);
			if (b == 0 || b == 2)
				J[b](0, 0) = 1.0;
			if (b == 1 || b == 3)
				J[b](1, 1) = 1.0;
			if (b == 1 || b == 4)
				J[b](2, 2) = 1.0;
			if (b == 1){
				J[b](1, 2) = 1.; // 0.5
				J[b](2, 1) = 1.;
			}
		}

		J2.resize(numBranches);
		for (NumBranchType b = 0; b < numBranches; b++) {
			J2[b].zeros(numPop, numPop);
			if (b == 0 || b == 2)
				J2[b](0, 0) = 1.0;
			if (b == 0){
				J2[b](0, 1) = 0.5;
				J2[b](1, 0) = 0.5;
				J2[b](1, 1) = 0.25;
			}
			if (b == 1 || b == 3)
				J2[b](1, 1) = 0.25;
			if (b == 1 || b == 4)
				J2[b](2, 2) = 1.; // 0.5
			if ( b == 1){
				J2[b](1, 2) = 0.5;
				J2[b](2, 1) = 0.5;
			}

		}

	};
};

TEST_F(TVarCovar_W_tests, constructor) {

	TVarCovars_W W(tree, j);

	for (NumStatesType i = 0; i < j; ++i) {
		EXPECT_EQ(W.getW(i).size(), 3);
		//EXPECT_ANY_THROW(W[i+j]);

		if ( i < 2) {
			for (NumBranchType b = 0; b < numBranches; b++) {
				for (NumPopType m = 0; m < numPop; ++m) {
					for (NumPopType n = 0; n < numPop; ++n) {
						if (i == 0) EXPECT_FLOAT_EQ(W[i].J(b)(m, n), J[b](m, n));
						else if (i == 1) EXPECT_FLOAT_EQ(W[i].J(b)(m, n), J2[b](m, n));
					}
				}
			}
		}

	}

}

TEST_F(TVarCovar_W_tests, initializeJustJ){
	// test vectorization
	TVarCovar_W W(tree, {0.5}, true);
	arma::Col<double> vecJ = arma::vectorise(J2[1]);
	for (NumPopType m = 0; m < tree.numPopulations_M() * tree.numPopulations_M(); m++) {
		EXPECT_FLOAT_EQ(W.vectorized_J().col(1)(m), vecJ(m));
	}

	//////////
	TVarCovar_W W2 = W;
	tree.update({0.2,0.2,0.2,0.2,0.2});
	W.update(tree);

	EXPECT_EQ( W.J(1)(4), J2[1](1,1));
	EXPECT_FALSE(W.varCovar(1,1) == W2.varCovar(1,1));
}


TEST_F(TVarCovar_W_tests, permutation){

	std::vector<double> ws{0,0.5,1};
	std::vector<double> tmp{};
	std::vector<std::vector<double>> permutations{};
	TVarCovars_W W(tree, j);
	W.permutate_migrations_w(permutations, ws, tmp,  2);
	std::vector<std::vector<double>> combinations{};

	combinations.resize(9);
	combinations[0] = {0.,0.};
	combinations[1] = {0.,0.5};
	combinations[2] = {0.,1.};
	combinations[3] = {0.5,0.};
	combinations[4] = {0.5,0.5};
	combinations[5] = {0.5,1.};
	combinations[6] = {1.,0.};
	combinations[7] = {1.,0.5};
	combinations[8] = {1.,1.};

	EXPECT_TRUE(combinations == permutations);
}



TEST_F(TVarCovar_W_tests, updateWs) {

	TVarCovars_W Ws = TVarCovars_W(tree, j);
	std::vector<double> ks(5);
	ks[0] = 0.03;
	ks[1] = 0.04;
	ks[2] = 0.05;
	ks[3] = 0.008;
	ks[4] = 0.008;
	tree.update(ks);

	Ws.update(tree);
	EXPECT_FLOAT_EQ(Ws[0](0, 0), 0.08);

	EXPECT_FLOAT_EQ(Ws[0](0, 1), 0.);
	EXPECT_FLOAT_EQ(Ws[0](0, 2), 0.);
	EXPECT_FLOAT_EQ(Ws[0](1, 0), 0.0);
	EXPECT_FLOAT_EQ(Ws[0](1, 1), 0.048);
	EXPECT_FLOAT_EQ(Ws[0](1, 2), 0.04);
	EXPECT_FLOAT_EQ(Ws[0](2, 0), 0.);
	EXPECT_FLOAT_EQ(Ws[0](2, 1), 0.04);
	EXPECT_FLOAT_EQ(Ws[0](2, 2), 0.048);


	EXPECT_FLOAT_EQ(Ws[1](0, 0), 0.08);
	EXPECT_FLOAT_EQ(Ws[1](0, 1), 0.015);
	EXPECT_FLOAT_EQ(Ws[1](1, 0), 0.015);
	EXPECT_FLOAT_EQ(Ws[1](1, 1), 0.0195);
	EXPECT_FLOAT_EQ(Ws[1](1, 2), 0.02);
	EXPECT_FLOAT_EQ(Ws[1](2, 1), 0.02);
	EXPECT_FLOAT_EQ(Ws[1](2, 2), 0.048);

	EXPECT_FLOAT_EQ(Ws[2](0, 0), 0.08);
	EXPECT_FLOAT_EQ(Ws[2](0, 1), 0.03);
	EXPECT_FLOAT_EQ(Ws[2](1, 0), 0.03);
	EXPECT_FLOAT_EQ(Ws[2](1, 1), 0.03);
	EXPECT_FLOAT_EQ(Ws[2](2, 2), 0.048);
}