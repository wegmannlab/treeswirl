//
// Created by creyna on 14.06.21.
//

#include "TTree.h"
#include "gtest/gtest.h"

//-------------------------------------------
// TTree
//-------------------------------------------

TEST(TTree_test, constructorFile) {
	std::string treeFile = "../tests/tree1.txt";
	TTree tree(treeFile);
	EXPECT_EQ(tree.branchLengths_c(0), 1);
	EXPECT_EQ(tree.branchLengths_c(1), 1);
	EXPECT_EQ(tree.branchLengths_c(2), 0.1);
	EXPECT_EQ(tree.branchLengths_c(3), 0.1);
	EXPECT_EQ(tree.branchLengths_c(4), 0.1);

	EXPECT_EQ(tree.numPopulations_M(), 3);
	EXPECT_EQ(tree.numBranches_K(), 5);
	EXPECT_EQ(tree.numIMatrices(), 2);
	EXPECT_EQ(tree.numMigrations(), 1);
	EXPECT_EQ(tree.populationNames(0), "Pop1");
	EXPECT_EQ(tree.populationNames(1), "Pop2");
	EXPECT_EQ(tree.populationNames(2), "Pop3");

	EXPECT_EQ(tree.getI(0)(0, 0), true);
	EXPECT_EQ(tree.getI(0)(0, 1), false);
	EXPECT_EQ(tree.getI(0)(0, 2), true);
	EXPECT_EQ(tree.getI(0)(0, 3), false);
	EXPECT_EQ(tree.getI(0)(0, 4), false);
	EXPECT_EQ(tree.getI(0)(1, 0), false);
	EXPECT_EQ(tree.getI(0)(1, 1), true);
	EXPECT_EQ(tree.getI(0)(1, 2), false);
	EXPECT_EQ(tree.getI(0)(1, 3), true);
	EXPECT_EQ(tree.getI(0)(1, 4), false);
	EXPECT_EQ(tree.getI(0)(2, 0), false);
	EXPECT_EQ(tree.getI(0)(2, 1), true);
	EXPECT_EQ(tree.getI(0)(2, 2), false);
	EXPECT_EQ(tree.getI(0)(2, 3), false);
	EXPECT_EQ(tree.getI(0)(2, 4), true);
	// 1 0 1 0 0 0 1 0 1 0 0 1 0 0 1
	// 1 0 1 0 0 1 0 0 0 0 0 1 0 0 1
	EXPECT_EQ(tree.getI(1)(0, 0), true);
	EXPECT_EQ(tree.getI(1)(0, 1), false);
	EXPECT_EQ(tree.getI(1)(0, 2), true);
	EXPECT_EQ(tree.getI(1)(0, 3), false);
	EXPECT_EQ(tree.getI(1)(0, 4), false);
	EXPECT_EQ(tree.getI(1)(1, 0), true);
	EXPECT_EQ(tree.getI(1)(1, 1), false);
	EXPECT_EQ(tree.getI(1)(1, 2), false);
	EXPECT_EQ(tree.getI(1)(1, 3), false);
	EXPECT_EQ(tree.getI(1)(1, 4), false);
	EXPECT_EQ(tree.getI(1)(2, 0), false);
	EXPECT_EQ(tree.getI(1)(2, 1), true);
	EXPECT_EQ(tree.getI(1)(2, 2), false);
	EXPECT_EQ(tree.getI(1)(2, 3), false);
	EXPECT_EQ(tree.getI(1)(2, 4), true);
}
